# Contributing

Contributions are welcome and appreciated. The development of this package takes place on [GitLab][sleep-brain-atlas]. Issues, bugs, and feature requests should be reported there. Code and documentation can be improved by submitting a pull request. Please add documentation for any new code snippet. You can improve or add functionality in the repository by forking from main, developing your code, and then creating a pull request.

As a developer, you should install additional dependencies. If you have [Task](https://taskfile.dev/installation/) installed on your system, you can install the development dependencies via

```sh
task install-dev
```

If you also have [uv](https://docs.astral.sh/uv/getting-started/installation/) installed, you can have a faster build by running

```sh
task uv-install-dev
```

To build the documentation, run

```sh
task docs
```

The generated HTML files will show up in the `public` directory and go live once a new tag is pushed to the repository.

For linting, run

```sh
task lint
```

If you do not wish to use Task, you can check `Taskfile.yml` for the actual commands that are run by under the hood and adapt from there.

[sleep-brain-atlas]: https://gitlab.com/ceda-unibas/sleep-brain-atlas
