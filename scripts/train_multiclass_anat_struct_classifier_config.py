import numpy as np
from sklearn.metrics import balanced_accuracy_score, make_scorer

# from sklearn.neighbors import RadiusNeighborsClassifier
from sklearn.neighbors import KNeighborsClassifier

# Define the number of folds for the cross-validation
cv = 3

# Define parameter grid for the grid search
param_grid = {
    # 'radius': [1., 1.5, 2.],  # Best is 1.5: 0.902 score
    "n_neighbors": [14, 15, 16],  # Best is 15: 0.901 score
}

# Define the verbosity level for the grid search
verbose = 4

# Define the base classifier to be used in the grid search
# clf = RadiusNeighborsClassifier(
#     weights='distance',
#     algorithm='kd_tree',
#     leaf_size=10,
#     p=np.inf,
#     outlier_label='outlier',
#     n_jobs=64  # Try to match this to the number of available cores
# )

clf = KNeighborsClassifier(
    weights="distance",
    algorithm="kd_tree",
    leaf_size=10,
    p=np.inf,
    n_jobs=64,  # Try to match this to the number of available cores
)


# Define the scorer for the grid search
def scorer(Y):
    return make_scorer(
        balanced_accuracy_score,
        adjusted=True,
    )
