"""This script performs the computation of all-by-all distance matrices.

See the notebook `notebooks/03_2_d_clustering_smoothed_all_exp_types.ipynb` for context.

Usage example:
    python compute_all_by_all_DM --data_dir data/ --save_dir results/

After running the command above, the directory results/ will contain the
following files:
    if include_initial_state:
        if exp_type == 'no' or exp_type == 'misting':
            filename = folder_path + "/voxel_grid_coords_exp_type_" + str(exp_type) + '_ZT00_included' + ".csv"
        elif exp_type == 'darkno':
            filename = folder_path + "/voxel_grid_coords_exp_type_" + str(exp_type) + '_ZT12_included' + ".csv"
    else:
        filename = folder_path + 'voxel_activation_counts_df_avg_' + exp_type + '.csv'
"""

import click
import os
import traceback
import numpy as np
import pandas as pd
import ast
import pickle
from sklearn.metrics import pairwise_distances
from shapely.geometry import Polygon
import ot
from joblib import Parallel, delayed

from sba import (
    dataset,
    plotting,
    point_cloud
)


def compute_pairwise_distance(i, j, df, features_to_compute_distances, option='polygon', cost_matrix='None'):
    """
    Given two indices, the function computes pairwise distance of two points.

    i : int
        The index of the one element.
    j : int
        The index of the other element.
    df : pandas.DataFrame
        The dataframe.
    features_to_compute_distances : list
        The features based on which distances will be computed.
    option : bool (optional)
        If 'polygon' the distances based on polygon approximation will be computed, otherwsie 
        if Wasserstein, the Wasserstein distance is used.
    cost_matrix : np.array (len(features_to_compute_distances), len(features_to_compute_distances))
        The cost matrix based on which the Wasserstein distance will be computed.
    """
    if option == 'polygon':
        curve1 = df.iloc[i][features_to_compute_distances]
        curve2 = df.iloc[j][features_to_compute_distances]
        
        # Construct the polygons
        curve1_pol = list(zip(np.arange(len(features_to_compute_distances)), curve1))
        curve2_pol = list(zip(np.arange(len(features_to_compute_distances)), curve2))
        
        c1 = [(0, 0)] + curve1_pol + [(7, 0)] + [(0, 0)]
        c2 = [(0, 0)] + curve2_pol + [(7, 0)] + [(0, 0)]
        
        p1 = Polygon(c1)
        p2 = Polygon(c2)
        p3 = p1.intersection(p2)
        
        # Calculate the distance
        d = 1 - p3.area / max(p1.area, p2.area)

    elif option == 'Wasserstein':
        distribution1 = df.iloc[i][features_to_compute_distances].to_numpy().flatten()
        distribution2 = df.iloc[j][features_to_compute_distances].to_numpy().flatten()
        
        # Compute the transport plan and EMD value
        transport_plan = ot.emd(distribution1, distribution2, cost_matrix)
        emd_value = np.sum(transport_plan * cost_matrix)
        d = emd_value
        # print('distance', d)
    return i, j, d


def transform_row(row, option='pmf'):
    """Normalizes every row of a dataframe so that the values sum up to 1.
    
    Parameters
    ----------
    row : pandas.Series
        The input row of the dataframe.
    
    Returns
    -------
    transformed_row : pandas.Series
        The normalized output row of the dataframe.
    """

    row_min = row.min()
    row_max = row.max()
    
    if option=='pmf': # Normalize by division with vector norm.
        if row_min < 0:
            transformed_row = (row - row_min)
            row_max = transformed_row.max()
            transformed_row = transformed_row/transformed_row.sum()
        else:
            transformed_row = row / row.sum()
    elif option=='minmax': # Min-max scaling.
        if row_max == row_min:
            transformed_row = pd.Series(0, index=row.index)  # Set to 0 if all values are equal, or some other default value
        else:
            transformed_row = (row - row_min) / (row_max - row_min)
    else:
        raise ValueError(f"Unknown option '{option}'. Choose 'pmf' or 'minmax'.")

    return transformed_row


@click.command()
@click.option('--data_dir',
              required=True,
              type=click.Path(dir_okay=True, exists=True),
              help='Path to the dataset directory')
@click.option('--save_dir',
              required=True,
              type=click.Path(dir_okay=True, exists=True),
              help='Path to the directory where to save the output')

def main(data_dir, save_dir):

    # Parameters.
    voxel_size_sel = 1
    exp_type = 'circadian'
    threshold = 0
    include_initial_state = True
    work_with_batch_corrected = True
    misting_with_adjusted_samples =True
    allowed_distance_options = {'None', 'Euclidean', 'Wasserstein', 'Polygon-intersection-over-union'}
    distance_option = 'Wasserstein' # Select one of 'Euclidean', 'Wasserstein', 'Polygon-intersection-over-union'.
    # Check if distance_option is valid.
    if distance_option not in allowed_distance_options:
        raise ValueError(f"Invalid distance option: '{distance_option}'. Please choose one of {allowed_distance_options}.")
    allowed_normalization_options = {'pmf', 'minmax'}

    df_names_clustering = []
    df_names_clustering.append('raw')
    df_names_clustering.append('vox std')

    if exp_type == 'circadian':
        conditions = ['ZT00', 'ZT03', 'ZT06', 'ZT09', 'ZT12', 'ZT15', 'ZT18', 'ZT21']
    elif exp_type == 'no' or exp_type == 'misting' or exp_type == 'darkno':
        if include_initial_state:
            if exp_type == 'no' or exp_type == 'misting':
                conditions = ['ZT00', 'SD1p5', 'SD3', 'SD5', 'SD6', 'R1p5', 'R3']
            elif exp_type == 'darkno':
                conditions = ['ZT12', 'SD1p5', 'SD3', 'SD5', 'SD6', 'R1p5', 'R3']
        else:
            conditions = ['SD1p5', 'SD3', 'SD5', 'SD6', 'R1p5', 'R3']
            
    # Load the dataframe.
    base_path = os.path.join(data_dir, 'analysis_files')
    folder_path = base_path + '/voxelization_and_analysis/voxel_size_' + str(voxel_size_sel) + '/smoothing'

    # Initialize a list to store the loaded DataFrames
    df_arr = []

    # Load the dataframes containing the kmeans clustering from step 1.
    for i, name in enumerate(df_names_clustering):
        if exp_type == 'misting' and misting_with_adjusted_samples:
            file_name = os.path.join(folder_path, f'exp_{exp_type}_voxels_size_{voxel_size_sel}_211110_samples_adjusted_thresholding_{threshold}_' + '_'.join(name.split(' ')) + '_kmeans_results.csv')
        else:
            file_name = os.path.join(folder_path, f'exp_{exp_type}_voxels_size_{voxel_size_sel}_threshold_{threshold}_' + '_'.join(name.split(' ')) + '_kmeans_results.csv')
        
        # Load the DataFrame
        df = pd.read_csv(file_name, index_col=0)  # Assuming index=True was used during saving
        df_arr.append(df)

    # Compute the cluster centroids.
    cluster_centr_df_arr = []

    for i in range(len(df_arr)):
        print(i, df_arr[i].shape)
        
        cluster_centr = df_arr[i].groupby('cluster').median() # Use median or mean (line below).
        # cluster_centr = df_kmeans[i].groupby('cluster').mean()
        
        cluster_centr = cluster_centr[conditions].copy(deep=True)

        cluster_centr_df = pd.DataFrame(cluster_centr)

        cluster_centr_df_arr.append(cluster_centr_df)

    # Note: In the following we will work only with df_arr[0].
    df = cluster_centr_df_arr[0].copy(deep=True)
    print(df.shape)

    if distance_option == 'Euclidean':
        if normalization_option != 'None':
            df_temp_norm = df[conditions].apply(lambda row: transform_row(row, option=normalization_option), axis=1)
            EDM = pairwise_distances(df_temp_norm[conditions].values, metric='euclidean')
        else:
            EDM = pairwise_distances(df[conditions].values, metric='euclidean')

        # Save the Euclidean distance matrix
        edm_npy_path = os.path.join(save_dir, f'Euclidean_DM_{exp_type}_normalization_{normalization_option}.npy')
        np.save(edm_npy_path, EDM)
        print(f"Euclidean all-by-all distance matrix saved as NPY at: {edm_npy_path}")
    elif distance_option == 'Wasserstein':
        # Normalize dataframe if neeeded.
        normalization_option = 'pmf' # Select 'pmf' for distribution normlization, 'minmax' for minmax
        # Check if normalization_option is valid.
        if normalization_option not in allowed_normalization_options:
            raise ValueError(f"Invalid distance option: '{normalization_option}'. Please choose one of {allowed_normalization_options}.")
        df_temp_norm = df[conditions].apply(transform_row, axis=1)

        size = len(conditions)
        cost_matrix = np.zeros((size, size))

        for i in range(size):
            for j in range(size):
                # Linear cost.
                forward_distance = abs(i - j)
                backward_distance = size - forward_distance
                # Quadratic cost.
                # forward_distance = abs(i - j)**2
                # backward_distance = (size - forward_distance)**2
                cost_matrix[i, j] = min(forward_distance, backward_distance)

        # Generate all unique pairs (i, j) where j > i
        indices = [(i, j) for i in range(df_temp_norm.shape[0]) for j in range(i + 1, df_temp_norm.shape[0])]

        WDM = np.zeros((df_temp_norm.shape[0], df_temp_norm.shape[0]))
        # Run parallel computation.
        results = Parallel(n_jobs=-1, verbose=10, batch_size=10)(
            delayed(compute_pairwise_distance)(i, j, df_temp_norm, conditions, 'Wasserstein', cost_matrix) for i, j in indices)

        # Fill the results into the distance matrix
        for i, j, emd_value in results:
            WDM[i, j] = emd_value
            WDM[j, i] = emd_value  # Fill symmetric entry

        # Save.
        wdm_npy_path = os.path.join(save_dir, f'Wasserstein_DM_{exp_type}_threshold_{threshold}.npy')
        np.save(wdm_npy_path, WDM)
        print(f"WDM saved as NPY at: {wdm_npy_path}")

    elif distance_option == 'Polygon-intersection-over-union':
        # Define the cost matrix for Earth mover's distance calculation.
        normalization_option = 'minmax' # Select 'pmf' for distribution normlization, 'minmax' for minmax
        # Check if normalization_option is valid.
        if normalization_option not in allowed_normalization_options:
            raise ValueError(f"Invalid distance option: '{normalization_option}'. Please choose one of {allowed_normalization_options}.")
        df_temp_norm = df[conditions].apply(lambda row: transform_row(row, option=normalization_option), axis=1)

        size = len(conditions)

        # df_temp_norm = df_sub[features_to_compute_distances].apply(transform_row(option='minmax'), axis=1)

        pi_dm = np.zeros((df_temp_norm.shape[0], df_temp_norm.shape[0]))

        print('Computing Polygon-intersection-over-union intersection distances...')

        # Generate all unique pairs (i, j) where j > i.
        indices = [(i, j) for i in range(df_temp_norm.shape[0]) for j in range(i + 1, df_temp_norm.shape[0])]

        # Run parallel computation
        results = Parallel(n_jobs=-1, verbose=10)(delayed(compute_pairwise_distance)(i, j, df_temp_norm, conditions, 'polygon') for i, j in indices)

        # Fill the results into the distance matrix
        for i, j, d in results:
            pi_dm[i, j] = d
            pi_dm[j, i] = d  # Fill symmetric entry

        # Save.
        pi_dm_npy_path = os.path.join(save_dir, f'Wasserstein_DM_{exp_type}_threshold_{threshold}_normalization_{normalization_option}.npy')
        np.save(pi_dm_npy_path, pi_dm)
        print(f"Polygon-intersection-over-union all-by-all distance matrix saved as NPY at: {pi_dm_npy_path}")

    print("Done. All files were saved on {}".format(save_dir))

if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())