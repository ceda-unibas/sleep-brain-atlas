"""Make 2D KDE plots for the circadian experiment type.

This script makes 2D KDE plots for the circadian experiment type. The plots
are made for each condition and each sample in the experiment type for the
following cartesian axes: x-y, x-z, y-z. The plots are saved in the specified
directory.

See the notebook `notebooks/02_point_cloud_comparison.ipynb` for context on
these plots.

Usage example:
    python make_2d_kde_circadian_plots.py --data_dir data/ --save_dir results/

After running the command above, the directory results/ will contain the
following files:
    kde_x_y_plot_per_sample_per_condition.png
    kde_x_z_plot_per_sample_per_condition.png
    kde_y_z_plot_per_sample_per_condition.png
"""

import os
import traceback

import click
import matplotlib.pyplot as plt
import seaborn as sns
from tqdm import tqdm

from sba import (
    dataset,
    plotting,
)


def make_kde_plot(
    grouping, save_path=None, cartesian_axes: tuple = ("x", "y"), **kwargs
):
    """Make a 2D KDE plot of activation densities for each sample in each group

    Parameters
    ----------
    grouping : pandas.core.groupby.generic.DataFrameGroupBy
        A grouping of the transformed centroids table.
    save_path : str, optional
        Path where to save the plot, by default None. If None, the plot is
        not saved.
    cartesian_axes : tuple, optional
        Cartesian axes to plot. The default is ('x', 'y').
    dpi : int, optional
        Resolution of the saved plot in dots per inch (DPI), by default 300.
    **kwargs : dict, optional
        Extra keyword arguments to pass to seaborn.kdeplot.

    Returns
    -------
    fig : matplotlib.figure.Figure
        Figure object.
    ax : matplotlib.axes._subplots.AxesSubplot
        Axes object.
    """
    # Parse kwargs by removing the key/value pairs that we already use in our
    # call to kdeplot
    kwargs.pop("data", None)
    kwargs.pop("x", None)
    kwargs.pop("y", None)
    kwargs.pop("fill", None)
    kwargs.pop("cmap", None)
    kwargs.pop("ax", None)

    # Create figure and axes
    fig, ax = plt.subplots(8, 8, figsize=(24, 24))
    [axi.set_axis_off() for axi in ax.ravel()]

    # Plot KDEs
    counter_row = 0
    counter_col = 0
    for _, df in tqdm(grouping):
        sample_id_grouping = df.groupby("sample_id")
        for _, df_sample in sample_id_grouping:
            sns.kdeplot(
                data=df_sample,
                x=cartesian_axes[0],
                y=cartesian_axes[1],
                fill=True,
                cmap="mako",
                ax=ax[counter_row, counter_col],
                **kwargs,
            )
            counter_col += 1
        counter_col = 0
        counter_row += 1

    # Make tight layout
    plt.tight_layout()

    # Save figure
    if save_path is not None:
        fig.savefig(save_path, dpi=300, bbox_inches="tight", pad_inches=0)

    return fig, ax


@click.command()
@click.option(
    "--data_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True),
    help="Path to the dataset directory",
)
@click.option(
    "--save_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True),
    help="Path to the directory where to save the output",
)
def main(data_dir, save_dir):
    # Load transformed centroids table
    aad = dataset.AtlasActivationsDataset(data_dir)
    transformed_centroids_table = aad.load_tc_table(format_conditions=True)

    # Create circadian exp_type data frame
    exp_type_grouping = transformed_centroids_table.groupby("exp_type")
    df_circadian = exp_type_grouping.get_group("circadian")

    # Group data frame by circadian condition
    condition_grouping = df_circadian.groupby("condition")

    # Run plotting functions
    print("Generating plots...")
    print("KDE X-Y...")
    _ = make_kde_plot(
        condition_grouping,
        os.path.join(
            save_dir,
            "kde_x_y_plot_per_sample_per_condition.png",
        ),
        cartesian_axes=("x", "y"),
    )
    print("KDE X-Z...")
    _ = make_kde_plot(
        condition_grouping,
        os.path.join(
            save_dir,
            "kde_x_z_plot_per_sample_per_condition.png",
        ),
        cartesian_axes=("x", "z"),
    )
    print("KDE Y-Z...")
    _ = make_kde_plot(
        condition_grouping,
        os.path.join(
            save_dir,
            "kde_y_z_plot_per_sample_per_condition.png",
        ),
        cartesian_axes=("y", "z"),
    )
    print("Done. All the plots were saved on {}".format(save_dir))


if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
