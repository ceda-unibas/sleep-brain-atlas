"""Extract the brain outline from the anatomical atlas.
"""

import click
import os
import traceback

from sba import (
    dataset,
)


@click.command()
@click.option('--data_dir',
              required=True,
              type=click.Path(dir_okay=True, exists=True),
              help='Path to the dataset directory')
@click.option('--save_path',
              required=False,
              default=None,
              type=click.Path(dir_okay=False, exists=False),
              help='Full path where to save the output')
@click.option('--subsample_fraction',
              required=False,
              type=float,
              default=0.1,
              help='Fraction of the total number of coordinates to keep')
def main(data_dir, save_path, subsample_fraction):

    # Create dataset object
    aad = dataset.AtlasActivationsDataset(data_dir)

    # Get the brain outline
    print("Building brain outline table...")
    _ = aad.load_brain_outline_table(
        rebuild=True,
        path=save_path,
        subsample_fraction=subsample_fraction
    )

    if save_path is None:
        save_path = aad.get_anatomical_atlas_path()
        save_path = os.path.join(save_path,
                                 "brain_outline_table.csv")

    print("Done. Results were saved on {}".format(save_path))


if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
