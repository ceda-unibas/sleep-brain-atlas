"""This script computes the activated cells in test mice data for every anatomical region by using the dense annotations of the regions and knn classifier.

See the notebook `notebooks/05_find_activations_in_all_anatomical_regions.ipynb` for context.

Usage example:
    python find_activated_cells_belonging_to_anatomical_regions_dense_knn --data_dir data/ --save_dir results/

After running the command above, the directory results/ will contain the
following files:
    circadian_region.npy
    cm_test.npy
    cm_train.npy
    density_of_regions_sorted.npy
    df_regions_stats.csv
    indices.npy
    number_of_points_by_region_sorted.npy
    set_labels.npy
    set_points.npy
    test_score.npy
    train_score.npy
"""

import click
import os
import traceback
import numpy as np
from copy import deepcopy
from sklearn.model_selection import train_test_split
from joblib import Parallel, delayed
import pandas as pd

from sba import (
    dataset,
    plotting,
    point_cloud
)

def run_inquiry(region_id, df, anatomical_atlas, method='knn'):
    """This function identifies the points belonging to the region with id equal to region_id and returns
    the points in test data that belong to the given anatomical regions.

    Parameters
    ----------
    regiond_id : int
        The id of the region as assigned in the anatomical table.
    df : pandas.DataFrame
        The dataframe with the activation points of the test mice brains.
    anatomical_atlas : sba.anatomical_atlas object
        The anatomical atlas object.
    method: str
        The method used to find the points belonging to the anatomical region in query (with region id equa lto region_id).
        Options: - (default) 'knn': kNN classifier.
                 - 'uc': The union of convex sets method.

    Returns
    -------
    set_points : numpy.list
        The points from the anatomical table selected to fit the classifier.
    set_labels : numpy.list
        The labels of the selected points (1 means the point belongs to the region, 0 it doesn't)
    train_score : float
        The f1 score in the train set.
    cm_train: numpy.array
        The confusion mtrix for the train data.
    test_score : float
        The f1 score in the test set.
    indices : numpy.list
        The list of the coordinates of the points in the experimental data found that belong to the region in query
    df_region : pandas.DataFrame()
        The dataframe containing the points in the experimental data found that belong to the region in query.
    """
    
    try:
        points_set = point_cloud.Set()
        
        # Train a classifier.
        set_points, set_labels = anatomical_atlas.get_one_vs_all_labeling(
            region_id,
            id_sample_frac=0.95,
            not_id_sample_frac=0.1
        )

        train_points, test_points, train_labels, test_labels = \
        train_test_split(
            set_points,
            set_labels,
            test_size=0.2,
            stratify=set_labels,
            random_state=2023,
        )
        
        train_score, cm_train = points_set.fit(train_points, train_labels, method=method, verbose=False) #method='uc', also perform split train - test before it (see notebook 002-quering anatomical structures)

        test_score, cm_test = points_set.evaluate(
        test_points,
        test_labels,
        verbose=False
    )
        
        indices = points_set.get_indices_belonging_to_set(
        df.loc[:, 'x':'z'].to_numpy()
    )

        df_region = df.iloc[indices, :] # The test points of the dataframe belonging to that region.

        return set_points, set_labels, train_score, cm_train, test_score, cm_test, indices, df_region
    except Exception as e:
        print(f"Task failed for k={k}. Error: {e}")
        return None, None, None, None, None, None, None, None


@click.command()
@click.option('--data_dir',
              required=True,
              type=click.Path(dir_okay=True, exists=True),
              help='Path to the dataset directory')
@click.option('--save_dir',
              required=True,
              type=click.Path(dir_okay=True, exists=True),
              help='Path to the directory where to save the output')


def main(data_dir, save_dir):

    # Load transformed centroids table and anatomical table
    aad = dataset.AtlasActivationsDataset(data_dir)
    transformed_centroids_table = aad.load_tc_table(format_conditions=True)
    anatomical_table = aad.load_anatomical_table(rebuild=False)
    anatomical_atlas = dataset.AnatomicalAtlas(anatomical_table)

    # Get the activated test points for the circadian cycle.
    circadian_df = transformed_centroids_table[transformed_centroids_table['exp_type']=='circadian']
    conditions = sorted(circadian_df['condition'].unique())

    # Check how many points each anatomical region has and sort them by absolute number of activations and by activation density (divided by the volume of the corresponding bounding box).
    number_of_points_by_region = {}
    density_of_regions = {}
    counter = 0
    
    print('Computing points of anatomical regions...')

    for region_id in anatomical_table.id.unique():
        
        counter += 1
        
        # Get the coordinates of the bounding box of the anatomical region (plus some margin).
        margin = [0, 0, 0]
        region_bounding_box_min = anatomical_table[anatomical_table['id']==region_id][['x','y','z']].min().to_numpy() - margin[:]
        region_bounding_box_max = anatomical_table[anatomical_table['id']==region_id][['x','y','z']].max().to_numpy() + margin[:]

        circadian_within_bounding_box = circadian_df[(circadian_df['x']>region_bounding_box_min[0])\
        & (circadian_df['y']>region_bounding_box_min[1]) & (circadian_df['z']>region_bounding_box_min[2])\
        & (circadian_df['x']<region_bounding_box_max[0]) & (circadian_df['y']<region_bounding_box_max[1])\
        & (circadian_df['z']<region_bounding_box_max[2])].copy(deep=True)
        
        number_of_points_by_region[region_id] = len(circadian_within_bounding_box)
        diameter = np.max(region_bounding_box_max[:]-region_bounding_box_min[:])
        volume = (region_bounding_box_max[0]-region_bounding_box_min[0])*(region_bounding_box_max[1]-region_bounding_box_min[1])*(region_bounding_box_max[2]-region_bounding_box_min[2])
        
        density_of_regions[region_id] = len(circadian_within_bounding_box)/volume
        
    number_of_points_by_region_sorted = dict(sorted(number_of_points_by_region.items(), key=lambda item: item[1], reverse=True))
    density_of_regions_sorted = dict(sorted(density_of_regions.items(), key=lambda item: item[1], reverse=True))

    # Save results
    save_name = os.path.join(
            save_dir,
            'number_of_points_by_region_sorted.npy',
        )
    temp_arr = np.asarray(number_of_points_by_region_sorted, dtype="object")
    np.save(save_name, temp_arr)

    save_name = os.path.join(
            save_dir,
            'density_of_regions_sorted.npy',
        )
    temp_arr = np.asarray(density_of_regions_sorted, dtype="object")
    np.save(save_name, temp_arr)

    start_ind = 0
    end_ind = len(number_of_points_by_region_sorted)
    regions_to_check = deepcopy(list(number_of_points_by_region_sorted)[start_ind:end_ind]) # Get the ids of the regions to be checked.

    # Find the points belonging to each region.
    print("Compute the points belonging to each anatomical region...")

    results_par_computation = Parallel(n_jobs=-1, verbose=10)(
    delayed(run_inquiry)(k, circadian_df, anatomical_atlas) for k in regions_to_check)

    set_points, set_labels, train_score, cm_train, test_score, cm_test, indices, circadian_region = zip(*results_par_computation)    

    for i in range(len(regions_to_check)):
        print('Region:', anatomical_atlas.translate_from_id('name', regions_to_check[i]), regions_to_check[i])
        print(train_score[i], test_score[i])

    # Put the results in a dataframe.
    df_regions_stats = pd.DataFrame()
    for i in range(len(circadian_region)):
        new_row = pd.DataFrame({'name': [anatomical_atlas.translate_from_id('name', regions_to_check[i])]
                })
        for c in conditions:
            new_row[c] = circadian_region[i][circadian_region[i]['condition']==c].count().values[0]
        new_row['max_diff'] = new_row[conditions].values.max() - new_row[conditions].values.min()
        new_row['max_relative_diff'] = new_row['max_diff']/new_row[conditions].values.min()
        new_row['kNN_train'] = train_score[i]
        new_row['kNN_test'] = test_score[i]
        df_regions_stats = pd.concat([df_regions_stats, new_row], ignore_index=True)

    # Save the dataframe.
    save_name = os.path.join(
            save_dir,
            'df_regions_stats.csv',
        )
    df_regions_stats.to_csv(save_name, index=False)

    # Save the rest of the results.
    save_name = os.path.join(
            save_dir,
            'set_points.npy',
        )
    temp_arr = np.asarray(set_points, dtype="object")
    np.save(save_name, temp_arr)

    save_name = os.path.join(
            save_dir,
            'set_labels.npy',
        )
    temp_arr = np.asarray(set_labels, dtype="object")
    np.save(save_name, temp_arr)

    save_name = os.path.join(
            save_dir,
            'train_score.npy',
        )
    temp_arr = np.asarray(train_score, dtype="object")
    np.save(save_name, temp_arr)

    save_name = os.path.join(
            save_dir,
            'test_score.npy',
        )
    temp_arr = np.asarray(test_score, dtype="object")
    np.save(save_name, temp_arr)

    save_name = os.path.join(
            save_dir,
            'cm_train.npy',
        )
    temp_arr = np.asarray(cm_train, dtype="object")
    np.save(save_name, temp_arr)

    save_name = os.path.join(
            save_dir,
            'cm_test.npy',
        )
    temp_arr = np.asarray(cm_test, dtype="object")
    np.save(save_name, temp_arr)

    save_name = os.path.join(
            save_dir,
            'indices.npy',
        )
    temp_arr = np.asarray(indices, dtype="object")
    np.save(save_name, temp_arr)

    save_name = os.path.join(
            save_dir,
            'circadian_region.npy',
        )
    temp_arr = np.asarray(circadian_region, dtype="object")
    np.save(save_name, temp_arr)

    print("Done. All the plots were saved on {}".format(save_dir))
    

if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())


