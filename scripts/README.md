# Notebooks

The `scrips/` folder contains executable scripts used in the project.

To make sure you can run these scripts, please run

```sh
pip install -r requirements.txt
```

under the repository's environment. This command will install the extra Python dependencies required by some of the scripts.

To run the R scripts the variable "SBA_DATA", which indicates the path to the data, needs to be defined in a .Renviron file. This file should be located in the root working directory visible by R.
