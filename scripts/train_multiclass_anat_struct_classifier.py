"""Script to train a multilabel classifier model to predict to which
anatomical regions a given point in the 3D atlas belongs."""

import os
import traceback

import click
import joblib
import numpy as np
import pandas as pd
import train_multiclass_anat_struct_classifier_config as config
from sklearn.metrics import (
    balanced_accuracy_score,
    roc_auc_score,
    top_k_accuracy_score,
)
from sklearn.model_selection import GridSearchCV, train_test_split

from sba import dataset


def subsample_table(anatomical_table, frac, min_n_points, seed):
    """Subsample the anatomical table to avoid out-of-memory errors."""
    if frac >= 1:
        # Bypass
        return anatomical_table

    # Group by id
    id_grouping = anatomical_table.groupby("id")

    # Subsample within each group
    dfs = []
    for _, df in id_grouping:
        n_points = df.shape[0]
        if (frac * n_points) < min_n_points:
            dfs.append(df.sample(n=min_n_points, replace=True, random_state=seed))
        else:
            dfs.append(df.sample(frac=frac, random_state=seed))

    return pd.concat(dfs)


def train_multiclass_classifier(X, Y, test_size=0.2, seed=2024):
    # Split the data into train and test sets
    X_train, X_test, Y_train, Y_test = train_test_split(
        X,
        Y,
        test_size=test_size,
        stratify=Y,
        random_state=seed,
    )

    # Define the grid search
    grid_search = GridSearchCV(
        estimator=config.clf,
        param_grid=config.param_grid,
        scoring=config.scorer(Y),
        cv=config.cv,
        verbose=config.verbose,
        n_jobs=1,
    )

    # Run the grid search
    grid_search.fit(X_train, Y_train)

    # Get the training results
    training_results = pd.DataFrame(grid_search.cv_results_)
    training_results = training_results.sort_values(by="rank_test_score")

    # Get the best classifier
    clf = grid_search.best_estimator_

    # Get predictions on the test set
    Y_pred = clf.predict(X_test)
    Y_pred_proba = clf.predict_proba(X_test)

    # In case there are outliers, their corresponding row in Y_pred_proba is
    # all zeros. We need to change them to 1 / n_classes to be able to use
    # roc_auc_score and top_k_accuracy_score
    n_classes = len(clf.classes_)
    mask = Y_pred_proba.sum(axis=1) == 0
    Y_pred_proba[mask] = 1 / n_classes

    # Evaluate on test set
    test_results = pd.DataFrame(
        {
            "roc_auc_ovr_weighted": [
                roc_auc_score(
                    Y_test, Y_pred_proba, average="weighted", multi_class="ovr"
                )
            ],
            "balanced_accuracy": [
                balanced_accuracy_score(Y_test, Y_pred, adjusted=True)
            ],
            "top_2_accuracy": [top_k_accuracy_score(Y_test, Y_pred_proba, k=2)],
        }
    )

    return clf, training_results, test_results


@click.command()
@click.option(
    "--data_dir",
    required=True,
    type=click.Path(exists=True),
    help="The root directory of the dataset",
)
@click.option(
    "--save_dir",
    required=True,
    type=click.Path(exists=False),
    help="The directory where to save the trained model " + "and performance results",
)
@click.option(
    "--test_size",
    default=0.2,
    type=float,
    help="The proportion of the dataset to in the test split",
)
@click.option(
    "--frac",
    default=0.01,
    type=float,
    help="The fraction of the dataset to use for training. "
    + "Set this to less than one to avoid out-of-memory errors",
)
@click.option(
    "--min_n_points",
    default=128,
    type=int,
    help="The minimum number of points to sample per anatomical "
    + "region. This is only used if frac < 1.0.",
)
@click.option(
    "--seed", default=2024, type=int, help="The random seed to use for reproducibility"
)
def main(data_dir, save_dir, test_size, frac, min_n_points, seed):
    # Create dataset object
    aad = dataset.AtlasActivationsDataset(data_dir)

    # Load the anatomical table
    anatomical_table = aad.load_anatomical_table()

    # Subsample the table to avoid out-of-memory errors
    anatomical_table = subsample_table(
        anatomical_table, frac=frac, min_n_points=min_n_points, seed=seed
    )

    # Get X, Y arrays for the classification task
    X = anatomical_table[["x", "y", "z"]].values.astype(np.float32)
    Y = anatomical_table["name"].values

    # Treat the problem as a multiclass classification problem
    clf, training_results, test_results = train_multiclass_classifier(
        X,
        Y,
        test_size=test_size,
        seed=seed,
    )

    # Make the save directory if it does not exist
    os.makedirs(save_dir, exist_ok=True)

    # Save the model as a joblib file
    model_name = "anat_struct_classifier"
    joblib.dump(clf, os.path.join(save_dir, model_name + ".joblib"))

    # Save the training and test results as CSV files
    training_results_name = "training_results.csv"
    training_results.to_csv(
        os.path.join(save_dir, training_results_name),
        index=False,
    )
    test_results_name = "test_results.csv"
    test_results.to_csv(
        os.path.join(save_dir, test_results_name),
        index=False,
    )


if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
