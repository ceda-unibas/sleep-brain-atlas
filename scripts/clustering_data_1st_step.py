"""This script performs the 1st step clustering of the large dataframe.

See the notebook `notebooks/03_2_d_clustering_smoothed_all_exp_types.ipynb` for context.

Usage example:
    python clustering_data_1st_step --data_dir data/ --save_dir results/

After running the command above, the directory results/ will contain the
following files:
    for i, df in enumerate(df_kmeans):
        if exp_type == 'misting' and misting_with_adjusted_samples:
            file_name = os.path.join(folder_path, 'exp_' + exp_type + 'voxels_size_' + str(voxel_size_sel) + '_211110_samples_adjusted' + '_thresholding_' + str(threshold) + '_' + ('_').join(df_names_clustering[i].split(' ')) + '_kmeans_results.csv')
        else:
            file_name = os.path.join(folder_path, 'exp_' + exp_type + 'voxels_size_' + str(voxel_size_sel) + '_threshold_' + str(threshold) + '_' + ('_').join(df_names_clustering[i].split(' ')) + '_kmeans_results.csv')
        print(file_name)
        df.to_csv(file_name, index=True)
"""

import click
import os
import traceback
import numpy as np
import pandas as pd
import pickle
from copy import deepcopy
from sklearn.cluster import MiniBatchKMeans

from sba import (
    dataset,
    plotting,
    point_cloud
)


def minibatch_kmeans_clustering(df_arr, df_names, n_clusters=5, batch_size=100):
    df_kmeans = []

    for i in range(len(df_arr)):
        print(df_names[i])

        # Perform MiniBatch K-Means clustering
        minibatch_kmeans = MiniBatchKMeans(n_clusters=n_clusters, batch_size=batch_size, random_state=42)
        minibatch_kmeans.fit(df_arr[i].T)  # Fit the model on transposed data
        clusters = minibatch_kmeans.predict(df_arr[i].T)  # Predict the clusters

        # Copy the DataFrame and assign the cluster labels
        df_temp = df_arr[i].T.copy(deep=True)
        df_temp['cluster'] = clusters
        df_kmeans.append(df_temp)

    return df_kmeans


@click.command()
@click.option('--data_dir',
              required=True,
              type=click.Path(dir_okay=True, exists=True),
              help='Path to the dataset directory')
@click.option('--save_dir',
              required=True,
              type=click.Path(dir_okay=True, exists=True),
              help='Path to the directory where to save the output')

def main(data_dir, save_dir):

    # Select a voxel size and experiment type to work with.
    voxel_size_sel = 1
    exp_type = 'misting'
    threshold = 0
    include_initial_state = True
    work_with_batch_corrected = True
    misting_with_adjusted_samples =True

    if exp_type == 'circadian':
        include_initial_state = False
        
    # Load data.
    print('Selected voxel size: ', voxel_size_sel)
    print('Selected experimental type: ', exp_type)
    print('Selected cutoff threshold: ', threshold)
    print("Loading data...")

    base_path = os.path.join(data_dir, 'analysis_files')
    folder_path = base_path + '/voxelization_and_analysis/voxel_size_' + str(voxel_size_sel) + '/smoothing'
    if not os.path.exists(folder_path):
        raise Exception('The selected folder does not exist!')
    
    # file_path = os.path.join(folder_path, 'voxel_grid_coords_3D_arr_indices.csv')

    # # Load the DataFrame with the voxel coordinates nad the symmetric voxels information.
    # voxel_grid_coords_sym = pd.read_csv(file_path, index_col=0)

    # # Transform some of the columns of the loaded dataframe for consistency.
    # voxel_grid_coords_sym['symmetric_vox_id'] = voxel_grid_coords_sym['symmetric_vox_id'].astype(object)
    # voxel_grid_coords_sym['3D_array_indices'] = voxel_grid_coords_sym['3D_array_indices'].apply(lambda x: ast.literal_eval(x))

    # Load the voxel activations counts.
    print('Loading the voxel activations file...')
    if work_with_batch_corrected:
        if include_initial_state:
            if exp_type == 'misting' and misting_with_adjusted_samples:
                file_name = 'dataframes_after_batch_correction_with_ZT00_' + 'misting_211110_samples_adjusted' + '_thresholding_' + str(threshold) + '.pkl'
            elif exp_type == 'no' or exp_type == 'misting':
                # file_name = 'dataframes_before_batch_correction_' + exp_type + '_ZT00_included' +'.pkl'
                file_name = 'dataframes_after_batch_correction_with_ZT00_' + exp_type + '_thresholding_' + str(threshold) + '.pkl'
            elif exp_type == 'darkno':
                file_name = 'dataframes_after_batch_correction_with_ZT12_' + exp_type + '_thresholding_' + str(threshold) + '.pkl'
        else:
            file_name = 'dataframes_after_batch_correction_' + exp_type + '_thresholding_' + str(threshold) + '.pkl'            
    else:
        if include_initial_state:
            if exp_type == 'no' or exp_type == 'misting':
                # file_name = 'dataframes_before_batch_correction_' + exp_type + '_ZT00_included' +'.pkl'
                file_name = 'dataframes_before_batch_correction_with_ZT00_' + exp_type + '_thresholding_' + str(threshold) + '.pkl'
            elif exp_type == 'darkno':
                file_name = 'dataframes_before_batch_correction_with_ZT12_' + exp_type + '_thresholding_' + str(threshold) + '.pkl'
        else:
            file_name = 'dataframes_before_batch_correction_' + exp_type + '_thresholding_' + str(threshold) + '.pkl'
            
    full_path = f'{folder_path}/{file_name}'

    with open(full_path, 'rb') as f:
        df_arr = pickle.load(f)

    # Load df names.
    if work_with_batch_corrected:
        if include_initial_state:
            if exp_type == 'no' or exp_type == 'misting':
                df_names_filename = os.path.join(folder_path, 'df_names_batch_corrected_' + exp_type + '_ZT00_included.npy')
            elif exp_type == 'darkno':
                df_names_filename = os.path.join(folder_path, 'df_names_batch_corrected_' + exp_type + '_ZT12_included.npy')
        else:
            df_names_filename = os.path.join(folder_path, 'df_names_batch_corrected_' + exp_type + '.npy')
    else:
        if include_initial_state:
            if exp_type == 'no' or exp_type == 'misting':
                df_names_filenam = os.path.join(folder_path, 'df_names_no_batch_corrected_' + exp_type + '_ZT00_included.npy')
            elif exp_type == 'darkno':
                df_names_filename = os.path.join(folder_path, 'df_names_no_batch_corrected_' + exp_type + '_ZT12_included.npy')
        else:
            df_names_filename = os.path.join(folder_path, 'df_names_no_batch_corrected_' + exp_type + '.npy')
        
    # df_names = np.load(base_path+'/df_names_' + exp_type + '.npy', allow_pickle=True)
    df_names = np.load(df_names_filename, allow_pickle=True)
    
    # Load the saved dataframes with average activations over time.
    # folder_name = 'analysis_files/voxelization_and_analysis/'+'voxel_size_' + str(voxel_size_sel)
    # base_path = os.path.join(DATA_DIR, folder_name, 'smoothing') # Add another folder or in the filenames the batch effect correction information.

    if work_with_batch_corrected:
        if include_initial_state:
            if exp_type == 'misting'  and misting_with_adjusted_samples:
                file_name = 'df_voxels_over_time_arr_batch_corrected_' + exp_type + '_ZT00_included_misting_211110_samples_adjusted' + '_thresholding_' + str(threshold) +  '.pkl'
            elif exp_type == 'no' or exp_type == 'misting':
                file_name = 'df_voxels_over_time_arr_batch_corrected_' + exp_type + '_ZT00_included' + '_thresholding_' + str(threshold) +  '.pkl'
            elif exp_type == 'darkno':
                file_name = 'df_voxels_over_time_arr_batch_corrected_' + exp_type + '_ZT12_included' + '_thresholding_' + str(threshold) + '.pkl'
        else:
            file_name = 'df_voxels_over_time_arr_batch_corrected_' + exp_type + '_thresholding_' + str(threshold) + '.pkl'
    else:
        if include_initial_state:
            if exp_type == 'no' or exp_type == 'misting':
                file_name = 'df_voxels_over_time_arr_' + exp_type + '_ZT00_included' + '_thresholding_' + str(threshold) +  '.pkl'
            elif exp_type == 'darkno':
                file_name = 'df_voxels_over_time_arr_' + exp_type + '_ZT12_included' + '_thresholding_' + str(threshold) + '.pkl'
        else:
            file_name = 'df_voxels_over_time_arr_' + exp_type + '_thresholding_' + str(threshold) + '.pkl'

    full_path = f'{folder_path}/{file_name}'
    with open(full_path, 'rb') as f:
        df_voxels_over_time_arr = pickle.load(f)

    if exp_type == 'circadian':
        conditions = ['ZT00', 'ZT03', 'ZT06', 'ZT09', 'ZT12', 'ZT15', 'ZT18', 'ZT21']
    elif exp_type == 'no' or exp_type == 'misting' or exp_type == 'darkno':
        if include_initial_state:
            if exp_type == 'no' or exp_type == 'misting':
                conditions = ['ZT00', 'SD1p5', 'SD3', 'SD5', 'SD6', 'R1p5', 'R3']
            elif exp_type == 'darkno':
                conditions = ['ZT12', 'SD1p5', 'SD3', 'SD5', 'SD6', 'R1p5', 'R3']
        else:
            conditions = ['SD1p5', 'SD3', 'SD5', 'SD6', 'R1p5', 'R3']
    print(conditions)
    
    selected_features = deepcopy(conditions)

    # Load the saved dataframes for the average variance over time.
    # folder_name = 'analysis_files/voxelization_and_analysis/'+'voxel_size_' + str(voxel_size)
    # base_path = os.path.join(DATA_DIR, folder_name, 'smoothing') # Add another folder or in the filenames the batch effect correction information.

    if work_with_batch_corrected:
        if include_initial_state:
            if exp_type == 'misting' and misting_with_adjusted_samples:
                file_name = 'df_voxels_over_time_arr_batch_corrected_' + exp_type + '_ZT00_included_var' + '_misting_211110_samples_adjusted' + '_thresholding_' + str(threshold) + '.pkl'
            elif exp_type == 'no' or exp_type == 'misting':
                file_name = 'df_voxels_over_time_arr_batch_corrected_' + exp_type + '_ZT00_included_var' + '_thresholding_' + str(threshold) + '.pkl'
            elif exp_type == 'darkno':
                file_name = 'df_voxels_over_time_arr_batch_corrected_' + exp_type + '_ZT12_included_var' + '_thresholding_' + str(threshold) + '.pkl'
        else:
            file_name = 'df_voxels_over_time_arr_var_batch_corrected_' + exp_type + '_thresholding_' + str(threshold) + '.pkl'
    else:
        if include_initial_state:
            if exp_type == 'no' or exp_type == 'misting':
                file_name = 'df_voxels_over_time_arr_' + exp_type + '_ZT00_included_var' + '_thresholding_' + str(threshold) + '.pkl'
            elif exp_type == 'darkno':
                file_name = 'df_voxels_over_time_arr_' + exp_type + '_ZT12_included_var' + '_thresholding_' + str(threshold) + '.pkl'
        else:
            file_name = 'df_voxels_over_time_arr_var_' + exp_type + '_thresholding_' + str(threshold) + '.pkl'

    full_path = f'{folder_path}/{file_name}'
    with open(full_path, 'rb') as f:
        df_voxels_over_time_arr_var = pickle.load(f)

    sel_ind_df = 0 # Select the dataframe to work with. 0: original data, 1: Log-transformed data.

    selected_voxels = list(df_voxels_over_time_arr[sel_ind_df].index)

    df_sel_ZT = df_voxels_over_time_arr[sel_ind_df].loc[selected_voxels, conditions].copy(deep=True)
    df_sel_vox = df_voxels_over_time_arr[sel_ind_df].loc[selected_voxels, conditions].copy(deep=True).T

    # scaler_conditions = StandardScaler()
    # Standardize for every feature.
    print('ZT std')
    df_sel_ZT_std = pd.DataFrame((df_sel_ZT[conditions]-df_sel_ZT[conditions].mean())/df_sel_ZT[conditions].std(), index=df_sel_ZT.index, columns=df_sel_ZT.columns) # Standardize for ZT condition.
    # scaler_voxels = StandardScaler()
    # Standardize for every voxel. Activation pattern over time matters.
    print('vox std')
    df_sel_vox_std = pd.DataFrame((df_sel_vox-df_sel_vox.mean())/df_sel_vox.std(), index=df_sel_vox.index, columns=df_sel_vox.columns) # Standardize for every voxel.

    df_arr_vox_clustering = []
    df_arr_vox_clustering.append(df_sel_vox) # raws ZT, cols: voxels for correlation computation
    df_arr_vox_clustering.append(df_sel_vox_std)

    df_names_clustering = []
    df_names_clustering.append('raw')
    df_names_clustering.append('vox std')

    sample_frac = 1 # Select the smapling percentage.

    threshold_df = 1e6

    df_sampled_arr = []

    for i in range(len(df_arr_vox_clustering)):
        if df_arr_vox_clustering[i].shape[1] > threshold_df:
            df_sampled = df_arr_vox_clustering[i].T.sample(frac=sample_frac)
            df_sampled_arr.append(df_sampled.T)
            
    print('Performing clustering...')
    clusters_num = 100000
    df_kmeans = minibatch_kmeans_clustering(df_sampled_arr, df_names_clustering, clusters_num, 10000)

    # Save.
    for i, df in enumerate(df_kmeans): # Save each DataFrame to a CSV file.
        if exp_type == 'misting' and misting_with_adjusted_samples:
            file_name = os.path.join(folder_path, 'exp_' + exp_type + '_voxels_size_' + str(voxel_size_sel) + '_211110_samples_adjusted' + '_thresholding_' + str(threshold) + '_' + ('_').join(df_names_clustering[i].split(' ')) + '_kmeans_results.csv')
        else:
            file_name = os.path.join(folder_path, 'exp_' + exp_type + '_voxels_size_' + str(voxel_size_sel) + '_threshold_' + str(threshold) + '_' + ('_').join(df_names_clustering[i].split(' ')) + '_kmeans_results.csv')
        print(file_name)
        df.to_csv(file_name, index=True)

    print("Done. All files were saved on {}".format(save_dir))

if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())