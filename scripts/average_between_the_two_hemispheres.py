"""This script computes the averaged activations for test mice brains between the left
and right hemisphere.

See the notebook `notebooks/03_1_d_voxel_analysis_batch_effect.ipynb` for context.

Usage example:
    python average_between_the_two_hemispheres --data_dir data/ --save_dir results/

After running the command above, the directory results/ will contain the
following files:
    
"""

import click
import os
import traceback
import numpy as np
from copy import deepcopy
import pandas as pd
from tqdm import tqdm

from sba import (
    dataset,
    plotting,
    point_cloud
)

@click.command()
@click.option('--data_dir',
              required=True,
              type=click.Path(dir_okay=True, exists=True),
              help='Path to the dataset directory')
@click.option('--save_dir',
              required=True,
              type=click.Path(dir_okay=True, exists=True),
              help='Path to the directory where to save the output')

def main(data_dir, save_dir):

    # Load transformed centroids table and anatomical table
    aad = dataset.AtlasActivationsDataset(data_dir)
    
    transformed_centroids_table = aad.load_tc_table(format_conditions=True)
    anatomical_table = aad.load_anatomical_boundaries_table(rebuild=False)
    anatomical_atlas = dataset.AnatomicalAtlas(anatomical_table)

    # Select a voxel size and experiment type to work with.
    voxel_size_sel = 1
    exp_type = 'darkno'
    include_initial_state = True
    if exp_type == 'circadian':
        include_initial_state = False
        
    # Load data.
    print('Selected voxel size: ', voxel_size_sel)
    print('Selected experimental type: ', exp_type)
    print("Loading data...")

    base_path = os.path.join(data_dir, 'analysis_files')
    folder_path = base_path + '/voxelization_and_analysis/voxel_size_' + str(voxel_size_sel)
    if not os.path.exists(folder_path):
        raise Exception('The selected folder does not exist!')
    
    # Load the voxel activations counts.
    if include_initial_state:
        if exp_type == 'no' or exp_type == 'misting':
            filename = folder_path + "/voxel_activation_counts_exp_type_" + str(exp_type) + '_ZT00_included' + ".csv"
        elif exp_type == 'darkno':
            filename = folder_path + "/voxel_activation_counts_exp_type_" + str(exp_type) + '_ZT12_included' + ".csv"
    else:
        filename = folder_path + "/voxel_activation_counts_exp_type_" + str(exp_type) + ".csv"
    voxel_activation_counts_df = pd.read_csv(filename, index_col = 0)
    
    # Load the voxel coordianates file.
    if include_initial_state:
        if exp_type == 'no' or exp_type == 'misting':
            filename = folder_path + "/voxel_grid_coords_exp_type_" + str(exp_type) + '_ZT00_included' + ".csv"
        elif exp_type == 'darkno':
            filename = folder_path + "/voxel_grid_coords_exp_type_" + str(exp_type) + '_ZT12_included' + ".csv"
    else:
        filename = folder_path + "/voxel_grid_coords_exp_type_" + str(exp_type) + ".csv"
    voxel_grid_coords = pd.read_csv(filename, index_col = 0)

    atlas_bounds_min = anatomical_table[['x','y','z']].min().to_numpy()
    atlas_bounds_max = anatomical_table[['x','y','z']].max().to_numpy()
    
    epsilon=0.5
    if voxel_size_sel == 1: # Throw away the voxels that are outside of the atlas coordinates.
        voxel_ids_to_keep = voxel_grid_coords[(voxel_grid_coords['x'] >= atlas_bounds_min[0]-epsilon) & (voxel_grid_coords['x'] <= atlas_bounds_max[0]+epsilon) &
            (voxel_grid_coords['y'] >= atlas_bounds_min[1]-epsilon) & (voxel_grid_coords['y'] <= atlas_bounds_max[1]+epsilon) &
            (voxel_grid_coords['z'] >= atlas_bounds_min[2]-epsilon) & (voxel_grid_coords['z'] <= atlas_bounds_max[2]+epsilon)].index
    
    voxel_activation_counts_df = voxel_activation_counts_df.T
    voxel_activation_counts_df = voxel_activation_counts_df[voxel_ids_to_keep]
    voxel_grid_coords = voxel_grid_coords.loc[voxel_ids_to_keep]

    mean_x_value = voxel_grid_coords['x'].min() + (voxel_grid_coords['x'].max() - voxel_grid_coords['x'].min())/2

    # Find the symmetric voxel (on the x-axis) for every voxel id.

    prev_x = -float('inf')
    sorted_list_x_values = sorted(voxel_grid_coords['x'].unique())
    voxel_grid_coords['symmetric_vox_id'] = None
                                
    for vox_x_ind, vox_x in tqdm(enumerate(sorted_list_x_values), total=len(sorted_list_x_values)):
        if prev_x > mean_x_value+voxel_size_sel: # Need to check if odd number of voxels on x-axis.
            print(prev_x, vox_x, vox_x_ind)
            break
        
        voxel_grid_coords.loc[voxel_grid_coords[voxel_grid_coords['x']==vox_x].index, 'symmetric_vox_id'] = voxel_grid_coords[voxel_grid_coords['x']==sorted_list_x_values[len(sorted_list_x_values)-vox_x_ind-1]].index.to_list()
        voxel_grid_coords.loc\
        [voxel_grid_coords[voxel_grid_coords['x']==sorted_list_x_values[len(sorted_list_x_values)-vox_x_ind-1]].index, 'symmetric_vox_id'] = voxel_grid_coords[voxel_grid_coords['x']==vox_x].index.to_list()
        
        prev_x = vox_x
        
    
    voxel_activation_counts_df_avg = voxel_activation_counts_df.copy(deep=True)

    voxel_activation_counts_df_avg.loc[:, :] = -1

    # Average the activations between the two hemispheres.
    prev_x = -float('inf')

    for index, row in voxel_activation_counts_df.iterrows(): # Iterate over samples.
        print(index)
        for vox_x_ind, vox_x in tqdm(enumerate(sorted_list_x_values), total=len(sorted_list_x_values)): # Iterate over the x-values.
            if prev_x > mean_x_value+voxel_size_sel: # Need to check if odd number of voxels on x-axis.
                print(prev_x, vox_x, vox_x_ind)
                break
            for vox_coords_index, vox_coords_row in voxel_grid_coords[voxel_grid_coords['x']==vox_x].iterrows(): # Iterate over the voxel indices with that x-value.
                voxel_activation_counts_df_avg.loc[index, vox_coords_index] = (voxel_activation_counts_df.loc[index, vox_coords_index]+\
                    voxel_activation_counts_df.loc[index, vox_coords_row['symmetric_vox_id']])/2
                voxel_activation_counts_df_avg.loc[index, vox_coords_row['symmetric_vox_id']] = (voxel_activation_counts_df.loc[index, vox_coords_index]+\
                    voxel_activation_counts_df.loc[index, vox_coords_row['symmetric_vox_id']])/2

            prev_x = vox_x

    # Save.
    if include_initial_state:
        if exp_type=='no' or exp_type=='misting':
            save_name = os.path.join(
                save_dir,
                'voxel_activation_counts_df_avg_' + exp_type + '_ZT00_included' + '.csv',
            )
        elif exp_type=='darkno':
            save_name = os.path.join(
                save_dir,
                'voxel_activation_counts_df_avg_' + exp_type + '_ZT12_included' + '.csv',
            )
    else:
        save_name = os.path.join(
                save_dir,
                'voxel_activation_counts_df_avg_' + exp_type + '.csv',
            )

    voxel_activation_counts_df_avg.to_csv(save_name, index=True)

    print("Done. All the plots were saved on {}".format(save_dir))

if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())