"""Prepare voxel selection files for the Brain Atlas View app."""

import json
import os
import shutil
import traceback
import warnings
from glob import glob

import chromadb
import click
import nrrd
import numpy as np
import pandas as pd

from sba import dataset, utils


def _copy_file(src_path, dst_dir):
    """Copy file from src to dst."""
    shutil.copy(src_path, dst_dir)


def _rename_file(src_path, dst_path):
    """Rename file from src to dst."""
    shutil.move(src_path, dst_path)


def _experiment_names():
    return ["circadian", "darkno", "misting", "no"]


def _process_structure_volumes(data_dir, save_dir):
    """Estimate the volume of each anatomical structure in the atlas and save results to a CSV file."""
    anatomical_table = pd.read_csv(
        os.path.join(data_dir, "atlas_files", "anatomical_table.csv")
    )
    # Since the anatomical structures are annotated on a regular grid of with spacing
    # of one atlas unit, the volume of each region is simply the number of points that
    # are annotated with that region.
    volumes = (
        anatomical_table.loc[:, ["name", "id", "acronym"]]
        .value_counts()
        .to_frame("volume")
    )
    # Turn MultiIndex into a simple index
    volumes = volumes.reset_index()
    # Save the dataframe to a CSV file, ignore index
    volumes.to_csv(
        os.path.join(save_dir, "anatomical_structure_volumes.csv"), index=False
    )


def _dir_from_voxel_size(data_dir, voxel_size):
    base_dir = os.path.join(data_dir, "analysis_files", "voxelization_and_analysis")
    return os.path.join(base_dir, "voxel_size_{}".format(voxel_size))


def _compute_slice_hist(tensor, slice_type):
    if slice_type == "x":
        summing_axes = (1, 2)
    elif slice_type == "y":
        summing_axes = (0, 2)
    elif slice_type == "z":
        summing_axes = (0, 1)
    else:
        raise ValueError("slice_coord must be one of 'x', 'y', or 'z'")

    # Sum over the spatial axes orthogonal to the slice
    slice_sums = np.nansum(tensor, axis=summing_axes)

    # Normalize so that the maximum is 1
    slice_sums = slice_sums / np.nanmax(slice_sums)

    # Return list of simple floats so that it can be serialized to JSON
    slice_sums = [val.item() for val in slice_sums]
    return slice_sums


def _process_activation_tensors(data_dir, save_dir, voxel_sizes):
    time_profiles = {}
    slice_hists = {}

    for voxel_size in voxel_sizes:
        exp_names = _experiment_names()
        time_profiles[voxel_size] = {}
        slice_hists[voxel_size] = {}

        for exp_name in exp_names:
            # Copy the 4D (space + time) experiment tensor to the save directory
            file_name = "voxel_signal_{}_raw.nrrd".format(exp_name)
            file_path = os.path.join(
                _dir_from_voxel_size(data_dir, voxel_size), "nrrd_files", file_name
            )
            _copy_file(file_path, save_dir)

            # Rename it to the BAV app convention
            new_name = "voxel_size_{}_{}_signal.nrrd".format(voxel_size, exp_name)
            _rename_file(
                os.path.join(save_dir, file_name), os.path.join(save_dir, new_name)
            )

            # Load the experiment tensor to compute summaries of it
            try:
                exp_tensor, _ = nrrd.read(file_path)
            except FileNotFoundError:
                warnings.warn(
                    "Activation tensor file not found for voxel size {}, experiment {}. Skipping...".format(
                        voxel_size, exp_name
                    ),
                    stacklevel=1,
                )
                continue

            # Save a version of the tensor summed over the time axis (avoids repeated
            # computation in the app)
            exp_tensor_summed_over_time = np.sum(exp_tensor, axis=-1)
            summed_tensor_path = os.path.join(
                save_dir,
                "voxel_size_{}_{}_signal_summed_over_time.nrrd".format(
                    voxel_size, exp_name
                ),
            )
            nrrd.write(summed_tensor_path, exp_tensor_summed_over_time)

            # Get the global time profile of the experiment
            global_time_profile = np.nansum(exp_tensor, axis=(0, 1, 2))
            time_profiles[voxel_size][exp_name] = [
                val.item() for val in global_time_profile
            ]

            # Get the x-, y-, and z-slice histograms
            slice_hists[voxel_size][exp_name] = {}
            slice_hists[voxel_size][exp_name]["x"] = _compute_slice_hist(
                exp_tensor_summed_over_time, "x"
            )
            slice_hists[voxel_size][exp_name]["y"] = _compute_slice_hist(
                exp_tensor_summed_over_time, "y"
            )
            slice_hists[voxel_size][exp_name]["z"] = _compute_slice_hist(
                exp_tensor_summed_over_time, "z"
            )

    # Save the time profiles and slice histograms to JSON
    time_profiles_path = os.path.join(save_dir, "global_time_profiles.json")
    with open(os.path.join(save_dir, time_profiles_path), "w") as f:
        json.dump(time_profiles, f, indent=4)

    slice_hists_path = os.path.join(save_dir, "signal_density_per_slice.json")
    with open(os.path.join(save_dir, slice_hists_path), "w") as f:
        json.dump(slice_hists, f, indent=4)


def _load_time_profile__and_clusters(tpc_path):
    time_profiles_and_clusters = pd.read_csv(tpc_path, index_col=0)
    # Convert to int32 to save on memory (we have about 100k different clusters)
    time_profiles_and_clusters["cluster"] = time_profiles_and_clusters[
        "cluster"
    ].astype("int32")
    return time_profiles_and_clusters


def _compute_time_profile_centroids(time_profiles_and_clusters):
    # Compute the mean time profile for each cluster and store them in a separate
    # table
    centroids_and_clusters = time_profiles_and_clusters.groupby("cluster").agg("mean")
    # Now clusters are the index, so we convert the rest of the table to float16 (6
    # decimal place precision) to save on memory
    centroids_and_clusters = centroids_and_clusters.astype("float16")
    return centroids_and_clusters


def _add_exp_collection(client, centroids_and_clusters, exp_name, voxel_size):
    # Try to delete the collection if it already exists
    name = "voxel_size_{}_{}".format(voxel_size, exp_name)
    try:
        client.delete_collection(name)
    except ValueError:
        pass
    # Create a new collection
    collection = client.create_collection(name=name, metadata={"hnsw:space": "l2"})

    # Normalize the time profile data so that the largest possible value is 1.
    # This makes it easier to interpret and set up the query time profiles.
    data = centroids_and_clusters.div(centroids_and_clusters.max(axis=1), axis=0)

    # Remove rows with NaN values
    data = data.dropna()

    # Add records in batches of client.get_max_batch_size()
    batch_size = client.get_max_batch_size()
    batch = data.iloc[:batch_size, :]
    n_processed = 0
    while len(batch) > 0:
        collection.add(
            embeddings=batch.values,
            ids=["cluster_" + str(idx) for idx in batch.index],
        )
        n_processed += len(batch)
        batch = data.iloc[n_processed : n_processed + batch_size, :]


def _load_voxel_coords(vc_path):
    voxel_coords = pd.read_csv(vc_path, index_col=0)
    # Convert columns to int16 to play well with the data tensor and to save memory
    # (the atlas coordinates are bounded within a cube of size about 500)
    voxel_coords = voxel_coords.astype("int16")
    return voxel_coords


def _merge_coords_and_clusters(voxel_coords, time_profiles_and_clusters):
    # Merge tables on index to connect voxel id to cluster number
    coords_and_cluster = voxel_coords.join(time_profiles_and_clusters["cluster"])

    # Drop rows without cluster assignment
    coords_and_cluster = coords_and_cluster.dropna(how="any")

    # Convert to int32 to save on memory (we have about 100k different clusters)
    coords_and_cluster["cluster"] = coords_and_cluster["cluster"].astype("int32")

    return coords_and_cluster


def _make_cluster_tensor(coords_and_cluster, atlas_shape, save_path):
    # Drop coordinates outside the brain limits
    def filter_by_brain_limits(df, atlas_shape):
        for i, coord in enumerate(["x", "y", "z"]):
            limits = [0, atlas_shape[i]]
            df = df[(df[coord] >= limits[0]) & (df[coord] <= limits[1])]
        return df

    coords_and_cluster = filter_by_brain_limits(coords_and_cluster, atlas_shape)

    # Build cluster tensor
    cluster_tensor = np.zeros(atlas_shape, dtype=np.int32)
    cluster_grouping = coords_and_cluster.groupby("cluster")
    for cluster, df in cluster_grouping:
        coords = df.loc[:, "x":"z"].values
        cluster_tensor[coords[:, 0], coords[:, 1], coords[:, 2]] = cluster

    # Save the tensor to an nrrd file
    nrrd.write(save_path, cluster_tensor)


def _build_time_profile_dbs(data_dir, save_dir, voxel_sizes, atlas_shape):
    """Build vector databases for the (clustered) voxel time profiles of each experiment."""

    db_dir = os.path.join(save_dir, "time_profile_db")
    # Delete the database directory if it already exists
    # (folders with random names are created at each run, so if we don't do this we
    # keep accumulating garbage in bav_files/time_profile_db)
    if os.path.exists(db_dir):
        shutil.rmtree(db_dir)
    client = chromadb.PersistentClient(path=db_dir)

    for voxel_size in voxel_sizes:
        voxel_size_dir = _dir_from_voxel_size(data_dir, voxel_size)

        for exp_name in _experiment_names():
            try:
                # Load the time profile and clusters table
                tpc_path = glob(
                    os.path.join(voxel_size_dir, "smoothing")
                    + "/exp_{}_*_raw_kmeans_results.csv".format(exp_name)
                )[0]
                time_profiles_and_clusters = _load_time_profile__and_clusters(tpc_path)

                # Load the voxel coordinates table
                vc_path = os.path.join(
                    voxel_size_dir, "voxel_grid_coords_exp_type_{}.csv".format(exp_name)
                )
                voxel_coords = _load_voxel_coords(vc_path)

            except (IndexError, FileNotFoundError):
                warnings.warn(
                    "Database-creating files not found for voxel size {}, experiment {}. Skipping...".format(
                        voxel_size, exp_name
                    ),
                    stacklevel=1,
                )
                continue

            print(
                "Adding time profile collection of {} experiment (voxel size {}) to the database...".format(
                    exp_name, voxel_size
                )
            )

            # Add the experiment's time profiles as a separate collection in the vector
            # database
            centroids_and_clusters = _compute_time_profile_centroids(
                time_profiles_and_clusters
            )
            _add_exp_collection(client, centroids_and_clusters, exp_name, voxel_size)

            # Merge voxel coordinates with cluster assignments into a single table
            coords_and_cluster = _merge_coords_and_clusters(
                voxel_coords, time_profiles_and_clusters
            )

            # Make tensor linking voxel coordinates to time profile cluster, and add it
            # to the database folder
            tensor_save_path = os.path.join(
                db_dir,
                "voxel_size_{}_{}_cluster_tensor.nrrd".format(voxel_size, exp_name),
            )
            _make_cluster_tensor(coords_and_cluster, atlas_shape, tensor_save_path)

    print("Database created on {}".format(db_dir))
    print("DB collections: {}".format(client.list_collections()))


def _process_hierarchical_clustering_files(data_dir, save_dir, voxel_size=1):
    base_dir = os.path.join(
        data_dir,
        "analysis_files",
        "voxelization_and_analysis",
        "voxel_size_{}".format(voxel_size),
        "clustering",
        "nrrd_files",
        "smoothing_cutoff_threshold_0",
    )

    # Create a directory to save the files (if it doesn't exist)
    utils.makedir(save_dir)

    for exp_name in _experiment_names():
        peak_dirs = glob(
            os.path.join(
                base_dir,
                "exp_type_{}".format(exp_name),
                "segmented_cluster",
                "peaks_*",
            )
        )
        peak_names = [os.path.basename(peak_dir) for peak_dir in peak_dirs]
        path_suffix = os.path.join(
            "all_regimes_together", "Euclidean_distance", "normalized_values"
        )
        for i, peak_dir in enumerate(peak_dirs):
            try:
                leaves_tensor_path = glob(
                    os.path.join(
                        peak_dir,
                        path_suffix,
                        "voxel_info_*raw.nrrd",
                    )
                )[0]
            except IndexError:
                # There is no file with the agreed upon naming convention in there
                leaves_tensor_path = None
            try:
                tree_info_path = glob(
                    os.path.join(
                        peak_dir,
                        path_suffix,
                        "df_tree.csv",
                    )
                )[0]
            except IndexError:
                # There is no file with the agreed upon naming convention in there
                tree_info_path = None

            # Copy files to the save directory if they exist, renaming them to our
            # convention
            if leaves_tensor_path:
                _copy_file(leaves_tensor_path, save_dir)
                new_name = "voxel_size_{}_{}_{}_leaves_tensor.nrrd".format(
                    voxel_size, exp_name, peak_names[i]
                )
                _rename_file(
                    os.path.join(save_dir, os.path.basename(leaves_tensor_path)),
                    os.path.join(save_dir, new_name),
                )
            if tree_info_path:
                _copy_file(tree_info_path, save_dir)
                new_name = "voxel_size_{}_{}_{}_tree_info.csv".format(
                    voxel_size, exp_name, peak_names[i]
                )
                _rename_file(
                    os.path.join(save_dir, os.path.basename(tree_info_path)),
                    os.path.join(save_dir, new_name),
                )


def _copy_tif_as_nrrd(tensor_path, save_dir):
    # Get the file name and extension
    file_name, file_ext = os.path.splitext(tensor_path)
    if file_ext == ".tif" or file_ext == ".tiff":
        tif_arr = utils.load_3d_tiff(tensor_path)
        new_name = os.path.basename(file_name) + ".nrrd"
        nrrd.write(os.path.join(save_dir, new_name), tif_arr)
    else:
        raise ValueError("File extension not recognized: {}".format(file_ext))


@click.command()
@click.option(
    "--data_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=True, resolve_path=True),
    help="Path to the dataset directory",
)
@click.option(
    "--save_dir",
    required=True,
    type=click.Path(dir_okay=True, exists=False),
    help="Path to the directory onto  which save the output files",
)
@click.option(
    "--voxel_sizes",
    "-vxs",
    required=True,
    type=int,
    multiple=True,
    help="Voxel sizes to process",
)
def main(data_dir, save_dir, voxel_sizes):
    print("Preparing files for Brain Atlas View app...")

    # Create the output directory if it does not exist
    utils.makedir(save_dir)

    # Create subdirectory `activation_files` on `save_dir`
    utils.makedir(os.path.join(save_dir, "activation_files"))

    # Copy atlas files from `data_dir/atlas_files` to `save_dir`
    print("Copying atlas files...")
    atlas_files = os.path.join(data_dir, "atlas_files")
    _copy_file(os.path.join(atlas_files, "anatomical_boundary_table.csv"), save_dir)
    _copy_file(os.path.join(atlas_files, "brain_outline_table.csv"), save_dir)
    _copy_tif_as_nrrd(os.path.join(atlas_files, "Gubra_Annotation_NoOB.tif"), save_dir)
    _copy_file(os.path.join(atlas_files, "Gubra_Template_NoOB.nrrd"), save_dir)
    _copy_file(
        os.path.join(atlas_files, "Gubra_Annotation_NoOB_boundaries_filled.nrrd"),
        save_dir,
    )

    # Estimate the volume of each anatomical structure and save results to a CSV file
    _process_structure_volumes(data_dir, save_dir)

    # Copy classifier of points into anatomical structures
    _copy_file(
        os.path.join(
            data_dir,
            "atlas_files",
            "anat_struct_classifier",
            "anat_struct_classifier.joblib",
        ),
        save_dir,
    )

    # Make sure voxel sizes is a list
    voxel_sizes = list(voxel_sizes)

    # Process activation tensors
    print("Processing activation tensors...")
    _process_activation_tensors(
        data_dir, os.path.join(save_dir, "activation_files"), voxel_sizes
    )

    # Make vector databases for the time profiles of each experiment
    print("Building time profile databases...")
    aad = dataset.AtlasActivationsDataset(data_dir)
    anatomical_array = aad.load_anatomical_array()
    _build_time_profile_dbs(
        data_dir,
        os.path.join(save_dir, "activation_files"),
        voxel_sizes,
        atlas_shape=anatomical_array.shape,
    )

    # Process files from the hierarchical clustering analysis from time profile peak
    # behaviors
    print("Processing hierarchical clustering analysis files...")
    _process_hierarchical_clustering_files(
        data_dir, os.path.join(save_dir, "activation_files", "hierarchical_clustering")
    )

    print("Done. Results were saved on {}".format(save_dir))


if __name__ == "__main__":
    try:
        main()
    except Exception:
        print("***** ERROR *****")
        print(traceback.format_exc())
