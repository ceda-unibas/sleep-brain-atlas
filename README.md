# sleep-brain-atlas

Analyzing the influence of sleep-deprivation on the patterns of activity in the brain of mice

[![Project Badge](https://img.shields.io/badge/Project%20status-Ongoing-green)][project-page]

## Installation

Create a new conda environment for this project with conda:

```sh
conda env create -f environment.yml
```

Activate the new environment with `conda activate sba` (or `source activate sba`, depending on your system) and install the necessary dependencies with

```sh
pip install .
```

If you are working in another environment, you can install the package from its GitLab archive with

```sh
pip install git+https://gitlab.com/ceda-unibas/sleep-brain-atlas/-/archive/main/sleep-brain-atlas-main.tar.gz
```

Note that further dependencies may need to be installed if you want to run the notebooks and/or scripts in the [`notebooks`][notebooks] and [`scripts`][scripts] directories. For instructions on how to install these dependencies, please refer to the README files therein.

## Usage

Once the environment is setup and the dependencies are installed, you can use the sba package in your Python code. For example,

```python
from sba import dataset
aad = dataset.AtlasActivationsDataset('path/to/dataset')
```

See [`notebooks`][notebooks] and the [`scripts`][scripts] for examples and detailed use cases. The notebooks in particular are numbered in increasing order of complexity, going from loading and displaying data to more specific analyses.

## Documentation

The documentation for this project is available at [ceda-unibas.gitlab.io/sleep-brain-atlas/](https://ceda-unibas.gitlab.io/sleep-brain-atlas/).

## Contributing

Contributions are welcome, see [CONTRIBUTING][contributing].

## Authors and acknowledgment

This project is a result of a collaboration between [CeDA][ceda] and [Alex Schier's research group][schier] at the University of Basel.

## License

This project is released under the [BSD 3-Clause License][license].

[ceda]: https://ceda.unibas.ch/
[contributing]: CONTRIBUTING.md
[schier]: https://www.biozentrum.unibas.ch/research/research-groups/research-groups-a-z/overview/unit/research-group-alex-schier
[license]: LICENSE
[notebooks]: https://gitlab.com/ceda-unibas/sleep-brain-atlas/-/tree/main/notebooks
[scripts]: https://gitlab.com/ceda-unibas/sleep-brain-atlas/-/tree/main/scripts
[project-page]: https://ceda.unibas.ch/project_sleep_brain_atlas.html
