"""Utilities for the notebooks"""

import io
import os
import random
from copy import deepcopy
from tqdm import tqdm

import fitz
import matplotlib.patches as mpatches  # For creating custom legend patches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import seaborn as sns
import umap
from minisom import MiniSom
from PIL import Image
from scipy.spatial import KDTree
import plotly.express as px

# from skfuzzy import cluster
import sklearn
from sklearn.preprocessing import StandardScaler
from skfuzzy.cluster import cmeans
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.metrics import silhouette_score
from sklearn.mixture import GaussianMixture
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KernelDensity
from sklearn.cluster import DBSCAN
from scipy.cluster.hierarchy import linkage
from scipy.cluster.hierarchy import fcluster
from scipy.spatial.distance import pdist, squareform
from scipy.cluster.hierarchy import dendrogram
from matplotlib.offsetbox import OffsetImage, AnnotationBbox

from io import BytesIO
from PIL import Image
import nrrd
from skimage.morphology import dilation

import ot
from shapely.geometry import Polygon
from joblib import Parallel, delayed

from sba import (
    dataset,
    plotting,
    utils
)


def plot_anatomical_annotations_side_by_side(aad, z_slice=125):
    """Plot anatomical annotations side by side.

    Grid annotations on the right, boundaries in the middle, and
    boundary-masked grid annotations on the left.

    Parameters
    ----------
    aad : `sba.dataset.AtlasActivationsDataset`
        An AtlasActivationsDataset object
    z_slice : int, optional
        The slice of the z-axis to plot, by default 125

    Returns
    -------
    `matplotlib` Figure
        The figure object where the plot was made
    `matplotlib` Axes
        The axes object where the plot was made
    """

    # Load TIFF file as a 3D array
    grid_annotations = aad.load_anatomical_array()

    # Load NRRD file as a 3D array
    boundaries = aad.load_anatomical_boundaries_array()

    # Create boundary annotations by masking the grid annotations with the
    # boundaries
    boundary_annotations = (grid_annotations * boundaries).astype(int)

    # Create a figure with three subplots in one row
    fig, ax = plt.subplots(1, 3, figsize=(15, 5))

    # Plot the grid annotations
    ax[0].imshow(grid_annotations[..., z_slice], cmap="tab10")
    ax[0].set_title("Grid annotations")
    ax[0].axis("off")

    # Plot the boundaries
    ax[1].imshow(boundaries[..., z_slice], cmap="gray")
    ax[1].set_title("Boundaries")
    ax[1].axis("off")

    # Plot the boundary annotations
    ax[2].imshow(boundary_annotations[..., z_slice], cmap="tab10")
    ax[2].set_title("Boundary annotations")
    ax[2].axis("off")

    return fig, ax


def compute_Hausdorff_distance(set1_points, set2_points):
    """Computes the Haussdorff distance of two point-clouds.

    Parameters
    ----------
    set1_points : `numpy` array
        Contains the X, Y, Z coordinates of the first point-cloud
    set2_points : `numpy` array
        Contains the X, Y, Z coordinates of the second point-cloud

    Returns
    -------
    float
        The Hausdorff distance of the two point-clouds

    """

    # Compute the distance from each point in cloud1 to the closest point in
    # cloud2
    tree2 = KDTree(set1_points)
    distances1, _ = tree2.query(set2_points)

    # Compute the maximum distance from cloud1 to cloud2
    max_distance1 = np.max(distances1)

    # Compute the distance from each point in cloud2 to the closest point in
    # cloud1
    tree1 = KDTree(set2_points)
    distances2, _ = tree1.query(set1_points)

    # Compute the maximum distance from cloud2 to cloud1
    max_distance2 = np.max(distances2)

    # Compute the Hausdorff distance as the maximum of the two maximum
    # distances
    hausdorff_distance = max(max_distance1, max_distance2)

    return hausdorff_distance


def remove_prefix_and_sort(prefix, arr):
    """Gets a list with alphanumerical values and sorts it
    without taking into account the not-numeric prefix.

    Parameters
    ----------
    prefix : str
        The common prefix in all entries of list `arr`
    arr : list
        A list containing the elements to be sorted

    Returns
    -------
    list
        The list after being sorted according to numerical part.
    """
    res = deepcopy(arr)

    res = [int(x.removeprefix(prefix)) for x in res]
    res = sorted(res)
    res = [prefix + str(x) for x in res]

    return res


def run_kmeans(k, scaled_points):
    """Runs kMeans algorithm.

    Parameters
    ----------
    k : float
        The number of clusters
    scaled_points : `numpy` array
        The data points to be clusters

    Returns
    -------
    tuple
        A tuple whose elements are{}
        - The kMeans model
        - A list with the labels.
        - The Silhouette score.
    """
    km = KMeans(n_clusters=k, init="k-means++", max_iter=300, n_init=10, random_state=0)

    km.fit(scaled_points)
    km_clusters = km.predict(scaled_points)
    s_res = silhouette_score(scaled_points, km_clusters)

    # bic = gm.bic(df_test_std[reduced_features])

    return km, km_clusters, s_res


def plot_metrics_km(silhouette_scores, df, features, clusters, models_km):
    fig, ax = plt.subplots(4, 1, figsize=(25, 25))

    # Silhouette scores: check how compact and well-separated are the clusters
    ax[0].plot(np.arange(2, len(silhouette_scores) + 2), silhouette_scores, "bx-")
    ax[0].set_xlabel("Number of clusters")
    ax[0].set_ylabel("Silhouette score")
    ax[0].set_title("Silhouette method (higher better)")

    inertia_scores = []
    for i in range(len(models_km)):
        inertia_scores.append(models_km[i].inertia_)

    ax[1].plot(np.arange(2, len(silhouette_scores) + 2), inertia_scores, "bx-")
    ax[1].set_xlabel("Number of clusters")
    ax[1].set_ylabel("Inertia")
    ax[1].set_title("Inertia")

    # df['clusters'] = clusters[:]

    cl = []
    db = []
    for i in range(len(models_km)):
        cl.append(sklearn.metrics.calinski_harabasz_score(df[features], clusters[i]))
        db.append(sklearn.metrics.davies_bouldin_score(df[features], clusters[i]))

    ax[2].plot(np.arange(2, len(silhouette_scores) + 2), cl, "bx-")
    ax[2].set_xlabel("Number of clusters")
    ax[2].set_ylabel("Calinski-Harabasz")
    ax[2].set_title("Calinski-Harabasz (higher better)")

    ax[3].plot(np.arange(2, len(silhouette_scores) + 2), db, "bx-")
    ax[3].set_xlabel("Number of clusters")
    ax[3].set_ylabel("Davies-Bouldin")
    ax[3].set_title("Davies-Bouldin (smaller better)")

    # Other metrics: connectivity, Dunn index

    return fig, ax


def print_cluster_info(df, st, mn, features, log=False):
    """Print the cluster information and return the rescaled data frame
    labeled

    Parameters
    ----------
    df : `pandas.DataFrame`
        The labeled data frame
    st : list
        The scales with which the features were standardized.
    mn : list
        The mean values of the features before standardization.
    features : list
        The features of the data frame.
    comp : float
        The number of the principal components

    Returns
    -------
    `pandas.DataFrame`
        The rescaled and labeled data frame.
    """

    df_test_rescaled = df.copy(deep=True)

    # Re-scale data frame features back to normal values.
    for i in range(len(st)):
        if log is True:  # in case the features are log-transformed
            df_test_rescaled[features[i]] = np.exp(
                df_test_rescaled[features[i]] * st[i] + mn[i]
            )
        else:  # in case the features are not log-transformed
            df_test_rescaled[features[i]] = (
                df_test_rescaled[features[i]] * st[i] + mn[i]
            )

    cluster_counts = df_test_rescaled.groupby("clusters")[features].count()
    cluster_means = df_test_rescaled.groupby("clusters")[features].mean()
    cluster_std = df_test_rescaled.groupby("clusters")[features].std()
    cluster_max = df_test_rescaled.groupby("clusters")[features].max()
    cluster_min = df_test_rescaled.groupby("clusters")[features].min()

    cluster_counts_std = df.groupby("clusters")[features].count()
    cluster_means_std = df.groupby("clusters")[features].mean()
    cluster_std_std = df.groupby("clusters")[features].std()
    cluster_max_std = df.groupby("clusters")[features].max()
    cluster_min_std = df.groupby("clusters")[features].min()

    print("Cluster counts")
    print(cluster_counts)
    print("Cluster means")
    print(cluster_means)
    print("Cluster std")
    print(cluster_std)
    print("Cluster max")
    print(cluster_max)
    print("Cluster min")
    print(cluster_min)

    print("Std results")
    print("Cluster counts")
    print(cluster_counts_std)
    print("Cluster means")
    print(cluster_means_std)
    print("Cluster std")
    print(cluster_std_std)
    print("Cluster max")
    print(cluster_max_std)
    print("Cluster min")
    print(cluster_min_std)

    return df_test_rescaled


def compute_KDE(df, features, cv_opt=True, bdwidth=10):
    """Perform Kernel Density Estimation (KDE) for the cell positions in space
    for the stable states."""

    X = df[features].to_numpy()

    # Perform cross-validation to select the optimal bandwidth value
    if cv_opt:
        grid = GridSearchCV(
            KernelDensity(), {"bandwidth": np.linspace(0.05, 30, 80)}, n_jobs=-1, cv=5
        )
        grid.fit(X)
        bandwidth = grid.best_params_["bandwidth"]
    else:
        bandwidth = bdwidth

    # Fit a kernel density estimator using the optimal bandwidth value.
    kde = KernelDensity(bandwidth=bandwidth).fit(X)

    # Evaluate the KDE on a grid of points for visualization.
    log_dens = kde.score_samples(X)
    dens = np.exp(log_dens)

    # Generate a grid of points to evaluate the density function.
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    z_min, z_max = X[:, 2].min() - 1, X[:, 2].max() + 1
    xx, yy, zz = np.mgrid[
        x_min:x_max:30j,  # Change to 20 instead of 100?
        y_min:y_max:30j,
        z_min:z_max:30j,
    ]
    grid = np.c_[xx.ravel(), yy.ravel(), zz.ravel()]

    log_dens = kde.score_samples(grid)
    dens = np.exp(log_dens)

    # Compute the mean integrated squared error (MISE)
    # mise = np.trapz((dens - true_dens)**2, x=x_grid)

    print("Best bandwidth:", bandwidth)

    return kde, grid, dens


def plot_kde_3D(grids, density_scores):
    """Creates a 3D plot of the estimated density for a given df containing
    the activated brain cells of a mouse.

    Parameters
    ----------
    grids : `numpy` array
        The 3D coordinates of the grid.
    density_scores : `numpy` array
        The density values of the points.

    Returns
    -------
    `plotly` Figure
        The figure object where the plot was made
    """
    fig = go.Figure(
        data=[
            go.Volume(
                x=grids[3][:, 0].flatten(),
                y=grids[3][:, 1].flatten(),
                z=grids[3][:, 2].flatten(),
                value=density_scores[3].flatten(),
                isomin=density_scores[3].min(),
                isomax=density_scores[3].max(),
                opacity=0.1,
                surface_count=51,
                surface=dict(count=51),
            )
        ]
    )

    fig.update_layout(
        scene=dict(
            xaxis=dict(range=[grids[3][:, 0].min(), grids[3][:, 0].max()]),
            yaxis=dict(range=[grids[3][:, 0].min(), grids[3][:, 0].max()]),
            zaxis=dict(range=[grids[3][:, 0].min(), grids[3][:, 0].max()]),
        ),
        width=800,
        height=800,
    )

    fig.show()

    return fig


def plot_coord_statistics(
    stats_per_condition, axis, width=800, height=600, x_axis_title="Condition"
):
    """Plots the statistics of a given axis for each condition.

    Parameters
    ----------
    stats_per_condition : dict
        Dictionary containing the statistics of the axis for each condition.
        Each value should be the `pandas.DataFrame` returned from a call to
        `pandas.DataFrame.describe()`.
    axis : str
        The axis to be considered. Options are 'x', 'y', 'z'.

    Returns
    -------
    `plotly` Figure
        The figure object where the plot was made
    """
    # Create the figure
    fig = go.Figure()

    # Add a box for each condition
    i = 0
    conditions = list(stats_per_condition.keys())
    for condition, df in stats_per_condition.items():
        fig.add_trace(
            go.Box(
                median=[df.loc["50%", axis]],
                q1=[df.loc["25%", axis]],
                q3=[df.loc["75%", axis]],
                lowerfence=[df.loc["min", axis]],
                upperfence=[df.loc["max", axis]],
                x=[conditions[i]],
                name=condition,
                marker_color=px.colors.qualitative.Plotly[
                    i % len(px.colors.qualitative.Plotly)
                ],  # mod, because i might be bigger than length of px.colors.qualitative.Plotly,
                boxmean=True,
            )
        )
        i += 1

    fig.update_layout(
        title="Distribution of {}-values per condition".format(axis),
        xaxis_title=x_axis_title,
        yaxis_title="{}-value".format(axis),
        template="plotly_white",
        width=width,
        height=height,
    )

    return fig


def get_2d_kde_plot(save_path=None, dpi=300):
    """Get a 2D KDE plot for the given grouping.

    Parameters
    ----------
    save_path : str, optional
        The path where to save (or from where to load) the plot, by default
        None. If rebuild is False, this parameter is required.
    dpi : int, optional
        The dpi of the plot, by default 300

    Returns
    -------
    `matplotlib` Figure
        The figure object where the plot was made
    `matplotlib` Axes
        The axes object where the plot was made
    """
    try:
        # Try to load a precomputed pdf plot from disk as a numpy array
        doc = fitz.open(save_path)
        pixmap = doc.get_page_pixmap(0, dpi=dpi)
        img = Image.open(io.BytesIO(pixmap.tobytes()))

        # Plot the loaded image
        fig, ax = plt.subplots(1, 1, figsize=(24, 24))
        ax.imshow(img)

        # Remove the axis
        ax.axis("off")

    except FileNotFoundError:
        print("No plot with this name found at the provided `save_path`")

    return fig, ax


def cluster_kmeans(df, n_clusters):
    """
    Applies K-means clustering to a DataFrame.

    Parameters
    ----------
    df : pandas.DataFrame
        DataFrame containing the data to cluster.
    n_clusters : int
        The number of clusters to fit.

    Returns
    -------
    results : dict
        Dictionary containing cluster labels and inertia.
    """
    # Apply K-means clustering
    kmeans = KMeans(n_clusters=n_clusters, random_state=42)
    kmeans.fit(df)

    # Retrieve cluster labels
    labels = kmeans.labels_

    # Retrieve inertia
    inertia = kmeans.inertia_

    score = silhouette_score(df, labels)

    # Returning results as a dictionary
    results = {"labels": labels, "inertia": inertia, "silhouette_score": score}

    return results


def cluster_gmm(data, n_clusters):
    """
    Applies Gaussian Mixture Model clustering to a DataFrame or numpy.array.

    Parameters
    ----------
    data : pandas.DataFrame or numpy.array
        DataFrame containing the data to perform the clustering upon.
    n_clusters : int
        The number of clusters (Gaussian components) to fit.

    Returns
    -------
    res : dict
        Contains the labels, bic score, aic score and silhouette score.
    """

    gmm = GaussianMixture(n_components=n_clusters, init_params='k-means++')
    gmm.fit(data)
    # silhouette_scores = []

    # Predict cluster labels
    labels = gmm.predict(data)

    # Calculate BIC and AIC
    bic = gmm.bic(data)
    aic = gmm.aic(data)

    # Calculate the silhouette score.
    score = silhouette_score(data, labels)

    res = {"labels": labels, "bic": bic, "aic": aic, "silhouette_scores": score}

    return res


def cluster_fcm(dt, n_clusters):
    """
    Applies Fuzzy C-Means clustering to a DataFrame.

    Parameters
    ----------
    dt : numpy.array
        DataFrame containing the data to perform the clustering upon.
    n_clusters : int
        The number of clusters to fit.

    Returns
    -------
    results : dict
        Dictionary containing membership matrix, the silhouette score and the Fuzzy Partition Coefficient (FPC).
    """
    # Data preparation
    data = dt.T  # Fuzzy C-Means needs data in the shape of [features, samples]

    # Fuzzy C-Means
    cntr, u, u0, d, jm, p, fpc = cmeans(
        data, c=n_clusters, m=2, error=0.005, maxiter=1000, init=None
    )

    # Find the cluster membership of each point
    cluster_membership = np.argmax(u, axis=0)

    # Silhouette score (optional, as it requires crisp clustering labels)
    score = silhouette_score(dt, cluster_membership)

    results = {"membership_matrix": u, "silhouette_score": score, "FPC": fpc}

    return results


def cluster_som(
    data, n_clusters, x_dim, y_dim, sigma=1, learning_rate=0.5, iterations=10000
):
    """
    Applies Self-Organizing Map (SOM) clustering to a DataFrame or numpy.array.

    Parameters
    ----------
    data : pandas.DataFrame or numpy.array
        DataFrame or numpy array containing the data to cluster.
    n_clusters : int
        The intended number of clusters. Note that actual clusters are determined by x_dim * y_dim.
    x_dim, y_dim : int
        Dimensions of the SOM grid. These define the number of potential clusters.
    sigma : float, optional
        Spread of the neighborhood function, defaults to 0.5.
    learning_rate : float, optional
        Learning rate for SOM training, defaults to 0.5.
    iterations : int, optional
        Number of iterations over the training data.

    Returns
    -------
    res : dict
        Contains the labels and the som object itself for further analysis.
    """

    # Feature scaling.
    # sc = MinMaxScaler(feature_range = (0,1))
    # sc.fit(X)
    # X=sc.transform(X)

    # Initialize and train the SOM
    som = MiniSom(
        x=x_dim,
        y=y_dim,
        input_len=data.shape[1],
        sigma=sigma,
        learning_rate=learning_rate,
    )
    som.random_weights_init(data)
    som.train_random(data, iterations)

    # Predict cluster labels by finding the winner node for each instance
    labels = np.array([som.winner(d) for d in data])

    # Convert 2D grid positions into unique cluster labels
    cluster_labels = labels[:, 0] * y_dim + labels[:, 1]

    res = {"labels": cluster_labels, "som_model": som}

    return res


def plot_multiple_dfs(df_arr, names, opacity=0.9, palet=None, regions_mode=False):
    """Plots all the points (X,Y,Z-coordinates) of multiple dataframes.

    Parameters
    ----------
    df_arr : list
        List of the pandas.dataframes containing the points (X,Y,Z-coordinates).
    names : list
        List of the names of each dataframe (legends)
    opacity : float, optional
    Defines the opacity of the markers.

    Returns
    -------
    fig: plotly.graph_objects.Figure
        Figure object.
    """

    traces = []

    if palet is None:
        palet = []

    # Create the color palet.
    for i in range(len(df_arr)):
        r = random.randint(0, 255)
        g = random.randint(0, 255)
        b = random.randint(0, 255)
        s = "rgb(" + str(r) + "," + str(g) + "," + str(b) + ")"
        palet.append(s)

    for i in range(len(df_arr)):
        if names is not None:
            if regions_mode:
                if i == 0:
                    opacity = 0.9
                else:
                    opacity = 0.1

            trace = go.Scatter3d(
                x=df_arr[i].loc[:, "x"],
                y=df_arr[i].loc[:, "y"],
                z=df_arr[i].loc[:, "z"],
                mode="markers",  # if not put to markers, then there are lines connecting the points.
                marker=dict(
                    size=5,
                    color=palet[i],
                    colorscale="Viridis",  # Choose a colorscale
                    opacity=opacity,
                ),
                name=str(names[i]),
            )
        traces.append(trace)

    layout = go.Layout(scene=dict(aspectmode="data"))

    fig = go.Figure(data=traces, layout=layout)

    # fig.show()

    return fig


def PCA_cl(df_arr, df_names, feature_names, plot_3d=True, cluster_attribute_name=None, perform_TSNe=False, perform_UMAP=False, scale_axis=None):
    """Performs PCA on the dataframes given as inputs (df_arr).

    Parameters
    ----------
    df_arr : list
        List of the pandas.dataframes.
    names : list
        List of the names of each dataframe.
    fetaure_names : list
        List containing the names of the features upon which PCA will be performed.
    plot_3d : bool, optional
        If True will perform PCA in 3-dimensions.

    Returns
    -------
    None

    """
    scaler = StandardScaler()

    for i in range(len(df_arr)):
        print(df_names[i])

        # Plot without cluster information.
        pca = PCA(n_components=3)
        if scale_axis is None:
            data_pca = pca.fit_transform(df_arr[i][feature_names])
        elif scale_axis == 'pmf':
            df_temp = df_arr[i][feature_names].apply(lambda row: transform_row(row, option='pmf'), axis=1)
            data_pca = pca.fit_transform(df_temp)
        elif scale_axis == 0:
            data_pca = pca.fit_transform(scaler.fit_transform(df_arr[i][feature_names]))
        elif scale_axis == 1:
            data_pca = pca.fit_transform(df_arr[i][feature_names].sub(df_arr[i][feature_names].mean(axis=1), axis=0).div(df_arr[i][feature_names].std(axis=1), axis=0))
        
        # Explained variance
        explained_variance = pca.explained_variance_ratio_
        explained_variance_percentage = deepcopy(explained_variance) * 100

        # Plotting
        plt.figure(figsize=(8, 6))

        plt.scatter(
            data_pca[:, 0], data_pca[:, 1]
        )  # Plot all points without cluster colors
        plt.title("Clusters in PCA-reduced Space")
        plt.xlabel(
            f"PCA Component 1 - {explained_variance_percentage[0]:.2f}% Variance Explained"
        )
        plt.ylabel(
            f"PCA Component 2 - {explained_variance_percentage[1]:.2f}% Variance Explained"
        )
        plt.legend()
        plt.show()

        if perform_TSNe:
            # TSNE
            tsne = TSNE(n_components=3)#, random_state=42)
            tsne_data = tsne.fit_transform(df_arr[i][feature_names])
            x_label = "t-SNE Dimension 1"
            y_label = "t-SNE Dimension 2"

            plt.figure(figsize=(8, 6))
            plt.scatter(
                tsne_data[:, 0], tsne_data[:, 1]
            )  # Plot all points without cluster colors
            plt.title(f"Clusters in tsne-reduced Space")
            plt.xlabel(x_label)
            plt.ylabel(y_label)
            plt.show()

        if perform_UMAP:
            # UMAP.
            um = umap.UMAP(n_components=3)#, random_state=42)
            umap_data = um.fit_transform(df_arr[i][feature_names])

            plt.figure(figsize=(8, 6))
            plt.scatter(
                umap_data[:, 0], umap_data[:, 1]
            )  # Plot all points without cluster colors
            plt.title(f"Clusters in umap-reduced Space")
            plt.xlabel(x_label)
            plt.ylabel(y_label)
            plt.show()

        if plot_3d:
            # Plotly 3D scatter plot
            fig = px.scatter_3d(
                x=data_pca[:, 0],
                y=data_pca[:, 1],
                z=data_pca[:, 2],
                title=f"Clusters in PCA-reduced Space (3D) - {df_names[i]}",
                labels={
                    "x": f"PCA 1 - {explained_variance_percentage[0]:.2f}% Variance Explained",
                    "y": f"PCA 2 - {explained_variance_percentage[1]:.2f}% Variance Explained",
                    "z": f"PCA 3 - {explained_variance_percentage[2]:.2f}% Variance Explained",
                },
            )
            # plt.legend()
            fig.show()

            if perform_TSNe:
                fig = px.scatter_3d(
                    x=tsne_data[:, 0],
                    y=tsne_data[:, 1],
                    z=tsne_data[:, 2],
                    title=f"Clusters in TSNE-reduced Space (3D) - {df_names[i]}",
                    labels={
                        "x": f"TSNE 1",  # - {explained_variance_percentage[0]:.2f}% Variance Explained',
                        "y": f"TSNE 2",  # - {explained_variance_percentage[1]:.2f}% Variance Explained',
                        "z": f"TSNE 3,# - {explained_variance_percentage[2]:.2f}% Variance Explained",
                    },
                )
                fig.show()

            if perform_UMAP:
                fig = px.scatter_3d(
                    x=umap_data[:, 0],
                    y=umap_data[:, 1],
                    z=umap_data[:, 2],
                    title=f"Clusters in UMAP-reduced Space (3D) - {df_names[i]}",
                    labels={
                        "x": f"UMAP 1",  # - {explained_variance_percentage[0]:.2f}% Variance Explained',
                        "y": f"UMAP 2",  # - {explained_variance_percentage[1]:.2f}% Variance Explained',
                        "z": f"UMAP 3",  # - {explained_variance_percentage[2]:.2f}% Variance Explained'
                    },
                )
                fig.show()

        # Plot with cluster information (if applicable).
        if cluster_attribute_name is None:
            print("No cluster information provided.")
        else:
            cluster_labels = df_arr[i][cluster_attribute_name]

            # Plotting
            plt.figure(figsize=(8, 6))
            unique_clusters = cluster_labels.unique()
            for cluster in unique_clusters:
                # Plot points assigned to each cluster
                plt.scatter(
                    data_pca[cluster_labels == cluster, 0],
                    data_pca[cluster_labels == cluster, 1],
                    label=f"Cluster {cluster}",
                )

            plt.title("Clusters in PCA-reduced Space")
            plt.xlabel(
                f"PCA Component 1 - {explained_variance_percentage[0]:.2f}% Variance Explained"
            )
            plt.ylabel(
                f"PCA Component 2 - {explained_variance_percentage[1]:.2f}% Variance Explained"
            )
            plt.legend()
            plt.show()

            plt.figure(figsize=(8, 6))
            fig = px.scatter_3d(
                x=data_pca[:, 0],
                y=data_pca[:, 1],
                z=data_pca[:, 2],
                color=cluster_labels if cluster_attribute_name else None,  # Add color by cluster if specified
                title=f"Clusters in PCA-reduced Space (3D) - {df_names[i]}",
                labels={
                    "x": f"PCA 1 - {explained_variance_percentage[0]:.2f}% Variance Explained",
                    "y": f"PCA 2 - {explained_variance_percentage[1]:.2f}% Variance Explained",
                    "z": f"PCA 3 - {explained_variance_percentage[2]:.2f}% Variance Explained",
                },
            )
            fig.show()

            # plt.legend()
            # plt.show()

            if perform_TSNe:
                # TSNE.
                plt.figure(figsize=(8, 6))
                for cluster in unique_clusters:
                    # Plot points assigned to each cluster
                    plt.scatter(
                        tsne_data[cluster_labels == cluster, 0],
                        tsne_data[cluster_labels == cluster, 1],
                        label=f"Cluster {cluster}",
                    )

                plt.title(f"Clusters in tsne-reduced Space")
                plt.xlabel(x_label)
                plt.ylabel(y_label)
                plt.show()

            if perform_UMAP:
                # UMAP.
                plt.figure(figsize=(8, 6))
                for cluster in unique_clusters:
                    # Plot points assigned to each cluster
                    plt.scatter(
                        umap_data[cluster_labels == cluster, 0],
                        umap_data[cluster_labels == cluster, 1],
                        label=f"Cluster {cluster}",
                    )

                plt.title(f"Clusters in umap-reduced Space")
                plt.xlabel(x_label)
                plt.ylabel(y_label)
                plt.legend()
                plt.show()


def plot_cluster_values_over_time(df, features, cluster_column_name, clusters_to_plot, clusters_indices=None, offset=0, verbose=False, plot_barycenters=False, shared_y_axis_range=True):
    """Takes as input a dataframe with the assigned clusters and plots some defined number of them.

    Parameters
    ----------
    df : pandas.DataFrame
        The input dataframe.
    features : list
        List of the feature columns.
    cluster_column_name : str
        The name of the column containing the cluster information.
    clusters_to_plot : int
        The number of clusters to be plotted.
    clusters_indices : list (optional)
        List of the clusters to be plotted.
    offset : int (optional)
        Int for the case that the clusters need to be plotted sequentially.
    verbose : bool (optional)
        If True then messages will be displayed.
    plot_barycenters : bool (optional)
        If True plot the barycenters. This should be True only if the clusters to be plotted are normalized so that they are distributions.

    Returns
    -------
    None

    """
    if clusters_indices is None:
        clusters_to_plot = clusters_to_plot # Select how many cluster to plot (as they are a lot).
        clusters_indices = random.sample(list(df[cluster_column_name].unique()), clusters_to_plot)
    else:
        clusters_to_plot = len(clusters_indices)

    if shared_y_axis_range:
        y_min = df[features].min().min()
        y_max = df[features].max().max()

    fig, axs = plt.subplots(nrows=clusters_to_plot, ncols=1, figsize=(20, len(features) * clusters_to_plot//3), sharex=True)

    axs = np.atleast_1d(axs)

    # Loop through each cluster and create a plot
    for ind, cluster in enumerate(clusters_indices):
        df_c = df[df[cluster_column_name] == cluster + offset]
        if verbose:
            print(ind, cluster, len(df_c))
            # print('Selected cluster indices: ', clusters_indices)

        if not df_c.empty:  # Check if df_c has any rows for this cluster.
            if plot_barycenters:
                distributions = []
            for index, row in df_c.iterrows():
                # axs[cluster].plot(np.arange(len(selected_features)), row[selected_features], marker='o', linestyle='-', alpha=0.3)
                axs[ind].plot(np.arange(len(features)), row[features].values, marker='o', linestyle='-',  alpha=0.3)
                if plot_barycenters:
                    distributions.append(row[features].values)
            # Calculate and plot the median values for the cluster.
            mean_values = df_c[features].median().values
            axs[ind].plot(np.arange(len(features)), mean_values, marker='o', linestyle='--', color='black', linewidth=5, label='Average')
        
            # Calculate and plot the barycenter for the cluster if it applies.
            if plot_barycenters:
                size = len(features)

                cost_matrix = np.zeros((size, size))

                for i in range(size):
                    for j in range(size):
                        # Linear cost.
                        forward_distance = abs(i - j)
                        backward_distance = size - forward_distance
                        # Quadratic cost.
                        # forward_distance = abs(i - j)**2
                        # backward_distance = (size - forward_distance)**2

                        cost_matrix[i, j] = min(forward_distance, backward_distance)

                distributions = np.array(distributions)
                distributions = distributions.T
                weights = np.ones(distributions.shape[1]) / distributions.shape[1]
                barycenter = ot.barycenter(distributions, cost_matrix, 0.1, weights)
                axs[ind].plot(np.arange(len(features)), barycenter, marker='x', linestyle='--', color='purple', linewidth=2, label='Wasserstein Barycenter')

        if shared_y_axis_range:
            axs[ind].set_ylim(y_min, y_max)

        axs[ind].set_title(f'Cluster {cluster}, elements: {len(df_c)}')
        axs[ind].set_ylabel('Values')

    # Set common labels
    plt.xlabel('ZT')

    # Adjust the layout and show the plot
    plt.tight_layout()
    plt.show()

    if plot_barycenters:
        sns.heatmap(cost_matrix)
        plt.show()

    return fig

# def plot_variance_per_feature(df, features_list, threshold1):


#     return


def transform_row(row, option='pmf'):
    """Normalizes every row of a dataframe so that the values sum up to 1.
    
    Parameters
    ----------
    row : pandas.Series
        The input row of the dataframe.
    
    Returns
    -------
    transformed_row : pandas.Series
        The normalized output row of the dataframe.
    """

    row_min = row.min()
    row_max = row.max()
    
    if option=='pmf': # Normalize by division with vector norm.
        if row_min < 0:
            transformed_row = (row - row_min)
            row_max = transformed_row.max()
            transformed_row = transformed_row/transformed_row.sum()
        else:
            transformed_row = row / row.sum()
    elif option=='minmax': # Min-max scaling.
        if row_max == row_min:
            transformed_row = pd.Series(0, index=row.index)  # Set to 0 if all values are equal, or some other default value
        else:
            transformed_row = (row - row_min) / (row_max - row_min)
    else:
        raise ValueError(f"Unknown option '{option}'. Choose 'pmf' or 'minmax'.")

    return transformed_row


def compute_pairwise_distance(i, j, df, features_to_compute_distances, option='polygon', cost_matrix='None'):
    """
    Given two indices, the function computes pairwise distance of two points.

    i : int
        The index of the one element.
    j : int
        The index of the other element.
    df : pandas.DataFrame
        The dataframe.
    features_to_compute_distances : list
        The features based on which distances will be computed.
    option : bool (optional)
        If 'polygon' the distances based on polygon approximation will be computed, otherwsie 
        if Wasserstein, the Wasserstein distance is used.
    cost_matrix : np.array (len(features_to_compute_distances), len(features_to_compute_distances))
        The cost matrix based on which the Wasserstein distance will be computed.
    """
    if option == 'polygon':
        curve1 = df.iloc[i][features_to_compute_distances]
        curve2 = df.iloc[j][features_to_compute_distances]
        
        # Construct the polygons
        curve1_pol = list(zip(np.arange(len(features_to_compute_distances)), curve1))
        curve2_pol = list(zip(np.arange(len(features_to_compute_distances)), curve2))
        
        c1 = [(0, 0)] + curve1_pol + [(7, 0)] + [(0, 0)]
        c2 = [(0, 0)] + curve2_pol + [(7, 0)] + [(0, 0)]
        
        p1 = Polygon(c1)
        p2 = Polygon(c2)
        p3 = p1.intersection(p2)
        
        # Calculate the distance
        d = 1 - p3.area / max(p1.area, p2.area)

    elif option == 'Wasserstein':
        distribution1 = df.iloc[i][features_to_compute_distances].to_numpy().flatten()
        distribution2 = df.iloc[j][features_to_compute_distances].to_numpy().flatten()
        
        # Compute the transport plan and EMD value
        transport_plan = ot.emd(distribution1, distribution2, cost_matrix)
        emd_value = np.sum(transport_plan * cost_matrix)
        d = emd_value
        # print('distance', d)
    return i, j, d


# def polygon_distance(u, v):
#     # Construct the polygons based on your curve points
#     curve1_pol = list(zip(np.arange(len(u)), u))
#     curve2_pol = list(zip(np.arange(len(v)), v))
    
#     c1 = [(0, 0)] + curve1_pol + [(len(u), 0)] + [(0, 0)]
#     c2 = [(0, 0)] + curve2_pol + [(len(v), 0)] + [(0, 0)]
    
#     p1 = Polygon(c1)
#     p2 = Polygon(c2)
#     p3 = p1.intersection(p2)
    
#     # Calculate the distance based on area
#     distance = 1 - p3.area / max(p1.area, p2.area)
#     return distance


def cluster_pipeline_algorithmic(df, feature_name, feature_values_list, features_to_compute_distances, clusters_num, cluster_column_name, \
    verbose=False, plot_option=True, distances_option='Euclidean', parallel_computation=True, algorithm='GMM'):
    """Given a dataframe takes a subset of it based on the desired options and performs 
    hierarchical clustering based on Euclidean and Wasserstein distance.

    Parameters
    ----------
    df : pandas.DataFrame
        The input dataframe.
    feature_name : str
        The feature based on which the subset dataframe will be computed.
    feature_values_list : list
        The values of the feature_name under consideration.
    features_to_compute_distances : list
        The features that will be used on the computation of the distances.
    clusters_num : int
        The desired number of clusters.
    cluster_column_name : str
        The name of the column the cluster results will be saved at.
    verbose : bool (optional)
        If True printing messages will appear.
    plot_option : bool (optional)
        If True figures will be plotted.

    Returns
    -------
    df_sub : pandas.DataFrame
        The selected subset of the input dataframe.
    """

    if feature_name is None:
        df_sub = df[features_to_compute_distances].copy(deep=True)
    else:
        df_sub = pd.DataFrame()
        for feature_value in feature_values_list:
            if verbose:
                print(feature_value, len(df[df[feature_name]==feature_value]))
            df_temp = df[df[feature_name]==feature_value].copy()
            df_sub = pd.concat([df_sub, df_temp], ignore_index=False)

    if algorithm == 'GMM':
        # GMM clustering.
        # Initialize and fit the Gaussian Mixture Model on the selected columns (conditions).
        gmm = GaussianMixture(n_components=clusters_num, init_params='k-means++')#, random_state=42)
        gmm.fit(df_sub)
        
        # Predict cluster labels for each row in the DataFrame
        labels = gmm.predict(df_sub)
        
        df_sub[cluster_column_name] = labels
        df_sub[cluster_column_name] += 1
    
        # Scale data and apply GMM on them.
        scaler = StandardScaler()
        X_norm = scaler.fit_transform(df_sub[features_to_compute_distances])#df_sub[features_to_compute_distances].apply(lambda row: transform_row(row, option=normalization_option), axis=1)

        gmm_s = GaussianMixture(n_components=clusters_num, init_params='k-means++')#, random_state=42)
        gmm_s.fit(X_norm)
        
        # Predict cluster labels for each row in the DataFrame
        labels = gmm_s.predict(X_norm)
        
        df_temp_norm = pd.DataFrame(X_norm, columns=features_to_compute_distances)
        # Assign the labels to the DataFrame
        df_temp_norm[cluster_column_name] = labels
        df_temp_norm[cluster_column_name] += 1
        # cluster_labels = fcluster(Z_norm, t=clusters_num, criterion='maxclust')
        # df_temp_norm[cluster_column_name] = cluster_labels

        # Scal over rows. Transpose the data to scale across rows.
        scaler_rows = StandardScaler()
        X_norm_rows = scaler_rows.fit_transform(df_sub[features_to_compute_distances].T).T

        # Create a DataFrame for the row-normalized data
        df_temp_norm_rows = pd.DataFrame(X_norm_rows, columns=features_to_compute_distances, index=df_sub.index)

        # Initialize and fit a new GMM on the row-normalized data
        gmm_rows = GaussianMixture(n_components=clusters_num, init_params='k-means++')
        gmm_rows.fit(X_norm_rows)

        # Predict cluster labels for each row in the row-normalized data
        labels_rows = gmm_rows.predict(X_norm_rows)

        # Assign the labels to the row-normalized DataFrame
        df_temp_norm_rows[cluster_column_name] = labels_rows
        df_temp_norm_rows[cluster_column_name] += 1
    elif algorithm == 'DBSCAN':
        # DBSCAN clustering (ignores clusters_num parameter)
        
        # Initialize DBSCAN
        dbscan = DBSCAN(eps=0.5, min_samples=5)  # You may want to adjust eps and min_samples
        labels = dbscan.fit_predict(df_sub[features_to_compute_distances])

        # DBSCAN labels -1 for noise, adjust to start clusters from 1
        df_sub[cluster_column_name] = labels
        df_sub[cluster_column_name] += 1

        # Normalize over features.
        scaler = StandardScaler()
        X_norm = scaler.fit_transform(df_sub[features_to_compute_distances])

        dbscan = DBSCAN(eps=0.5, min_samples=5)  # You may want to adjust eps and min_samples
        labels = dbscan.fit_predict(X_norm)

        df_temp_norm = pd.DataFrame(X_norm, columns=features_to_compute_distances, index=df_sub.index)
        # DBSCAN labels -1 for noise, adjust to start clusters from 1
        df_temp_norm[cluster_column_name] = labels
        df_temp_norm[cluster_column_name] += 1

        # Normalize over rows.
        scaler = StandardScaler()
        X_norm_rows = scaler.fit_transform(df_sub[features_to_compute_distances].T).T
        
        dbscan = DBSCAN(eps=0.5, min_samples=5)  # You may want to adjust eps and min_samples
        labels = dbscan.fit_predict(df_sub[features_to_compute_distances])

        dbscan = DBSCAN(eps=0.5, min_samples=5)  # You may want to adjust eps and min_samples
        labels = dbscan.fit_predict(X_norm_rows)
        df_temp_norm_rows = pd.DataFrame(X_norm_rows, columns=features_to_compute_distances, index=df_sub.index)
        df_temp_norm_rows[cluster_column_name] += 1

    # Plot.
    if plot_option:
        print('Plotting the original time-series.')
        plot_cluster_values_over_time(df_sub, features_to_compute_distances, cluster_column_name, clusters_num, np.arange(1, clusters_num+1), verbose=True)

        # # Plot dendrogram
        # fig, ax1 = plt.subplots(figsize=(10, 10))

        # # Plot the dendrogram
        # dendrogram(Z, ax=ax1, orientation='left', truncate_mode='lastp', p=clusters_num);
        
        # ax1.set_title('Dendrogram');

        # plt.tight_layout();
        # plt.show()

        # df_temp = df_sub[features_to_compute_distances].copy(deep=True)
        # df_temp[cluster_column_name] = df_sub[cluster_column_name]

        # plot_multiple_time_profiles(df_temp, np.arange(1, clusters_num+1), cluster_column_name);
    
        print('Plotting the normalized (over features) time-series.')
        plot_cluster_values_over_time(df_temp_norm, features_to_compute_distances, cluster_column_name, clusters_num, np.arange(1, clusters_num+1), verbose=True)

        print('Plotting the normalized (over time) time-series.')
        plot_cluster_values_over_time(df_temp_norm_rows, features_to_compute_distances, cluster_column_name, clusters_num, np.arange(1, clusters_num+1), verbose=True)

        # Plot dendrogram
        # fig, ax1 = plt.subplots(figsize=(10, 10))

        # # Plot the dendrogram.
        # if distances_option == 'Euclidean':
        #     dendrogram(Z_norm, ax=ax1, orientation='left', truncate_mode='lastp', p=clusters_num);
        # else:
        #     dendrogram(Z, ax=ax1, orientation='left', truncate_mode='lastp', p=clusters_num);
        # ax1.set_title('Dendrogram');

        # plt.tight_layout();
        # plt.show()

        plot_multiple_time_profiles(df_sub, np.arange(1, clusters_num+1), cluster_column_name);
        plot_multiple_time_profiles(df_temp_norm, np.arange(1, clusters_num+1), cluster_column_name);
        plot_multiple_time_profiles(df_temp_norm_rows, np.arange(1, clusters_num+1), cluster_column_name);

    return df_sub, df_temp_norm


def cluster_pipeline_hierarchical(df, feature_name, feature_values_list, features_to_compute_distances, clusters_num, cluster_column_name, \
    verbose=False, plot_option=True, distances_option='Euclidean', parallel_computation=True, share_y=True):
    """Given a dataframe takes a subset of it based on the desired options and performs 
    hierarchical clustering based on Euclidean and Wasserstein distance.

    Parameters
    ----------
    df : pandas.DataFrame
        The input dataframe.
    feature_name : str
        The feature based on which the subset dataframe will be computed.
    feature_values_list : list
        The values of the feature_name under consideration.
    features_to_compute_distances : list
        The features that will be used on the computation of the distances.
    clusters_num : int
        The desired number of clusters.
    cluster_column_name : str
        The name of the column the cluster results will be saved at.
    verbose : bool (optional)
        If True printing messages will appear.
    plot_option : bool (optional)
        If True figures will be plotted.

    Returns
    -------
    df_sub : pandas.DataFrame
        The selected subset of the input dataframe.
    """

    if feature_name is None:
        df_sub = df[features_to_compute_distances].copy(deep=True)
    else:
        df_sub = pd.DataFrame()
        for feature_value in feature_values_list:
            if verbose:
                print(feature_value, len(df[df[feature_name]==feature_value]))
            df_temp = df[df[feature_name]==feature_value].copy()
            df_sub = pd.concat([df_sub, df_temp], ignore_index=False)

    if distances_option == 'Euclidean':
        # Hierarchical clustering based on Euclidean distance.
        # normalization_option = 'minmax'
        # df_temp_norm = df_sub[features_to_compute_distances].apply(lambda row: transform_row(row, option=normalization_option), axis=1)

        print('Computing the all-by-all distance matrix...')
        condensed_dist_matrix = pdist(df_sub[features_to_compute_distances], metric='euclidean')
        # Convert the condensed distance matrix to a square all-by-all distance matrix
        all_by_all_DM = squareform(condensed_dist_matrix)

        print('Performing hierarchical clustering (Euclidean distances)...')
        Z = linkage(df_sub[features_to_compute_distances], method='ward')#method='ward')
        cluster_labels = fcluster(Z, t=clusters_num, criterion='maxclust')
        df_sub[cluster_column_name] = cluster_labels

        df_temp_norm = df_sub[features_to_compute_distances].apply(lambda row: transform_row(row, option='pmf'), axis=1)

        Z_norm = linkage(df_temp_norm[features_to_compute_distances], method='ward')
        cluster_labels = fcluster(Z_norm, t=clusters_num, criterion='maxclust')
        df_temp_norm[cluster_column_name] = cluster_labels

    elif distances_option == 'Wasserstein':
        # Hierarchical clustering based on Wasserstein distance.

        # Define the cost matrix for Earth mover's distance calculation.
        size = len(features_to_compute_distances)
        cost_matrix = np.zeros((size, size))

        for i in range(size):
            for j in range(size):
                forward_distance = abs(i - j)
                backward_distance = size - forward_distance
                cost_matrix[i, j] = min(forward_distance, backward_distance)

        if verbose:
            print('The cost matrix for Wasserstein distance calculation:')
            sns.heatmap(cost_matrix, annot=True)

        df_temp_norm = df_sub[features_to_compute_distances].apply(transform_row, axis=1)

        all_by_all_DM = np.zeros((df_temp_norm.shape[0], df_temp_norm.shape[0]))
        if verbose:
            print('Computing Wasserstein distances...')

        if parallel_computation:
            # Generate all unique pairs (i, j) where j > i
            indices = [(i, j) for i in range(df_temp_norm.shape[0]) for j in range(i + 1, df_temp_norm.shape[0])]

            # Run parallel computation
            results = Parallel(n_jobs=-1, verbose=1, batch_size=10)(
                delayed(compute_pairwise_distance)(i, j, df_temp_norm, features_to_compute_distances, 'Wasserstein', cost_matrix) for i, j in indices)

            # Fill the results into the distance matrix
            for i, j, emd_value in results:
                all_by_all_DM[i, j] = emd_value
                all_by_all_DM[j, i] = emd_value  # Fill symmetric entry
        else:
            for i in tqdm(range(df_temp_norm.shape[0]), desc="Outer loop progress"):
                for j in range(i+1, df_temp_norm.shape[0]):
                    distribution1 = df_temp_norm.iloc[i][features_to_compute_distances].to_numpy()
                    distribution2 = df_temp_norm.iloc[j][features_to_compute_distances].to_numpy()
                    transport_plan = ot.emd(distribution1.flatten(), distribution2.flatten(), cost_matrix)
                    # sns.heatmap(transport_plan, annot=True)
                    emd_value = np.sum(transport_plan * cost_matrix)
                    all_by_all_DM[i,j] = emd_value
                    all_by_all_DM[j,i] = emd_value
                
        WDM_condensed = squareform(all_by_all_DM)
        Z = linkage(WDM_condensed, method='ward')#method='ward')

        cluster_labels = fcluster(Z, t=clusters_num, criterion='maxclust')
        df_sub[cluster_column_name] = cluster_labels
        df_temp_norm[cluster_column_name] = cluster_labels

    elif distances_option == 'Polygon':
        # Hierarchical clustering based on Polygon-intersection-over_union distance.

        # Define the cost matrix for Earth mover's distance calculation.
        size = len(features_to_compute_distances)

        # df_temp_norm = df_sub[features_to_compute_distances].apply(transform_row(option='minmax'), axis=1)
        df_temp_norm = df_sub[features_to_compute_distances].apply(lambda row: transform_row(row, option='minmax'), axis=1)

        all_by_all_DM = np.zeros((df_temp_norm.shape[0], df_temp_norm.shape[0]))

        if verbose:
            print('Computing polygon intersection distances...')

        # # Use pdist with the custom polygon-based distance function instead of the code below.
        # distance_matrix_condensed = pdist(df_temp_norm[features_to_compute_distances].values, metric=polygon_distance)
        # # Convert to a square form if needed
        # pi_dm = squareform(distance_matrix_condensed)

        # Parallel computation.
        if parallel_computation:
            # Generate all unique pairs (i, j) where j > i
            indices = [(i, j) for i in range(df_temp_norm.shape[0]) for j in range(i + 1, df_temp_norm.shape[0])]

            # Run parallel computation
            results = Parallel(n_jobs=-1, verbose=1)(delayed(compute_pairwise_distance)(i, j, df_temp_norm, features_to_compute_distances, 'polygon') for i, j in indices)

            # Fill the results into the distance matrix
            for i, j, d in results:
                all_by_all_DM[i, j] = d
                all_by_all_DM[j, i] = d  # Fill symmetric entry
        else:
            # This part will be parallelized.
            for i in tqdm(range(df_temp_norm.shape[0]), desc="Outer loop progress"):
                for j in range(i+1, df_temp_norm.shape[0]):
                    curve1 = df_temp_norm.iloc[i][features_to_compute_distances]
                    curve2 = df_temp_norm.iloc[j][features_to_compute_distances]
                    curve1_pol=list(zip(np.arange(len(features_to_compute_distances)), curve1))
                    curve2_pol=list(zip(np.arange(len(features_to_compute_distances)), curve2))
                    c1 = [(0,0)] + curve1_pol + [(7,0)] + [(0,0)]
                    c2 = [(0,0)] + curve2_pol + [(7,0)] + [(0,0)]
                    p1 = Polygon(c1)
                    p2 = Polygon(c2)
                    p3 = p1.intersection(p2)
                    # d = 1 - p3.area/max(p1.area, p2.area)
                    p4 = p1.union(p2)
                    d = 1 - p3.area/p4.area
                    all_by_all_DM[i,j] = d
                    all_by_all_DM[j,i] = d
                
        pi_dm_condensed = squareform(all_by_all_DM)
        Z = linkage(pi_dm_condensed, method='ward')

        cluster_labels = fcluster(Z, t=clusters_num, criterion='maxclust')
        df_sub[cluster_column_name] = cluster_labels
        df_temp_norm[cluster_column_name] = cluster_labels
        
    # Plot.
    if plot_option:
        print('Plotting the original time-series (share_y).')
        fig_clusters_raw_share_y = plot_cluster_values_over_time(df_sub, features_to_compute_distances, cluster_column_name, clusters_num, np.arange(1, clusters_num+1), verbose=True, shared_y_axis_range=True)

        print('Plotting the original time-series (not share_y).')
        fig_clusters_raw_not_share_y = plot_cluster_values_over_time(df_sub, features_to_compute_distances, cluster_column_name, clusters_num, np.arange(1, clusters_num+1), verbose=True, shared_y_axis_range=False)

        # Plot dendrogram
        fig_dendro_raw, ax1 = plt.subplots(figsize=(10, 10))

        # Plot the dendrogram
        dendrogram(Z, ax=ax1, orientation='left', truncate_mode='lastp', p=clusters_num);
        
        ax1.set_title('Dendrogram');

        plt.tight_layout();
        plt.show()

        df_temp = df_sub[features_to_compute_distances].copy(deep=True)
        df_temp[cluster_column_name] = df_sub[cluster_column_name]

        fig_interact_raw = plot_multiple_time_profiles(df_temp, np.arange(1, clusters_num+1), cluster_column_name);
    
        print('Plotting the normalized time-series.')
        fig_clusters_norm = plot_cluster_values_over_time(df_temp_norm, features_to_compute_distances, cluster_column_name, clusters_num, np.arange(1, clusters_num+1), verbose=True)

        # Plot dendrogram
        fig_dendro_norm, ax1 = plt.subplots(figsize=(10, 10))

        # Plot the dendrogram.
        if distances_option == 'Euclidean':
            dendrogram(Z_norm, ax=ax1, orientation='left', truncate_mode='lastp', p=clusters_num);
        else:
            dendrogram(Z, ax=ax1, orientation='left', truncate_mode='lastp', p=clusters_num);
        ax1.set_title('Dendrogram');

        plt.tight_layout();
        plt.show()

        fig_interact_norm = plot_multiple_time_profiles(df_temp_norm, np.arange(1, clusters_num+1), cluster_column_name);

    return df_sub, df_temp_norm, all_by_all_DM, Z, Z_norm, fig_dendro_raw, fig_dendro_norm, fig_clusters_raw_share_y, fig_clusters_norm, fig_interact_raw, fig_interact_norm, fig_clusters_raw_not_share_y


def plot_heatmap(
    df_arr, df_names, feature_names, cluster_name="cluster", features_for_ordering=None
):
    """Performs PCA on the dataframes given as inputs (df_arr).

    Parameters
    ----------
    df_arr : list
        List of the pandas.dataframes.
    names : list
        List of the names of each dataframe.
    fetaure_names : list
        List containing the names of the features upon which PCA will be performed.

    Returns
    -------
    None

    """

    figures = []

    for i in range(len(df_arr)):
        print(df_names[i])
        if features_for_ordering is None:
            sorted_df = df_arr[i].sort_values(cluster_name)
        else:
            sorted_df = df_arr[i].copy(deep=True)
            sorted_df.sort_values(
                by=features_for_ordering,
                ascending=False * len(features_for_ordering),
                inplace=True,
            )

        unique_clusters = sorted_df[cluster_name].unique()
        palette = sns.color_palette("hsv", len(unique_clusters))

        # Map cluster to color
        cluster_colors = sorted_df[cluster_name].map(
            dict(zip(unique_clusters, palette))
        )

        # Prepare the DataFrame for the heatmap (exclude the cluster column if it's included)
        heatmap_data = sorted_df[feature_names]

        # Create a row colors DataFrame for cluster colors
        row_colors = pd.DataFrame(cluster_colors).rename(
            columns={cluster_name: "Cluster Color"}
        )

        # Plot using clustermap
        g = sns.clustermap(
            heatmap_data,
            row_colors=row_colors,
            figsize=(10, 10),
            cmap="coolwarm",
            row_cluster=False,
            col_cluster=False,
            method="ward",
        )

        # Create legend handles for the clusters
        legend_handles = [
            mpatches.Patch(color=color, label=f"Cluster {cluster}")
            for cluster, color in zip(unique_clusters, palette)
        ]

        # Create a legend for the cluster colors
        plt.legend(
            handles=legend_handles,
            title="Cluster",
            bbox_to_anchor=(1, 1),
            loc="upper left",
            borderaxespad=0.0,
        )

        # Display the clustermap with the legend
        plt.show()

        figures.append(g)

    return figures


def plot_single_time_profile(df):
        """Plot time profile of the data

        Parameters
        ----------
        df : pandas.core.frame.DataFrame
            Data frame with the time profile data. Each row is a sample, and each
            column is a time point. The order of the columns is the order in which
            the time points should be plotted.

        Returns
        -------
        plotly.graph_objects.Figure
            Plotly figure
        """
        # Make a summary of the data per time point
        df_summary = make_time_df_summary(df)
        # Plot time profile with error bars
        fig = px.line(
            df_summary,
            x="time",
            y="mean",
            error_y="std",
            hover_data={"time": False},
            labels={"mean": "value", "std": "Standard deviation", "time": "Time point"},
            markers=True,
            line_shape="spline",
        )
        # Change line, marker and layout properties to better match the other plots in
        # this module
        # fig.update_traces(line=dict(color=_default_activation_color(), width=3))
        fig.update_traces(line=dict(width=3))
        fig.update_traces(
            marker=dict(
                # color=_default_activation_color(),
                size=9,
                line=dict(color="black", width=2),
            )
        )
        # self._set_layout(fig)
        return fig


def plot_multiple_time_profiles(df, clusters_to_plot, cluster_col='cluster'):
    """
    Plot time profiles for multiple clusters.

    Parameters
    ----------
    df : pandas.core.frame.DataFrame
        Data frame with the time profile data for all clusters.
    clusters_to_plot : list
        List of cluster IDs to plot.
    cluster_col : str, optional
        The name of the column in `df` that contains the cluster labels, by default 'cluster'.

    Returns
    -------
    plotly.graph_objects.Figure
        Plotly figure with time profiles for all specified clusters.
    """
    # Initialize a plotly figure object
    fig = go.Figure()

    # Iterate over each cluster and plot its time profile
    if df.index.name != None:
        df.index.name = None

    for cluster in clusters_to_plot:
        # print(cluster)
        # Filter the DataFrame for the current cluster
        df_cluster = df[df[cluster_col] == cluster]
        df_cluster = df_cluster.drop(columns=[cluster_col])

        # Get the time profile summary for the current cluster
        # display(df_cluster)
        df_summary = make_time_df_summary(df_cluster)

        # Add the cluster's time profile to the plot
        fig.add_trace(
            go.Scatter(
                x=df_summary["time"],
                y=df_summary["mean"],
                error_y=dict(type='data', array=df_summary["std"]),
                mode='lines+markers',
                name=f"Cluster {cluster}",
                line=dict(width=3),
                marker=dict(size=9, line=dict(color="black", width=2)),
            )
        )

    # Set layout options for the plot
    fig.update_layout(
        title="Time Profiles for Multiple Clusters",
        xaxis_title="Time point",
        yaxis_title="Mean Activation",
        showlegend=True,
    )

    fig.show();

    return fig


def make_time_df_summary(df):
    """Plot time profile of the data

        Parameters
        ----------
        df : pandas.core.frame.DataFrame
            Data frame with the time profile data. Each row is a sample, and each
            column is a time point. The order of the columns is the order in which
            the time points should be plotted.

        Returns
        -------
        plotly.graph_objects.Figure
            Plotly figure
        """
    
    # Record the order of the time columns
    time_columns = df.columns.to_list()
    
    # Turn this into a long-form DataFrame
    df_summary = df.stack().reset_index()
    # Drop column "level_0"
    if 'level_0' in df_summary.columns:
        df_summary = df_summary.drop(columns=["level_0"])
    # Rename remaining columns to "time" and "value"
    df_summary.columns = ["time", "value"]
    # Summarize the data by time (mean, std)
    df_summary = df_summary.groupby("time").agg(["mean", "std"]).reset_index()
    df_summary.columns = ["time", "mean", "std"]
    # Round values to 3 decimal places
    df_summary = df_summary.round(3)
    # Make time column the index
    df_summary = df_summary.set_index("time")
    # Reorder rows according to the ordering in time_columns
    df_summary = df_summary.reindex(time_columns)
    # Turn index back into time column
    df_summary = df_summary.reset_index()
    # Rename index -> time
    df_summary = df_summary.rename(columns={"index": "time"})
    return df_summary


def plot_voxels_per_cluster(df_step2, df_step1, cluster_column_name, voxel_grid_coords, features, brain_outline, cluster_names=None, save_path_folder=None, sample_option=False, plot_option=True):
    """Plots the voxels corresponding to each cluster. This function is designed for the two-step clustering process.

        Parameters
        ----------
        df_step2 : pandas.core.frame.DataFrame
            The Dataframe with the cluster information from the second step clsutering process.
        df_step1 : pandas.core.frame.DataFrame
            The Dataframe with the cluster information from the first step clsutering process, which contains also the indices to the voxel coordinates.
        cluster_column_name : str
            The name of the column containing the cluster information.
        features : list
            The list containing the feature names (time points).
        cluster_names : list (optional)
            The list containing the names of the clusters. If None, then the cluster names will be 
            the numerical values of the entries in cluster_column_name in a sorted order (int).
        save_path_folder : str (optional)
            The path where the files will be saved. If None then no saving will be performed.

        Returns
        -------
        plotly.graph_objects.Figure
            Plotly figure
        """

    df_arr_cl = []
    df_arr_cl_sampled = []

    if cluster_names is None:
        df_names_cl = list(sorted(df_step2[cluster_column_name].unique()))
    else:
        df_names_cl = deepcopy(cluster_names)

    voxel_info = pd.DataFrame()
    voxel_info_sampled = pd.DataFrame()
    # print(len(df_names_cl))
    total_voxels = 0

    for i in tqdm(df_names_cl, desc="Processing clusters"):
        df_arr_cl.append(voxel_grid_coords.loc[df_step1[df_step1['cluster'].\
                isin(df_step2[df_step2[cluster_column_name]==i].index)].index])

        df_arr_cl_sampled.append(df_arr_cl[-1].sample(frac=0.1))

        filtered = df_step1[df_step1['cluster'].\
                isin(df_step2[df_step2['hierarchical_cluster']==i].index)]
        df_temp = df_arr_cl[-1].copy(deep=True)
        df_temp['cluster'] = i
        df_temp[features] = filtered.loc[df_temp.index, features]
        voxel_info = pd.concat([voxel_info, df_temp])
        voxel_info_sampled = pd.concat([voxel_info, df_temp])
        
        total_voxels += len(df_arr_cl[-1])
        print(i, len(df_arr_cl[-1]), len(df_arr_cl_sampled[-1]))

    print('Total voxels: ', total_voxels)
    voxel_info['mean'] = None
    voxel_info['std'] = None

    voxel_info['mean'] = voxel_info[features].mean(axis=1)
    voxel_info['std'] = voxel_info[features].std(axis=1)

    voxel_info_sampled['mean'] = None
    voxel_info_sampled['std'] = None

    voxel_info_sampled['mean'] = voxel_info_sampled[features].mean(axis=1)
    voxel_info_sampled['std'] = voxel_info_sampled[features].std(axis=1)

    if total_voxels > 100000:
        sample_option = True
        
    if sample_option:
        fig = plot_multiple_dfs(df_arr_cl_sampled, df_names_cl, 0.7)
    else:
        fig = plot_multiple_dfs(df_arr_cl, df_names_cl, 0.7)
    fig = plotting.plot_brain_outline(brain_outline,
                                  
                                  fig=fig)
    fig = plotting.add_legend(fig)

    if plot_option:
        fig.show()

    return voxel_info, voxel_info_sampled, fig


def save_to_nrrd_files(voxel_info, cluster_column_name, aad, voxel_size, exp_type, save_path=None):
    
    base_path = aad.get_anatomical_atlas_path()
    nrrd_input_path = os.path.join(
            base_path, "Gubra_Annotation_NoOB_boundaries_filled.nrrd"
        )
    nrrd_arr = utils.load_nrrd_array(nrrd_input_path)

    nrrd_out = np.zeros_like(nrrd_arr)

    voxel_info['original_index'] = voxel_info.index
    # Incorporate the cluster information into the voxel grid.
    # voxel_info['cluster'] = -1

    for cl in tqdm(sorted(voxel_info['cluster'].unique()), desc='Prodress'):
        # print(cl)
        nrrd_out = np.zeros_like(nrrd_arr)
        for index, row in voxel_info[voxel_info['cluster']==cl].iterrows():
            coord_x = int(row['x'])
            coord_y = int(row['y'])
            coord_z = int(row['z'])
            if coord_x in range(nrrd_out.shape[0]) and coord_y in range(nrrd_out.shape[1]) and coord_z in range(nrrd_out.shape[2]):
                nrrd_out[coord_x, coord_y, coord_z] = 1 # Set 1 if the voxel belongs to the cluster.

        # Dilation.
        nrrd_out_dil = dilation(nrrd_out, footprint=np.ones((voxel_size, voxel_size, voxel_size)))
        
        print(cl, np.sum(nrrd_out_dil))
        # Save.
        if save_path:
            output_file = os.path.join(save_path, 'voxel_clusters_' + exp_type + '_cluster' + str(cl) + '.nrrd')
            nrrd.write(output_file, nrrd_out_dil)

    # return nrrd_out_dil