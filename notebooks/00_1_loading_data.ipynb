{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Loading data\n",
    "\n",
    "This notebook runs through how you could load and parse the raw data from the `sleep-brain-atlas` project so that it is \"analysis-ready\"\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import config\n",
    "import matplotlib.pyplot as plt\n",
    "import nb_utils\n",
    "\n",
    "from sba import (\n",
    "    dataset,\n",
    ")\n",
    "\n",
    "%load_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "# Dataset path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/config.py`\n",
    "DATA_DIR = config.DATA_DIR"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The `AtlasActivationDataset` class\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The main dataset object is implemented via the `sba.dataset.AtlasActivationDataset` class. All you need to use it is provide the path to the root of the data directory.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aad = dataset.AtlasActivationsDataset(DATA_DIR)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can access the paths to the transformed centroids JSON files via the class method `get_tc_json_paths`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "json_paths = aad.get_tc_json_paths()\n",
    "json_paths[0]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then you may load any given JSON file as a `pandas` data frame via the function `sba.dataset.load_df_from_json`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sample_df = dataset.load_df_from_json(json_paths[0])\n",
    "sample_df.head()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, you can skip loading the JSON files one by one and load instead all the transformed centroid information from the dataset at once. Do this using the class method `sba.dataset.AtlasActivationDataset.load_tc_table`. Note that by default, this function will try to load a file named `'transformed_centroids_table.csv'` from the transformed centroids subfolder. This is done to avoid having to rebuild the table from scratch, an action that can take a couple of minutes to complete. If you create the table from scratch, and cannot (or does not want to) save it in the default transformed centroid folder, you may select a different save path by setting the argument `path` to another value. Then, next time you run the `load_tc_table`, you just need to point `save_path` to that other value, and set `rebuild=False`, to have the function load the same table that you had previously computed. Loading a previously computed table should take a couple of seconds, rather than a couple of minutes.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "transformed_centroids_table = aad.load_tc_table(\n",
    "    rebuild=False,\n",
    "    # Comment the line below to load the table from DATA_DIR\n",
    "    path=\"~/Downloads/tmp_bav_files/transformed_centroids_table.csv\",\n",
    ")\n",
    "transformed_centroids_table.head()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Grouping\n",
    "\n",
    "You can group the entries of the transformed centroid by any column. A useful grouping in this project is by experiment type.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exp_type_grouping = transformed_centroids_table.groupby(\"exp_type\")\n",
    "list(exp_type_grouping.groups.keys())"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once grouped by experiment type, the data frame can then be further grouped by condition. For example, let us take the `'circadian'` experiment type and see all the possible condition groups.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "circadian = exp_type_grouping.get_group(\"circadian\")\n",
    "condition_grouping = circadian.groupby(\"condition\")\n",
    "list(condition_grouping.groups.keys())"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given this grouping, one can retrieve the coordinates of all activations under, say, `'ZT0'`, as follows:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coords = condition_grouping.get_group(\"ZT0\").loc[:, \"x\":\"z\"]\n",
    "coords.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading supplementary anatomical information\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Anatomical structures annotated on a 3D regular grid over the Atlas\n",
    "\n",
    "An `AtlasActivationDataset` object can load a table containing information on known anatomical structures in the brain. As with the transformed centroid table, you can choose whether to rebuild this table from scratch (costly) or load it from a location where a pre-computed version was saved. This table represents a regular 3D grid sampling of some anatomical structures on the brain atlas. Each (x,y,z)-coordinate on the table comes attached with a structure ID, its name, acronym, the ID of its parent structure and its depth along the hierarchy of the anatomical annotations.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "anatomical_table = aad.load_anatomical_table(\n",
    "    rebuild=False,\n",
    "    # Comment the line below to load the table from DATA_DIR\n",
    "    # path=\"~/Downloads/tmp_sba_files/anatomical_table.csv\",\n",
    ")\n",
    "anatomical_table.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This table is good because it provides a very dense annotation of known anatomical structures in the brain atlas. Dense annotations are good when the need for precisely identifying some structure rises. However this same density means that the table could be cumbersome to work with. See how many points it contains:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Number of points in the anatomical table: {:,}\".format(len(anatomical_table)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Boundaries of anatomical structures annotated on Atlas\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In some applications, it may be advantageous to load the following table instead. It contains fewer points than the previous one because only the boundaries of known anatomical structures are annotated. Working with this table, one has to make the assumption that the boundaries enclose their respective anatomical structures, as the interiors are not annotated. This makes for less precise, but more manageable annotations.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "anatomical_boundary_table = aad.load_anatomical_boundaries_table(\n",
    "    rebuild=False,\n",
    "    # Comment the line below to load the table from DATA_DIR\n",
    "    # path=\"~/Downloads/tmp_sba_files/anatomical_boundary_table.csv\",\n",
    ")\n",
    "anatomical_boundary_table.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In fact, we get about a tenfold reduction in the number of points that we work with:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\n",
    "    \"Number of points in the anatomical boundaries table: {:,}\".format(\n",
    "        len(anatomical_boundary_table)\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a visual intuition of the two kinds of annotations, see the figure below. On the very left, we have the grid annotations at a certain z-slice of the brain (what one gets via `aad.load_anatomical_table`). The middle image shows all possible structure boundaries at this slice (the boundaries are the white lines) in the dataset. Then, on the very right, we have the masking of the grid annotations by the boundaries (what one gets via `aad.load_anatomical_boundaries_table`). Feel free to play around with the slice number to see how the annotations change.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = nb_utils.plot_anatomical_annotations_side_by_side(aad, z_slice=125)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The `AnatomicalAtlas` class\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using any of the loaded anatomical tables, one may now create an `AnatomicalAtlas` object, which makes it easier to interact with the information contained in the tables themselves. This object automatically records how many different anatomical structures are present in the input table, via the attribute `n_structures`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note: you could also use the `anatomical_boundary_table` here instead of the\n",
    "# larger `anatomical_table`. Everything would work the same\n",
    "anatomical_atlas = dataset.AnatomicalAtlas(anatomical_table)\n",
    "anatomical_atlas.n_structures"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we want to extract only the part of the point cloud belonging to the Suprachiasmatic Nucleus (SCH), for example, we need to know its ID in the anatomical table convention. To help with this, we can take advantage of the class method `translate_to_id`. Note that the method can automatically do slight typo corrections on the name, so it should still work if you match the name actually recorded on the table on the table up to 2 characters. Try writing `'Supraciasmatic Mucleus'` and you'll see that this is true. Make too many typos, though, and a ValueError will be raised.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "anatomical_atlas.translate_to_id(\"name\", \"suprachiasmatic nucleus\", fix_typos=True)\n",
    "\n",
    "# You could also try to translate to the acronym:\n",
    "# anatomical_atlas.translate_to_id('acronym', 'SCH')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we know the SCH's ID, we can easily access its point cloud.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "suprachiasmatic_nucleus = anatomical_atlas.get_structure_from_id(286)\n",
    "suprachiasmatic_nucleus.head()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can retrieve the point cloud of the parent of a given structure with the method `get_parent_from_id`, whenever there are parents recorded.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    df = anatomical_atlas.get_parent_from_id(286)\n",
    "    df.head()\n",
    "except ValueError:\n",
    "    print(\"The parent's ID does not appear on the table.\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the parent of the SCH, a structure with ID = 141, does not appear in the anatomical table under the column `'id'`. That is, there are no annotations for this parent structure in the table we used to create the `AnatomicalAtlas` object.\n",
    "\n",
    "Let us try to retrieve the parent of a different child then.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = anatomical_atlas.get_parent_from_id(6)\n",
    "df.head()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that the points in the returned point cloud all have ID = 784. If we want to know the name of the structure that it represents, we can employ the method `translate_from_id`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "anatomical_atlas.translate_from_id(\"name\", 784)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can also get all the children structures from a single parent ID. This is what the method `get_children_from_id` does.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cst_children = anatomical_atlas.get_children_from_id(784)\n",
    "cst_children.head()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see then that the structure with ID = 6 is one of the children structures of the Corticospinal tract, as expected. Let us check its name and the name of its siblings:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "child_ids = cst_children[\"id\"].unique()\n",
    "print(\"Children of the corticospinal tract:\")\n",
    "for child_id in child_ids:\n",
    "    print(\"\\t{}\".format(anatomical_atlas.translate_from_id(\"name\", child_id)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
