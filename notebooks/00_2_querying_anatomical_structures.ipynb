{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Querying anatomical structures\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import config\n",
    "import nb_utils\n",
    "import numpy as np\n",
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "from sba import dataset, point_cloud\n",
    "\n",
    "%reload_ext autoreload\n",
    "%autoreload 2\n",
    "\n",
    "# Dataset path as a global variable. Remember to set the correct path to the\n",
    "# dataset in `notebooks/config.py`\n",
    "DATA_DIR = config.DATA_DIR"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load the data\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create dataset object\n",
    "aad = dataset.AtlasActivationsDataset(DATA_DIR)\n",
    "\n",
    "# Load the transformed centroids table and restrict it to circadian experiments\n",
    "transformed_centroids_table = aad.load_tc_table(\n",
    "    # Comment the line below load the table from DATA_DIR\n",
    "    # path='~/Downloads/tmp_sba_files/transformed_centroids_table.csv'\n",
    ")\n",
    "exp_type_grouping = transformed_centroids_table.groupby(\"exp_type\")\n",
    "circadian = exp_type_grouping.get_group(\"circadian\")\n",
    "\n",
    "# Load the anatomical table and build an AnatomicalAtlas object from it\n",
    "anatomical_table = aad.load_anatomical_table(\n",
    "    # Comment the line below load the table from DATA_DIR\n",
    "    # path='~/Downloads/tmp_sba_files/anatomical_table.csv'\n",
    ")\n",
    "anatomical_atlas = dataset.AnatomicalAtlas(anatomical_table)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Motivation: restricting activations to a specific anatomical structure\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The interest in having anatomical annotations in supplement to the neuronal activations lies in providing a way to interpret patterns of activations in terms of known anatomical structures. If in certain stages of an experiment, activations seem to be very evident within, say, the SCH, then we have a lead to follow in order to understand the role of the SCH in what was being recorded in the experiment.\n",
    "\n",
    "The basic task in this line of reasoning is thus being able to say whether an activation fall within a certain anatomical structure or not.\n",
    "\n",
    "The base class `sba.point_cloud.Set` provides one with the template to define a set from a set of labeled points. Once a set is defined, you can, for instance, query whether an arbitrary point belongs to that set.\n",
    "\n",
    "Let us take the Suprachiasmatic nucleus as an example and define a set from its point cloud.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scn_set = point_cloud.Set()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Modeling anatomical structures as k-nearest neighbors (k-NN) classifiers\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To define a set from a collection of points, we use the `Set.fit` method. This method requires that we provide a `scikit-learn`-type of dataset with points and corresponding labels. The labels should be seen as the values of the set's indicator function evaluated at the points. That is, taking the SCN as example, points within the SCN should be assigned a label of 1, whereas points outside of it a label of 0. Luckily, such one-versus-all labeling is automatically given to us via the `get_one_vs_all_labeling` method of the `AnatomicalAtlas` class.\n",
    "\n",
    "All we need to provide the aforementioned method is the ID of the structure for with we want to get a labeling and (optionally) some sampling fraction parameters. Sampling can be useful to reduce the size of the dataset, and thus speed up computations down the line. Setting `id_sample_frac=0.8` means that we wil retain (uniformly at random) only 80% of the points associated with label 1. Similarly, with `not_id_sample_frac=0.1` we randomly retain only 10% of the points with label 0.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scn_points, scn_labels = anatomical_atlas.get_one_vs_all_labeling(\n",
    "    286, id_sample_frac=0.8, not_id_sample_frac=0.1\n",
    ")\n",
    "print(\"Shape of `scn_points` = {}\".format(scn_points.shape))\n",
    "print(\"Shape of `scn_labels` = {}\".format(scn_labels.shape))\n",
    "print(\"Number of 0-labels = {}\".format(np.sum(scn_labels == 0)))\n",
    "print(\"Number of 1-labels = {}\".format(np.sum(scn_labels == 1)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we have the one-versus-all labeling for the set in which we are interested, we can call the `Set.fit` method to define an abstraction for what we imagine as the continuous SCN set. Fitting essentially boils down to training a classifier that will be able to decide whether an arbitrary query point belongs to our set of interest or not. To do that, let us split the SCN data into training and testing sets.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Split points and labels into training and test sets, making sure to\n",
    "# get a `(1 - test_size)` fraction of each class in the training set\n",
    "#  and `test_size` in the test set (stratified split)\n",
    "train_points, test_points, train_labels, test_labels = train_test_split(\n",
    "    scn_points,\n",
    "    scn_labels,\n",
    "    test_size=0.2,\n",
    "    stratify=scn_labels,\n",
    "    random_state=2023,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "K-nearest-neighbors classification is one of the methods used by the `Set.fit` function. The k-NN implementation used under the hood is the one from `sklearn.neighbors.KNeighborsClassifier`. You may provide any optional arguments to the `KNeighborsClassifier` constructor directly via name-value pairs in the call to `Set.fit`. The latter returns the F1-score of the trained classifier on the training data, as well as a confusion matrix for the two-labels (out-of-set versus in-set).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_score, cm_train = scn_set.fit(\n",
    "    train_points, train_labels, method=\"knn\", verbose=True\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can then see how well this classifier generalizes to the test set we defined beforehand.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_score, cm_test = scn_set.evaluate(test_points, test_labels, verbose=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With such large F1-scores, we can be fairly confident that `scn_set`'s internal classifier is modeling fairly well the abstract SCN set that we have in mind.\n",
    "\n",
    "Now, for actually using this model in practice. A common action in our analysis is checking how do neural activations within a known anatomical structure behave. To begin answering this question, we need to be able to filter out only the activation coordinates that fall within such anatomical structure.\n",
    "\n",
    "Let us check, in the circadian subset of the transformed centroids table, which coordinates fall within the SCN. To do that, we employ `scn_set`'s `get_indices_belonging_to_set` method. It takes the numpy array containing the x,y,z-coordinates in the `circadian` data frame, and returns the indices of the rows whose coordinates fall within the SCN.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "indices = scn_set.get_indices_belonging_to_set(circadian.loc[:, \"x\":\"z\"].to_numpy())\n",
    "indices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use these indices to restrict the `circadian` data frame to only the rows containing activations that fall within the SCN.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "circadian_scn = circadian.iloc[indices, :]\n",
    "circadian_scn.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Modeling anatomical structures as unions of convex (UC) sets\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another way to model anatomical structures is as unions of convex sets. This is the approach taken when calling `Set.fit` with `method=uc`. The idea is to split space into sub-regions, assume that the set is convex within each of these sub-regions, and then model the set as a union of such convex regions.\n",
    "\n",
    "The advantage in this approach is that querying whether a point belongs to the fitted set or not is much faster: it's simply a linear programming problem. The union of convex sets also remains flexible enough to model anatomical structures of potentially complex shapes. The downside is that one has to define a partition of the point cloud into convex sub-regions. We take a simple rout here and assume that the anatomical structures are convex at each of the left and right brain hemispheres (see `sba.point_cloud.hemisphere_partition`). This assumption will not hold in general, especially for larger structures, but can be seen as a conservative approximation of the area covered by the structures themselves.\n",
    "\n",
    "Let us use the same SCN training points/labels as before, but now model the set as a union of convex sets.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_score, train_cm = scn_set.fit(\n",
    "    train_points,\n",
    "    train_labels,\n",
    "    method=\"uc\",\n",
    "    verbose=True,\n",
    "    partition_fun=lambda pts: point_cloud.hemisphere_partition(pts, aad),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Not how training is now faster by a factor of about 5, with nonetheless similar F1-score. Let us see how well the model generalizes to the test set.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_score, cm_test = scn_set.evaluate(test_points, test_labels, verbose=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We still have a large test score, and we can observe again much faster inference due to the linear programming nature of the problem.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Side note: using the anatomical boundary annotations to fit the sets.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How are both approaches to modeling anatomical structures as sets in 3D space affected if we use the annotations in the sparser table of anatomical boundaries instead of the denser table of grid annotations?\n",
    "\n",
    "Let us define an `AnatomicalAtlas` object from the boundary annotations table.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the anatomical boundary table and build an AnatomicalAtlas object from it\n",
    "anatomical_boundary_table = aad.load_anatomical_boundaries_table(\n",
    "    # path='~/Downloads/anatomical_boundary_table.csv'  # Comment this line to\n",
    "    # load the table from\n",
    "    # DATA_DIR\n",
    ")\n",
    "anatomical_boundary_atlas = dataset.AnatomicalAtlas(anatomical_boundary_table)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For some variety, we'll also extract points within and outside the SCN in a different manner. The SCN points we'll get with the usual `get_structure_from_id` method. But the points outside will be given by the method `get_negative_sample_from_id`. The latter allows one to sample either a fixed number or a fractions of the rest of the annotations table. Let us sample 5 times as many non-SCN points than SCN points.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scn = anatomical_boundary_atlas.get_structure_from_id(286)\n",
    "not_scn = anatomical_boundary_atlas.get_negative_sample_from_id(286, n=5 * len(scn))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We then build the `points` and `labels` arrays explicitly by assigning vectors of 1s and 0s to the SCN and non-SCN points, respectively. We will train the sets with these sparsely annotated boundary points, but evaluate the fit on the denser grid annotations from before. The goal is to get a taste for how much we loose in generalization performance when basing our decisions on the sparser annotations.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "positive_samples = scn.loc[:, \"x\":\"z\"].to_numpy()\n",
    "positive_labels = np.ones(len(positive_samples))\n",
    "\n",
    "negative_samples = not_scn.loc[:, \"x\":\"z\"].to_numpy()\n",
    "negative_labels = np.zeros(len(negative_samples))\n",
    "\n",
    "points = np.vstack((positive_samples, negative_samples))\n",
    "labels = np.hstack((positive_labels, negative_labels))\n",
    "\n",
    "# Let us create a new set object to distinguish the sets fit from the\n",
    "# alternative boundary annotations from the previous ones\n",
    "scn_set_alt = point_cloud.Set()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compare graphically the two different representations\n",
    "\n",
    "- \"Dense\" is the one we used before\n",
    "- \"Sparse\" is the one with only boundary annotations\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scn_dense = anatomical_atlas.get_structure_from_id(286)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Points in dense annotation:\", len(scn_dense))\n",
    "print(\"Points in sparse annotation:\", len(scn))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice the difference in points used for dense and sparse annotations.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nb_utils.plot_multiple_dfs(\n",
    "    [scn, scn_dense], [\"scn boundaries annotations\", \"scn dense annotations\"], 0.1\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The k-NN approach\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_score, train_cm = scn_set_alt.fit(\n",
    "    points,  # Those are sparsely annotated points,\n",
    "    labels,  # and their labels\n",
    "    method=\"knn\",\n",
    "    verbose=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_score, cm_test = scn_set_alt.evaluate(\n",
    "    test_points,  # Those are the densely annotated SCN points from before,\n",
    "    test_labels,  # and their labels\n",
    "    verbose=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The UC approach\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_score, train_cm = scn_set_alt.fit(\n",
    "    points,  # Those are sparsely annotated points,\n",
    "    labels,  # and their labels\n",
    "    method=\"uc\",\n",
    "    verbose=True,\n",
    "    partition_fun=lambda pts: point_cloud.hemisphere_partition(pts, aad),\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_score, cm_test = scn_set_alt.evaluate(\n",
    "    test_points,  # Those are the densely annotated SCN points from before,\n",
    "    test_labels,  # and their labels\n",
    "    verbose=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Conclusion\n",
    "\n",
    "The UC approach is more robust than the k-NN one to the sparser annotations. Therefore, modeling anatomical structures as unions of convex sets should be preferred when using the anatomical boundary annotations.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Estimating the volume of anatomical structures\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are several ways one may estimate the volume of any given annotated anatomical structure.\n",
    "\n",
    "If a `Set` object has been fit to a structure, then one has access to the `Set.get_volume` method. By default, it will try to estimate the volume using functionality from the `polytope` package if a union of convex model was fit. Here's the thusly-estimated volume of the SCN in atlas units cubed:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scn_volume = scn_set.get_volume(method=\"auto\")\n",
    "print(\n",
    "    \"Volume estimated from union of convex polytopes {:.2f}\".format(\n",
    "        scn_volume,\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Otherwise, `Set.get_volume` will try to estimate the structures volume via a Monte Carlo method. The idea is to sample uniformly at random a large number of points within the bounding box of the structure, and count how many of these points fall within the structure itself. The ratio of the number of points within the structure to the total number of points sampled, times the volume of the bounding box, is then used as the volume estimate.\n",
    "\n",
    "You can force the use of the Monte Carlo by passing `method='mc'` explicitly to `Set.get_volume`. As you can see, the Monte Carlo estimate is not too far off from the polytope-based one.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scn_volume = scn_set.get_volume(method=\"mc\")\n",
    "print(\n",
    "    \"MC-estimated volume = {:.2f}\".format(\n",
    "        scn_volume,\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default, the Monte Carlo method samples 100,000 points. You can control this number the `n_samples` argument. If we decrease the number of samples to 1000, the estimate discrepancy with the polytope-based method goes to the order of hundreds of cubic atlas units.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n_samples = 1000\n",
    "scn_volume = scn_set.get_volume(method=\"mc\", n_samples=n_samples)\n",
    "print(\n",
    "    \"MC-estimated volume ({0:d} samples) = {1:.2f}\".format(\n",
    "        int(n_samples),\n",
    "        scn_volume,\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As an alternative to using `Set` objects, one may also take advantage of the dense grid of annotations in the anatomical table and use those directly. It suffices to determine the largest rectangular cuboid that can be inscribed between annotated points, compute its volume, an then multiply this volume by the total number of points in the anatomical structure of interest.\n",
    "\n",
    "The `AnatomicalAtlas` class provides a `get_volume_from_id` method that does just this process. It produces a number that is quite distinct from the ones before, however.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scn_volume = anatomical_atlas.estimate_volume_from_id(286)\n",
    "print(\"Volume estimated from anatomical table = {:.2f}\".format(scn_volume))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When using the sparser boundary annotations to create an `AnatomicalAtlas` object, one can still use the `get_volume_from_id` method. However, it's important to note that the volume estimate will be that of a thick shell around the boundary of the anatomical structure.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scn_volume = anatomical_boundary_atlas.estimate_volume_from_id(286)\n",
    "print(\"Volume estimated from anatomical boundaries table = {:.2f}\".format(scn_volume))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Conclusion:**\n",
    "\n",
    "In the end, any estimate of volume should work as long as it's used consistently and not mixed with other types of estimates.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "sba",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
