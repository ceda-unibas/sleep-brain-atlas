# Notebooks

The `notebooks/` folder contains examples, analyses, and narratives concerning the project.

To make sure you can run the notebooks, please run

```sh
pip install -r requirements.txt
```

under the repository's environment. This command will install the extra Python dependencies required by some of the notebooks.

If you have access to (a version of) the project's dataset, make sure to point to its location in `config.py`. In doing so, all the notebooks will automatically know where to load the data from.
