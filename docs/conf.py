project = "sleep-brain-atlas"

copyright = "2025, Rodrigo C. G. Pena"

author = "Rodrigo C. G. Pena"

extensions = [
    "myst_parser",  # Markdown support
    "autoapi.extension",  # Auto-generate API documentation
    "numpydoc",  # NumPy style docstrings
    "sphinx.ext.autodoc",  # Include documentation from docstrings
    "sphinxcontrib.bibtex",  # Include BibTeX citations
    "sphinx.ext.viewcode",  # Link to source code
    "sphinx.ext.intersphinx",  # Link to other projects' documentation
]

source_suffix = {
    ".rst": "restructuredtext",
    ".txt": "markdown",
    ".md": "markdown",
}

myst_enable_extensions = [
    "colon_fence",  # ::: can be used instead of ``` for better rendering
]

autoapi_dirs = ["../sba/"]  # Which directory to search for Python files
autoapi_ignore = ["*/conf.py"]  # Ignore when generating API documentation
auto_api_options = [
    "members",
    "show-inheritance",
    "show-module-summary",
    "imported-members",
]

intersphinx_mapping = {
    "skimage": ("https://scikit-image.org/docs/stable/", None),
    "numpy": ("https://numpy.org/doc/stable/", None),
    "plotly": ("https://plotly.com/python-api-reference/", None),
    "pyntcloud": ("https://pyntcloud.readthedocs.io/en/latest/", None),
    "pandas": ("https://pandas.pydata.org/docs/", None),
}

bibtex_bibfiles = ["refs.bib"]  # BibTeX file with references

templates_path = ["_templates"]

exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

html_theme = "sphinx_book_theme"
html_theme_options = {
    "repository_url": "https://gitlab.com/ceda-unibas/sleep-brain-atlas",
    "use_source_button": True,
    "path_to_docs": "docs",
    "use_edit_page_button": True,
    "use_repository_button": True,
    "use_issues_button": True,
}

suppress_warnings = ["myst.xref_missing"]
