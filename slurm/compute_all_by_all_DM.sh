#!/bin/bash

#Name of the job
#SBATCH --job-name=compute_all_by_all_DM

#Number of CPU cores reserved
#SBATCH --cpus-per-task=80

#Memory reserved per core.
#SBATCH --mem-per-cpu=10G

#Time during which the task will run
#SBATCH --time=7-00:00:00
#SBATCH --qos=1week

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/compute_all_by_all_DM.o
#SBATCH --error=logs/compute_all_by_all_DM.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sba_ub

#Export the required environment variables
##########################################

#Run the desired commands
#########################
# Compute the number of activations for all regions
python scripts/compute_all_by_all_DM.py --data_dir $SBA_DATA --save_dir $SBA_DATA/analysis_files/voxelization_and_analysis/voxel_size_1/smoothing
