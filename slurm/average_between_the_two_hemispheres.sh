#!/bin/bash

#Name of the job
#SBATCH --job-name=average_activations_between_the_two_hemispheres

#Number of CPU cores reserved
#SBATCH --cpus-per-task=1

#Memory reserved per core.
#SBATCH --mem-per-cpu=250G

#Time during which the task will run
#SBATCH --time=23:00:00
#SBATCH --qos=1day

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/average_between_the_two_hemispheres.o
#SBATCH --error=logs/average_between_the_two_hemispheres.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sba

#Export the required environment variables
##########################################

#Run the desired commands
#########################
# Compute the number of activations for all regions
python scripts/average_between_the_two_hemispheres.py --data_dir $SBA_DATA --save_dir $SBA_DATA/analysis_files/voxelization_and_analysis/voxel_size_1
