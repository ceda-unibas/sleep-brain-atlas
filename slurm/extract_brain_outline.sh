#!/bin/bash

#Name of the job
#SBATCH --job-name=extract_brain_outline

#Number of CPU cores reserved
#SBATCH --cpus-per-task=1

#Memory reserved per core.
#SBATCH --mem-per-cpu=16G

#Time during which the task will run
#SBATCH --time=0:30:00
#SBATCH --qos=30min

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/extract_brain_outline.o
#SBATCH --error=logs/extract_brain_outline.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sba

#Export the required environment variables
##########################################
subsample_fraction=0.1 # Fraction of the total number of coordinates to keep

#Run the desired commands
#########################
# Extract brain outline from anatomical annotations
python scripts/extract_brain_outline.py --data_dir $SBA_DATA --save_path $SBA_DATA/atlas_files/brain_outline_table.csv --subsample_fraction $subsample_fraction
