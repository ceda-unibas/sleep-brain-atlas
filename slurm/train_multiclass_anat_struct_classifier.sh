#!/bin/bash

#Name of the job
#SBATCH --job-name=train_multiclass_anat_struct_classifier

#sciCORE account running the job


#Number of CPU cores reserved
#SBATCH --cpus-per-task=64

#Memory reserved
#SBATCH --mem=256G

#Time during which the task will run
#SBATCH --time=06:00:00
#SBATCH --qos=6hours

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/train_multiclass_anat_struct_classifier.o
#SBATCH --error=logs/train_multiclass_anat_struct_classifier.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT


#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sba

#Export the required environment variables
##########################################
frac=1            # Fraction of the complete dataset to load
min_n_points=128  # Minimum number of points per anatomical  region to load
test_size=0.2     # Fraction of the loaded data to be used as test set

#Run the desired commands
#########################
python scripts/train_multiclass_anat_struct_classifier.py --data_dir $SBA_DATA --save_dir $SBA_DATA/atlas_files/anat_struct_classifier --test_size $test_size --frac $frac --min_n_points $min_n_points

