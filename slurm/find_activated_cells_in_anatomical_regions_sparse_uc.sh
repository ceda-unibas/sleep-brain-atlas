#!/bin/bash

#Name of the job
#SBATCH --job-name=find_activated_cells_in_anatomical_regions

#Number of CPU cores reserved
#SBATCH --cpus-per-task=1

#Memory reserved per core.
#SBATCH --mem-per-cpu=300G

#Time during which the task will run
#SBATCH --time=20:00:00
#SBATCH --qos=1day

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/find_activated_cells_in_anatomical_regions.o
#SBATCH --error=logs/find_activated_cells_in_anatomical_regions.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sba

#Export the required environment variables
##########################################

#Run the desired commands
#########################
# Compute the number of activations for all regions
python scripts/find_activated_cells_in_anatomical_regions_sparse_uc.py --data_dir $SBA_DATA --save_dir $SBA_DATA/analysis_files/activations_per_anatomical_region/sparse_annotations_uc
