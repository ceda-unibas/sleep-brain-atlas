#!/bin/bash

#Name of the job
#SBATCH --job-name=clustering_data_1st_step

#Number of CPU cores reserved
#SBATCH --cpus-per-task=8

#Memory reserved per core.
#SBATCH --mem-per-cpu=115G

#Time during which the task will run
#SBATCH --time=7-00:00:00
#SBATCH --qos=1week

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/clustering_data_1st_step.o
#SBATCH --error=logs/clustering_data_1st_step.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)"
conda activate sba_ub

#Export the required environment variables
##########################################

#Run the desired commands
#########################
# Compute the number of activations for all regions
python scripts/clustering_data_1st_step.py --data_dir $SBA_DATA --save_dir $SBA_DATA/analysis_files/voxelization_and_analysis/voxel_size_1
