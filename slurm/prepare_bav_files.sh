#!/bin/bash

#Name of the job
#SBATCH --job-name=prepare_bav_files

#Number of CPU cores reserved
#SBATCH --cpus-per-task=8

#Memory reserved
#SBATCH --mem=32G

#Time during which the task will run
#SBATCH --time=00:30:00
#SBATCH --qos=30min

#Paths to STDOUT or STDERR files should be absolute or relative to the current
#working directory
#SBATCH --output=logs/prepare_bav_files.o
#SBATCH --error=logs/prepare_bav_files.e

#Notify via email when the task ends or fails
#SBATCH --mail-type=END,FAIL,TIME_LIMIT

#This job runs from the current working directory

#Load the required modules
##########################
eval "$(conda shell.bash hook)" || true
conda activate sba

#Export the required environment variables
##########################################

#Run the desired commands
#########################
python scripts/prepare_bav_files.py --data_dir "${SBA_DATA}" --save_dir "${SBA_DATA}" bav_files --voxel_sizes 1
