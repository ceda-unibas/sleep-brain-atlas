"""Dataset processing module"""

import os
from os import listdir
from os.path import isfile, join
from pathlib import Path

import nrrd
import numpy as np
import pandas as pd
from skimage import measure
from tqdm import tqdm

from sba import utils


def get_json_filename_info(json_path):
    """Get information from JSON file name

    The individual mice transformed centroid files have information abut the
    mice and the experiment in their filenames. Use this function to extract
    them

    Parameters
    ----------
    json_path : str
        Path to the JSON file

    Returns
    -------
    tuple
        4-tuple containing, in order, the experiment type, the experiment ID,
        the mouse ID, and the condition
    """
    p = Path(json_path)
    name, _ = os.path.splitext(p.name)

    info_parts = name.split("_")

    condition = info_parts[-1]
    mouse_id = info_parts[-2]
    experiment_id = info_parts[-3]
    experiment_type = info_parts[-4]

    return experiment_type, experiment_id, mouse_id, condition


def fix_exp_type(exp_type, possible_exp_types=None, typo_dist=2):
    """Try to fix typos in the provided experiment type string

    Parameters
    ----------
    exp_type : str
        The experiment type string to potentially fix
    possible_exp_types : list, optional
        List of strings containing the ground truth spelling of all the
        possible experiment types. By default it is the following:
        `['arrhythmicNO', 'circadian', 'darkno', 'misting', 'no', 'sum']`
    typo_dist : int, optional
        Maximum edit distance between the provided experiment type string and
        any of the possible experiment type string to define the
        presence of a typo, by default 2. See `sba.utils.fix_typo`

    Returns
    -------
    str
       The returned string will be one of the possible experiment types if the
       provided experiment type string is close enough to any of the strings in
       that list. Otherwise, the provided experiment type string itself is
       returned
    """
    if possible_exp_types is None:
        possible_exp_types = [
            "arrhythmicNO",
            "circadian",
            "darkno",
            "misting",
            "no",
            "sum",
        ]
    for s in possible_exp_types:
        formatted_exp_type = utils.fix_typo(s, exp_type, typo_dist=typo_dist)

        if formatted_exp_type != exp_type:
            return formatted_exp_type

    return exp_type


def load_df_from_json(json_path):
    """Load data frame from individual transformed centroid JSON file

    Parameters
    ----------
    json_path : str
        path to the JSON file

    Returns
    -------
    `pandas.DataFrame`
        A table with columns `'x', 'y', 'z'` for the coordinates of brain atlas
        activations, and supplemental columns `'exp_type', 'sample_id', and
        'condition'` for the experiment type, the sample ID, and the condition,
        respectively. Sample ID is defined as a concatenation of the experiment
        ID and the mouse ID that one would find in the JSON filename.
    """
    # Load coordinates of brain atlas activations
    df = pd.read_json(json_path)
    df.rename(columns={0: "x", 1: "y", 2: "z"}, inplace=True)

    # Load supplemental information
    experiment_type, experiment_id, mouse_id, condition = get_json_filename_info(
        json_path
    )
    df["exp_type"] = fix_exp_type(experiment_type)
    df["sample_id"] = experiment_id + "_" + mouse_id
    df["condition"] = condition

    return df


def format_zt(condition):
    """Format Zeitgeber time (ZT) condition string

    Parameters
    ----------
    condition : str
        Condition string

    Returns
    -------
    str
        Formatted condition string
    """
    if len(condition) == 3:
        return condition.replace("ZT", "ZT0")
    else:
        return condition


def include_hemisphere_information(aad, df):
    """Add column to table indicating to which hemisphere each row belongs

    Parameters
    ----------
    aad : AtlasActivationsDataset
        An atlas activations dataset object. Used to get the dimensions of the
        atlas
    df : pandas.DataFrame
        Table to which to add the lobe information. Should have at least a
        column `'x'` with the x-coordinates in the brain atlas

    Returns
    -------
    pandas.DataFrame
        A copy of the provided table with the lobe information added. Lobe
        information is encoded as a binary column `'hemisphere'`, where each
        of the two values indicates one of the hemispheres
    """
    hemisphere_division_at = aad.atlas_dims[0] // 2
    hemisphere = np.zeros(len(df))
    hemisphere[np.where(df["x"] > hemisphere_division_at)[0]] = 1
    df.loc[:, "hemisphere"] = hemisphere.astype(int)
    return df


class AtlasActivationsDataset:
    """Dataset of brain atlas activations

    Parameters
    ----------
    path : str
        Path to the dataset
    """

    def __init__(self, path) -> None:
        self.path = path
        self.atlas_dims = self._get_atlas_dims()

    def _get_atlas_dims(self):
        """Get dimensions of the atlas

        Returns
        -------
        tuple
            A 3-tuple with the (x,y,z)-dimensions of the atlas
        """
        # Base path to anatomical atlas files
        base_path = self.get_anatomical_atlas_path()

        # Get the path to the NRRD file encoding the boundaries of the
        # anatomical structures
        nrrd_path = os.path.join(
            base_path, "Gubra_Annotation_NoOB_boundaries_filled.nrrd"
        )

        # Try to read header of NRRD file to get dimensions
        try:
            header = nrrd.read_header(nrrd_path)
            # Get dimensions from header field `'sizes'`
            # Dimensions x and y are swapped in the NRRD file, with
            # respect to the TIFF file in the dataset
            dim_y, dim_x, dim_z = header["sizes"]
            atlas_dims = (dim_x, dim_y, dim_z)
        except (FileNotFoundError, OSError):
            atlas_dims = (None, None, None)

        return atlas_dims

    def get_tc_path(self):
        """Get path to the individual animal json transformed centroids
        folder

        Returns
        -------
        str
            Path to the folder
        """
        return os.path.join(self.path, "Individual_animal_json_transformed_centroids")

    def get_anatomical_atlas_path(self):
        """Get path to the anatomical atlas files

        Returns
        -------
        str
            Path to the folder
        """
        return os.path.join(self.path, "atlas_files")

    def get_tc_json_paths(self):
        """Get paths to the transformed centroids JSON files

        Returns
        -------
        list
            List of JSON paths
        """
        tc_path = self.get_tc_path()
        filenames = [f for f in listdir(tc_path) if isfile(join(tc_path, f))]
        json_paths = []
        for filename in filenames:
            _, ext = os.path.splitext(filename)
            if ext == ".json":
                json_paths.append(os.path.join(tc_path, filename))
        return json_paths

    def load_tc_table(
        self,
        rebuild=False,
        path=None,
        format_conditions=False,
        include_hemisphere_info=False,
    ):
        """Load full transformed centroids table

        This function runs through all the individual transformed centroids
        JSON files and builds a table by concatenating row-wise the individual
        data frames induced by said JSON files.

        Parameters
        ----------
        rebuild : bool, optional
            Rebuild the whole table from scratch, by default False. Rebuilding
            from scratch could be time consuming (on the order of a couple of
            minutes), but you could set this flag to True if needed.
        path : str, optional
            Path where to save the built table. This defaults to the directory
            with individual json transformed centroids, under file name
            `'transformed_centroids_table.csv'`, but you can set it to any
            other path. If `rebuild == False`, the function will try to load
            the table from the specified (or implied) `path`
        format_conditions : bool, optional
            Format conditions strings so that they are easier to sort, by
            default False. If set to True, the function will, for example,
            replace `'ZT3'` with `'ZT03'` so that all Zeitgeber times (ZTs)
            have the same number of digits, and thus are easier to sort


        Returns
        -------
        `pandas.DataFrame`
            A table with columns `'x', 'y', 'z'` for the coordinates of brain
            atlas activations, and supplemental columns
            `'exp_type', 'exp_id', 'mouse_id', 'condition'` for the experiment
            type, the experiment ID, the mouse ID, and the condition,
            respectively

        See also
        --------
        load_df_from_json
        """
        # Path to the transformed centroids JSON files
        tc_path = self.get_tc_path()

        if path is None:
            path = os.path.join(tc_path, "transformed_centroids_table.csv")

        if rebuild:  # Rebuild the table from scratch
            df = self._rebuild_tc_table(path)

        else:  # Try to load the table from `path`
            try:
                df = pd.read_csv(path)
            except FileNotFoundError:
                print(
                    "File does not exist at the provided `path`. "
                    + "Try another path, or try enforcing the table to "
                    + "be rebuilt by setting`rebuild=True`"
                )
                return None

        if format_conditions:
            # Format Zeitgeber time (ZT) conditions
            df.loc[:, "condition"] = df["condition"].apply(format_zt)

        if include_hemisphere_info:
            df = include_hemisphere_information(self, df)

        return df

    def _rebuild_tc_table(self, path):
        # Path to the transformed centroids JSON files
        tc_path = self.get_tc_path()
        df_list = []
        json_paths = self.get_tc_json_paths()
        for p in tqdm(json_paths):
            df_list.append(load_df_from_json(os.path.join(tc_path, p)))
        df = pd.concat(df_list, ignore_index=True)
        df.to_csv(path, index=False)
        return df

    def _merge_anatomical_metadata(self, df, merge_on="id"):
        """Merge anatomical metadata to the provided table

        Parameters
        ----------
        df : `pandas.DataFrame`
            Table to which to merge anatomical metadata
        merge_on : str, optional
            Column on which to merge the anatomical metadata, by default 'id'

        Returns
        -------
        `pandas.DataFrame`
            The provided table with the anatomical metadata merged
        """
        # Load the metadata CSV file as a pandas DataFrame
        base_path = self.get_anatomical_atlas_path()
        metadata_path = os.path.join(base_path, "Gubra_regionIDs.csv")
        metadata_df = pd.read_csv(metadata_path)

        # Merge the two DataFrames on the `merge_on` column
        df = pd.merge(df, metadata_df, on=merge_on)

        # Remove rows with name == 'undefined' or 'Unlabeled'
        df = df[~df["name"].isin(["undefined", "Unlabeled"])].reset_index(drop=True)

        return df

    def load_anatomical_array(self, base_path=None):
        """Load array with IDs of known anatomical structures in the brain

        Parameters
        ----------
        base_path : str, optional
            Path to the directory where the anatomical atlas files are stored,
            by default None. If None, the function will try to get the path
            from the `get_anatomical_atlas_path` method

        Returns
        -------
        `numpy.ndarray`
            A 3D array with the IDs of the anatomical structures

        See also
        --------
        sba.utils.load_3d_tiff
        """
        # Base path to anatomical atlas files
        if base_path is None:
            base_path = self.get_anatomical_atlas_path()

        # Get path to TIFF file encoding the IDs and coordinates of the
        # anatomical structures, and the path to the CSV metadata file
        # indicating the IDs, names, and parent IDs of the anatomical
        # structures
        tif_path = os.path.join(base_path, "Gubra_Annotation_NoOB.tif")

        # Load TIFF file as a 3D array
        tif_arr = utils.load_3d_tiff(tif_path, dtype=int)

        # The arrays `tif_arr` and `nrrd_arr` have the first two
        # dimensions swapped (recording artifact). Swap the first two
        # dimensions in `tif_arr` to match `nrrd_arr` and the transformed
        # centroids table
        tif_arr = np.swapaxes(tif_arr, 0, 1)

        return tif_arr

    def load_anatomical_table(
        self, rebuild=False, path=None, include_hemisphere_info=False
    ):
        """Load table with coordinates of known anatomical structures in the
        brain

        Parameters
        ----------
        rebuild : bool, optional
            Rebuild the table from scratch, by default False. Rebuilding
            from scratch could be time consuming (on the order of a couple of
            minutes), but you could set this flag to True if needed.
        path : str, optional
            Path where to save the built table. This defaults to the directory
            where anatomical annotations are stored, but you can set it to any
            other path. If `rebuild == False`, the function will try to load
            the table from the specified (or implied) `path`
        include_hemisphere_info : bool, optional
            Include information about the hemisphere (left/right) to which each
            row belongs, by default False

        Returns
        -------
        `pandas.DataFrame`
            A table with columns:
            - `'x', 'y', 'z'`: atlas coordinates of the anatomical structure
            - `'id'`: ID of the anatomical structure
            - `'name'`: name of the anatomical structure
            - `'acronym'`: acronym of the anatomical structure
            - `'parent_structure_id'`: ID of the parent anatomical structure
            -  `'depth'`: depth of the anatomical structure in the anatomical
            hierarchy
        """
        # Base path to anatomical atlas files
        base_path = self.get_anatomical_atlas_path()

        # Parse save/load path
        if path is None:
            path = os.path.join(base_path, "anatomical_table.csv")

        if rebuild:  # Rebuild the table from scratch
            df = self._rebuild_anatomical_table(path)

        else:  # Try to load the table from `path`
            try:
                df = pd.read_csv(path)
            except FileNotFoundError:
                print(
                    "File does not exist at the provided `path`. "
                    + "Try another path, or try enforcing the table to "
                    + "be rebuilt by setting`rebuild=True`"
                )
                return None

        if include_hemisphere_info:
            df = include_hemisphere_information(self, df)

        return df

    def _rebuild_anatomical_table(self, path):
        # Load TIFF file as a 3D array
        tif_arr = self.load_anatomical_array()

        # Turn 3D array into table with columns 'x', 'y', 'z', and 'id'.
        tif_df = utils.tri_dim_array_to_table(
            tif_arr, column_names=["id", "x", "y", "z"]
        )

        # Remove rows with id == 0 because these are undefined regions
        tif_df = tif_df[tif_df["id"] != 0]

        # Merge data frame with anatomical metadata
        df = self._merge_anatomical_metadata(tif_df, merge_on="id")

        # Save the table to a CSV file
        df.to_csv(path, index=False)

        return df

    def load_anatomical_boundaries_array(
        self, base_path=None, dilate=False, footprint=None
    ):
        """Load array with boundaries of known anatomical structures in the
        brain

        Parameters
        ----------
        base_path : str, optional
            Path to the directory where the anatomical atlas files are stored,
            by default None. If None, the function will try to get the path
            from the `get_anatomical_atlas_path` method
        dilate : bool, optional
            Morphologically dilate the boundary lines at each z-slice to make
            them thicker, by default False
        footprint : `numpy.ndarray`, optional
            Structuring element used for morphological dilation, by default
            None. If None, a cross-shaped footprint (connectivity=1) is used

        Returns
        -------
        `numpy.ndarray`
            A 3D array with the boundaries of the anatomical structures

        See also
        --------
        sba.utils.dilate_3d_skeleton
        """
        # Base path to anatomical atlas files
        if base_path is None:
            base_path = self.get_anatomical_atlas_path()

        # Get the path to the NRRD file encoding the boundaries of the
        # anatomical structures
        nrrd_path = os.path.join(
            base_path, "Gubra_Annotation_NoOB_boundaries_filled.nrrd"
        )

        # Load NRRD file as a 3D array
        nrrd_arr = utils.load_nrrd_array(nrrd_path)

        # Make sure nrrd_arr is binary with values 0 and 1
        nrrd_arr = (nrrd_arr > 0).astype(int)

        # The boundary lines could be too thin to be used as masks. A
        # possible way around that is to morphologically dilate the
        # boundary lines at each z-slice to make them thicker
        if dilate:
            nrrd_arr = utils.dilate_3d_skeleton(nrrd_arr, footprint=footprint)

        return nrrd_arr

    def load_anatomical_boundaries_table(
        self, rebuild=False, path=None, include_hemisphere_info=False, **kwargs
    ):
        """Load table with coordinates of the boundaries of known anatomical
        structures in the brain

        Parameters
        ----------
        rebuild : bool, optional
            Rebuild the table from scratch, by default False. Rebuilding
            from scratch could be time consuming (on the order of a couple of
            minutes), but you could set this flag to True if needed.
        path : str, optional
            Path where to save the built table. This defaults to the directory
            where anatomical annotations are stored, but you can set it to any
            other path. If `rebuild == False`, the function will try to load
            the table from the specified (or implied) `path`
        include_hemisphere_info : bool, optional
            Include information about the hemisphere (left/right) to which each
            row belongs, by default False
        **kwargs : optional
            Optional keyword arguments to be passed to
            `sba.dataset.load_anatomical_boundaries_array`

        Returns
        -------
        `pandas.DataFrame`
            A table with columns:
            - `'x', 'y', 'z'`: atlas coordinates of the boundary of the
            anatomical structure
            - `'id'`: ID of the anatomical structure
            - `'name'`: name of the anatomical structure
            - `'acronym'`: acronym of the anatomical structure
            - `'parent_structure_id'`: ID of the parent anatomical structure
            -  `'depth'`: depth of the anatomical structure in the anatomical
            hierarchy
        """
        # Base path to anatomical atlas files
        base_path = self.get_anatomical_atlas_path()

        # Parse save/load path
        if path is None:
            path = os.path.join(base_path, "anatomical_boundary_table.csv")

        if rebuild:  # Rebuild the table from scratch
            df = self._rebuild_anatomical_boundaries_table(path, **kwargs)

        else:  # Try to load the table from `path`
            try:
                df = pd.read_csv(path)
            except FileNotFoundError:
                print(
                    "File does not exist at the provided `path`. "
                    + "Try another path, or try enforcing the table to "
                    + "be rebuilt by setting`rebuild=True`"
                )
                return None

        if include_hemisphere_info:
            df = include_hemisphere_information(self, df)

        return df

    def _rebuild_anatomical_boundaries_table(self, path, **kwargs):
        # Load TIFF file as a 3D array
        tif_arr = self.load_anatomical_array()

        # Load NRRD file as a 3D array
        nrrd_arr = self.load_anatomical_boundaries_array(**kwargs)

        # Use `nrrd_arr` to mask `tif_arr` and get the IDs of the annotated
        # regions at the boundaries
        masked_arr = (tif_arr * nrrd_arr).astype(int)

        # Turn 3D array into table with columns 'x', 'y', 'z', and 'id'.
        boundaries_df = utils.tri_dim_array_to_table(
            masked_arr, column_names=["id", "x", "y", "z"]
        )

        # Remove rows with id == 0 because these are undefined regions
        boundaries_df = boundaries_df[boundaries_df["id"] != 0]

        # Merge data frame with anatomical metadata
        df = self._merge_anatomical_metadata(boundaries_df, merge_on="id")

        # Save the table to a CSV file
        df.to_csv(path, index=False)

        return df

    def load_brain_outline_table(
        self,
        rebuild=False,
        path=None,
        include_hemisphere_info=False,
        subsample_fraction=0.1,
    ):
        """Load table with coordinates of the the brain atlas outline

        Parameters
        ----------
        rebuild : bool, optional
            Rebuild the table from scratch, by default False. Rebuilding
            from scratch could be time consuming, but you could set this flag
            to True if needed.
        path : str, optional
            Path where to save the built table. This defaults to the directory
            where anatomical annotations are stored, but you can set it to any
            other path. If `rebuild == False`, the function will try to load
            the table from the specified (or implied) `path`
        include_hemisphere_info : bool, optional
            Include information about the hemisphere (left/right) to which each
            row belongs, by default False
        subsample_fraction : float, optional
            Fraction of the total number of coordinates to keep in the table,
            by default 0.1. Only used if `rebuild == True`

        Returns
        -------
        """
        # Base path to anatomical atlas files
        base_path = self.get_anatomical_atlas_path()

        # Parse save/load path
        if path is None:
            path = os.path.join(base_path, "brain_outline_table.csv")

        if rebuild:  # Rebuild the table from scratch
            df = self._rebuild_brain_outline_table(subsample_fraction, path)

        else:  # Try to load the table from `path`
            try:
                df = pd.read_csv(path)
            except FileNotFoundError:
                print(
                    "File does not exist at the provided `path`. "
                    + "Try another path, or try enforcing the table to "
                    + "be rebuilt by setting`rebuild=True`"
                )
                return None

        if include_hemisphere_info:
            df = include_hemisphere_information(self, df)

        return df

    def _rebuild_brain_outline_table(self, subsample_fraction, path):
        # Load TIFF file of anatomical annotations as a 3D array
        tif_arr = self.load_anatomical_array()
        n_x, n_y, n_z = tif_arr.shape

        # Go through each z-slice and find the coordinates of the brain
        # outline
        outline_coords = None
        for z in tqdm(range(n_z)):
            # Get the z-slice as an image and set to 1 all pixels different
            # from 0
            image = tif_arr[:, :, z].reshape(n_x, n_y)
            image[image != 0] = 1

            # Get segmentation of the image into connected components
            labels = measure.label(image)
            segments = measure.regionprops(labels)

            # Go through each connected component and compile a list of
            # boundary coordinates
            coords = self._compile_boundary_coords(segments)

            # Update outline coordinates by adding the slice coordinates to it
            self._update_outline_coords(outline_coords, coords, subsample_fraction, z)

        # Keep outline coords as integers, because they are indices in a 3D
        # regular grid
        if outline_coords is not None:
            outline_coords = outline_coords.astype(int)

        # Turn 3D array into table with columns 'x', 'y', 'z'.
        df = pd.DataFrame(outline_coords, columns=["x", "y", "z"])

        # Save the table to a CSV file
        df.to_csv(path, index=False)

        return df

    def _compile_boundary_coords(self, segments):
        coords = None
        for segment in segments:
            try:
                tmp = utils.boundary_tracing(segment)
            except IndexError:
                continue

            if coords is None:
                coords = tmp
            else:
                coords = np.vstack((coords, tmp))
        return coords

    def _update_outline_coords(self, outline_coords, coords, subsample_fraction, z):
        if coords is not None:
            # Subsample the coordinates to reduce the size of the table
            n_coords_to_keep = subsample_fraction * len(coords)
            coords = coords[:: int(len(coords) / n_coords_to_keep)]

            # Add z-coordinate back to the coordinates
            coords = np.hstack((coords, np.ones((len(coords), 1)) * z))

            # Add the slice coordinates to the array of outline
            # coordinates
            if outline_coords is None:
                outline_coords = coords
            else:
                outline_coords = np.vstack((outline_coords, coords))


class AnatomicalAtlas:
    """Anatomical atlas

    Parameters
    ----------
    anatomical_table : pandas.DataFrame
        A table with columns `'x', 'y', 'z'` for the coordinates of known
        anatomical structures in the brain, and supplemental columns like
        `'id'`, `'name'`, and`'parent_structure_id'` for supplemental
        information on the structures. See `sba.dataset.load_anatomical_table`

    Attributes
    ----------
    anatomical_table : pandas.DataFrame
        The anatomical table
    id_translation_table : `DataFrame`
        A table with a summary of the supplemental columns of the anatomical
        table, used to translate between IDs and other properties
    n_structures : int
        Number of structures in the anatomical table

    See also
    --------
    AtlasActivationsDataset.load_anatomical_table
    """

    def __init__(self, anatomical_table) -> None:
        self.anatomical_table = anatomical_table
        self.xyzmin, self.xyzmax = self._get_bounding_box()
        self.id_translation_table = self._build_id_translation_table()
        self.n_structures = len(self.anatomical_table["id"].unique())

    def _get_bounding_box(self):
        xyzmin = self.anatomical_table.loc[:, "x":"z"].to_numpy().min(axis=0)
        xyzmax = self.anatomical_table.loc[:, "x":"z"].to_numpy().max(axis=0)
        return xyzmin, xyzmax

    def _build_id_translation_table(self):
        # Make a copy of self.anatomical_table and restrict the columns to all
        # but 'x', 'y', 'z'
        id_translation_table = self.anatomical_table.drop(columns=["x", "y", "z"])
        # Remove duplicate rows
        id_translation_table.drop_duplicates(inplace=True)
        # Sort by 'id' column
        id_translation_table.sort_values(by="id", inplace=True)
        # Reset the index
        id_translation_table.reset_index(drop=True, inplace=True)
        return id_translation_table

    def _fix_structure_name(self, name, typo_dist=2):
        """Fix potential typo in the provided structure name

        Parameters
        ----------
        name : str
            Name of the structure
        typo_dist : int, optional
            Maximum edit distance between the provided structure name and any
            of the possible structure names to define the presence of a typo,
            by default 2. See `sba.utils.fix_typo`

        Returns
        -------
        str
            The corrected structure name. If a typo has been deemed to take
            place, then the corrected name is returned. Otherwise, the provided
            name is returned
        """
        possible_structure_names = self.id_translation_table["name"].unique()

        for s in possible_structure_names:
            formatted_name = utils.fix_typo(s, name, typo_dist=typo_dist)

            # If a typo has been corrected, return the corrected name
            if formatted_name != name:
                return formatted_name

        return name

    def translate_to_id(self, property_name, property_value, fix_typos=False):
        """Translate property value to ID

        Parameters
        ----------
        property_name : str
            Name of the property. Valid options are the names of the columns in
            self.anatomical_table, excluding `'x'`, `'y'`, and `'z'`
        property_value : str or int
            Value of the property

        Returns
        -------
        int
            ID corresponding to the provided property value

        Raises
        ------
        ValueError
            If the provided property name is not one of the valid options

        See also
        --------
        translate_from_id

        Examples
        --------
        >>> from sba import dataset
        >>> dataset_path = 'path/to/dataset'
        >>> aad = dataset.AtlasActivationsDataset(dataset_path)
        >>> anatomical_table = aad.load_anatomical_table()
        >>> atlas = AnatomicalAtlas(anatomical_table)
        >>> atlas.translate_to_id('name', 'Cerebellum')
        512
        >>> atlas.translate_to_id('acronym', 'MOs')
        385
        """
        # Raise error if property_name is not one of the valid options
        possible_property_names = list(self.id_translation_table.columns)
        if property_name not in possible_property_names:
            raise ValueError(
                "Invalid property name. Valid options are: {}".format(
                    possible_property_names
                )
            )

        # If property_name is `'name'`, fix potential typos if the user
        # wants to do so
        if (property_name == "name") and fix_typos:
            property_value = self._fix_structure_name(property_value)

        # Find the row in the id translation table were column `property_name`
        # has value `property_value`
        row = self.id_translation_table[
            self.id_translation_table[property_name] == property_value
        ]

        # If the row is empty, raise an error
        if row.empty:
            raise ValueError(
                "Provided {} value does not appear on the table."
                "For a list of valid values check"
                "`self.id_translation_table['{}'].unique()`".format(
                    property_name, property_name
                )
            )

        # Return the entry in the `'id'` column of the row
        return row["id"].iloc[0]

    def translate_from_id(self, property_name, id):
        """Translate ID to property value

        Parameters
        ----------
        property_name : str
            Name of the property. Valid options are the names of the columns in
            self.anatomical_table, excluding `'x'`, `'y'`, and `'z'`
        id : int
            ID of the structure

        Returns
        -------
        str or int
            Value of the property corresponding to the provided ID

        Raises
        ------
        ValueError
            If the provided property name is not one of the options

        See also
        --------
        translate_to_id
        """
        # Raise error if property_name is not one of the valid options
        possible_property_names = list(self.id_translation_table.columns)
        if property_name not in possible_property_names:
            raise ValueError(
                "Invalid property name. Valid options are: {}".format(
                    possible_property_names
                )
            )

        # Find the row in the id translation table were column `'id'` has value
        # `id`
        row = self.id_translation_table[self.id_translation_table["id"] == id]

        # If the row is empty, raise an error
        if row.empty:
            raise ValueError(
                "Provided ID does not appear on the table."
                "For a list of valid IDs check"
                "`self.id_translation_table['id'].unique()`"
            )

        # Return the entry in the `property_name` column of the row
        return row[property_name].iloc[0]

    def _filter_by_property(self, property_name, property_value):
        try:
            grouping = self.anatomical_table.groupby(property_name)
            df = grouping.get_group(property_value)
        except KeyError as err:
            raise ValueError(
                "{} = {} does not appear on the table. ".format(
                    property_name, property_value
                )
            ) from err
        return df

    def get_structure_from_id(self, id):
        """Get anatomical structure from its ID

        Parameters
        ----------
        id : int
            ID of the structure

        Returns
        -------
        pandas.DataFrame
            A selection of the anatomical table matching the provided ID
        """
        return self._filter_by_property("id", id)

    def get_parent_from_id(self, id):
        """Get parent anatomical structure from child ID

        Parameters
        ----------
        id : int
            ID of the structure

        Returns
        -------
        pandas.DataFrame
            A selection of the anatomical table matching the desired parent
        """
        parent_id = self.translate_from_id("parent_structure_id", id)
        return self._filter_by_property("id", parent_id)

    def get_children_from_id(self, id):
        """Get children anatomical structures from parent ID

        Parameters
        ----------
        id : int
            ID of the structure

        Returns
        -------
        pandas.DataFrame
            A selection of the anatomical table matching the provided ID
        """
        return self._filter_by_property("parent_structure_id", id)

    def get_negative_sample_from_id(self, id, n=None, frac=None, random_state=2023):
        """Get negative sample coordinates from an anatomical structure ID

        A negative sample is a set of coordinates that do not belong to the
        anatomical structure with the provided ID

        Parameters
        ----------
        id : int
            ID of the structure from which to get negative samples
        n : int, optional
            Number of negative samples to get, by default None. If None, then
            `frac` must be provided
        frac : float, optional
            Fraction of negative samples to get, by default None. If None, then
            `n` must be provided
        random_state : int, optional
            Random state to use for sampling, by default 2023

        Returns
        -------
        pandas.DataFrame
            A selection of the anatomical table matching the provided ID
        """
        df = self.anatomical_table[self.anatomical_table["id"] != id]
        df = df.sample(n=n, frac=frac, random_state=random_state)
        return df

    def get_one_vs_all_labeling(self, id, id_sample_frac=1.0, not_id_sample_frac=1.0):
        """Get one-vs-all-labeling of the anatomical table, where the
        coordinates associated with the provided ID are labeled as 1, and the
        rest are labeled as 0

        Parameters
        ----------
        id : int
            ID of the structure
        id_sample_frac : float, optional
            Fraction of the coordinates associated to label 1 to sample, by
            default 1. This is useful to reduce the size of the
            dataset, and thus speed up computations. If set to 1, all the
            coordinates be used
        not_id_sample_frac : float, optional
            Fraction of the coordinates associated to label 0 to sample, by
            default 1. This is useful to reduce the size of the
            dataset, and thus speed up computations. If set to 1, all the
            coordinates be used

        Returns
        -------
        tuple
            2-tuple containing, in order, the coordinates of the labeled
            structures, and the corresponding binary, one-vs-all labels
        """
        # Copy the anatomical table
        labeled_df = self.anatomical_table.copy()

        # Get all the rows where the `'id'` column matches the provided ID, and
        # sample a fraction of them
        id_df = labeled_df[labeled_df["id"] == id].sample(
            frac=id_sample_frac, random_state=2023
        )

        # Add a column `'label'` with all ones
        id_df["label"] = 1

        # Get all the rows where the `'id'` column does not match the provided
        # ID, and sample a fraction of them
        not_id_df = labeled_df[labeled_df["id"] != id].sample(
            frac=not_id_sample_frac, random_state=2023
        )

        # Add a column `'label'` with all zeros
        not_id_df["label"] = 0

        # Save columns `'x'`, `'y'`, `'z'` as a numpy point array and the
        # `'label'` column as a numpy labels array
        points = np.vstack(
            [id_df[["x", "y", "z"]].to_numpy(), not_id_df[["x", "y", "z"]].to_numpy()]
        )
        labels = np.hstack([id_df["label"].to_numpy(), not_id_df["label"].to_numpy()])

        return points, labels

    def estimate_volume_from_id(self, id):
        """Estimate the volume of an anatomical structure given its ID

        The volume is estimated by computing the volume of the largest
        rectangular cuboid that can be inscribed between annotated points, then
        multiplying the result by the number of annotated points in the
        structure

        Parameters
        ----------
        id : int
            ID of the structure

        Returns
        -------
        float
            Estimated volume of the structure
        """
        # Get structure from ID
        structure = self.get_structure_from_id(id)

        try:
            # Restrict points to one hemisphere, because the anatomical
            # structures are supposed to be symmetric
            structure = structure["hemisphere" == 0]
            single_hemisphere = True
        except KeyError:
            single_hemisphere = False

        # Get unique coordinates for each axis
        x_coords = np.unique(structure.loc[:, "x"].to_numpy())
        y_coords = np.unique(structure.loc[:, "y"].to_numpy())
        z_coords = np.unique(structure.loc[:, "z"].to_numpy())

        # Get the minimum distance between two consecutive coordinates for
        # each axis
        delta_x = np.min(np.diff(x_coords))
        delta_y = np.min(np.diff(y_coords))
        delta_z = np.min(np.diff(z_coords))

        # Compute the volume of the largest rectangular cuboid that can be
        # inscribed between annotated points
        cuboid_volume = delta_x * delta_y * delta_z

        # Estimate volume by multiplying the number of points by the volume of
        # the cuboid
        volume = len(structure) * cuboid_volume

        # If the structure is restricted to a single hemisphere, multiply the
        # estimated volume by 2
        if single_hemisphere:
            volume *= 2.0

        return volume
