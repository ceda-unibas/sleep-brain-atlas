"""Point cloud processing functionality"""

import joblib
import numpy as np
import pandas as pd
from pyntcloud import PyntCloud
from pyntcloud.structures import VoxelGrid
from pyntcloud.utils.array import cartesian
from sklearn.metrics import confusion_matrix, f1_score
from sklearn.neighbors import KNeighborsClassifier

from sba import utils

with utils.all_logging_disabled():
    # Polytope issues a warning if cvxopt with GLPK is not installed
    # We can safely ignore this warning
    import polytope


def hemisphere_partition(points, aad):
    """Partition a point array into two brain atlas hemispheres

    Parameters
    ----------
    points : `numpy.ndarray`
        Array of points, shape `(n_points, n_dimensions)`
    aad : `sba.dataset.AtlasActivationsDataset`
        Object for the dataset of brain atlas activations

    Returns
    -------
    list
        List of two arrays of points, one for each hemisphere
    """
    # Get y coordinate of hemisphere division
    hemisphere_division_at = aad.atlas_dims[0] // 2

    # Partition points into two hemispheres
    hemisphere_1 = points[points[:, 0] < hemisphere_division_at]
    hemisphere_2 = points[points[:, 0] >= hemisphere_division_at]

    return [hemisphere_1, hemisphere_2]


def make_voxel_agg_function(signal: np.array, func="mean"):
    """Make an aggregation function for voxel features

    Intended use is to create a custom callable for the VoxelGridMod.get_voxel_features
    method

    Parameters
    ----------
    signal : numpy.ndarray
        Signal array, shape `(n_points,)`, where each element is the signal
        value of a point in the point cloud.
    func : str, optional
        Aggregation function to use, by default 'mean'. For all available
        options, see :func:`pandas.core.groupby.DataFrameGroupBy.aggregate`

    Returns
    -------
    callable
        Aggregation function for voxel features. Takes a `pyntcloud.structures.VoxelGrid` as input and returns a `numpy.ndarray` of voxel features with shape
        `(voxel_grid.n_voxels,)`
    """
    df = pd.DataFrame(signal, columns=["signal"])

    def agg_function(voxel_grid):
        df["voxel_id"] = voxel_grid.voxel_n
        averages = df.groupby("voxel_id").agg(func)
        features = np.zeros(len(voxel_grid.voxel_centers))
        features[averages.index] = averages.signal
        return features.reshape(voxel_grid.x_y_z)

    return agg_function


class VoxelGridMod(VoxelGrid):
    """Voxel grid class

    Parameters
    ----------
    points : `numpy.ndarray`
        Points of a point cloud to be voxelized, shape `(n_points, 3)`
    xyzmin : tuple, optional
        Minimum coordinates of the voxel grid, by default None. Should be a 3D
        tuple with the minimum coordinates in the x, y, and z dimensions,
        respectively
    xyzmax : tuple, optional
        Maximum coordinates of the voxel grid, by default None. Should be a 3D
        tuple with the maximum coordinates in the x, y, and z dimensions,
        respectively
    **kwargs
        Keyword arguments to pass to the `pyntcloud.structures.VoxelGrid`
        constructor

    Notes
    -----
    This is a modification of the pyntcloud.structures.VoxelGrid. The main difference
    is that this implementation allows for the use of a custom voxelization bounding box and the use of custom methods for the computation of voxel features.
    """

    def __init__(self, points, xyzmin: tuple = None, xyzmax: tuple = None, **kwargs):
        super().__init__(points=points, **kwargs)
        self.xyzmin = xyzmin
        self.xyzmax = xyzmax

    def _parse_xyz_limits(self):
        if self.xyzmin is None:
            xyzmin = self._points.min(0)
        else:
            xyzmin = np.asarray(self.xyzmin)

        if self.xyzmax is None:
            xyzmax = self._points.max(0)
        else:
            xyzmax = np.asarray(self.xyzmax)
        return xyzmin, xyzmax

    def _adjust_xyz(self, xyz_range, xyzmin, xyzmax):
        if self.regular_bounding_box:
            # Adjust to obtain a minimum bounding box with all sides of equal length
            margin = max(xyz_range) - xyz_range
            xyzmin = xyzmin - margin / 2
            xyzmax = xyzmax + margin / 2

        for n, size in enumerate(self.sizes):
            if size is None:
                continue
            margin = (((xyz_range[n] // size) + 1) * size) - xyz_range[n]
            xyzmin[n] -= margin / 2
            xyzmax[n] += margin / 2
            self.x_y_z[n] = ((xyzmax[n] - xyzmin[n]) / size).astype(int)

        return xyzmin, xyzmax

    def _create_regular_grid(self, xyzmin, xyzmax):
        segments = []
        shape = []
        for i in range(3):
            s, step = np.linspace(
                xyzmin[i], xyzmax[i], num=(self.x_y_z[i] + 1), retstep=True
            )
            segments.append(s)
            shape.append(step)
        return segments, shape

    def _compute_voxel_centers(self):
        midsegments = [
            (self.segments[i][1:] + self.segments[i][:-1]) / 2 for i in range(3)
        ]
        return cartesian(midsegments).astype(np.float32)

    def _compute_voxel_colors(self):
        if self.colors is not None:
            order = np.argsort(self.voxel_n)
            _, breaks, counts = np.unique(
                self.voxel_n[order], return_index=True, return_counts=True
            )
            repeated_counts = np.repeat(counts[:, None], 3, axis=1)
            # Square colors. Why square? watch this:
            # https://www.youtube.com/watch?v=LKnqECcg6Gw
            squared_colors = np.square(self.colors[order].astype(np.int64))
            summed_colors = np.add.reduceat(squared_colors, breaks, axis=0)
            averaged_colors = np.sqrt(summed_colors / repeated_counts)
            voxel_colors = np.rint(averaged_colors).astype(np.uint8)
            return voxel_colors

    def compute(self):
        """Compute voxel grid

        Notes
        -----
        This is a reimplementation of the `compute` method of the `VoxelGrid`
        class of the `pyntcloud` package. The main difference is that this
        implementation allows for the use of a custom bounding box via explicit
        specification of `xyzmin` and `xyzmax` parameters.
        """
        # Parse xyz range
        xyzmin, xyzmax = self._parse_xyz_limits()
        xyz_range = xyzmax - xyzmin

        # Adjust
        xyzmin, xyzmax = self._adjust_xyz(xyz_range, xyzmin, xyzmax)

        # Create regular grid
        segments, shape = self._create_regular_grid(xyzmin, xyzmax)

        # Update attributes
        self.xyzmin = xyzmin
        self.xyzmax = xyzmax
        self.segments = segments
        self.shape = shape
        self.n_voxels = np.prod(self.x_y_z)
        self.id = "V({},{},{})".format(
            self.x_y_z, self.sizes, self.regular_bounding_box
        )

        # Find where each point lies in corresponding segmented axis. Subtraction by 1
        # is so that indices start from zero. Clip for edge cases
        self.voxel_x = np.clip(
            np.searchsorted(self.segments[0], self._points[:, 0]) - 1, 0, self.x_y_z[0]
        )
        self.voxel_y = np.clip(
            np.searchsorted(self.segments[1], self._points[:, 1]) - 1, 0, self.x_y_z[1]
        )
        self.voxel_z = np.clip(
            np.searchsorted(self.segments[2], self._points[:, 2]) - 1, 0, self.x_y_z[2]
        )
        self.voxel_n = np.ravel_multi_index(
            [self.voxel_x, self.voxel_y, self.voxel_z], self.x_y_z
        )

        # Compute the center of each voxel
        self.voxel_centers = self._compute_voxel_centers()

        # Compute voxel colors
        self.voxel_colors = self._compute_voxel_colors()

    def get_voxel_features(self, method="count"):
        """Get voxel features

        Parameters
        ----------
        method : str or callable, optional
            Method of feature computation, by default "count". Options are "count", "binary" or a callable. If a callable is provided, it should take a `VoxelGridMod` object as input and return a `numpy.ndarray` of voxel features with shape `(voxel_grid.n_voxels,)`. If "binary", then the voxel features are binary, with a `1` indicating that the voxel contains at least one point. If "count", then the voxel features are the number of points in each voxel. By default, "count".

        Returns
        -------
        numpy.ndarray
            Voxel features

        Notes
        -----
        This is a reimplementation of the `get_feature_vector` method from :class:`pyntcloud.structures.VoxelGrid`. The main difference is
        that this implementation allows for the use of a custom method for
        feature computation.

        When implementing a custom method, it's useful to know of the existence
        of the attribute `voxel_n` of the `VoxelGrid` class. This attribute is
        a 1D, raveled indices array with the same length as the number of
        points in the point cloud. Each element of the array indicates the
        voxel to which the corresponding point belongs. Using `voxel_n`, the
        default method, `"count"`, is a two-liner:
        `count = np.bincount (voxel_grid.voxel_n); vector[:len(count)] = count`
        """
        vector = np.zeros(self.n_voxels)

        available_methods = ["count", "binary"]

        if method == "binary":
            vector[np.unique(self.voxel_n)] = 1

        elif method == "count":
            count = np.bincount(self.voxel_n)
            vector[: len(count)] = count

        elif callable(method):
            vector = method(self)

        else:
            raise ValueError(
                "Method {} not available. Available methods are: {}".format(
                    method, available_methods
                )
            )

        return vector.reshape(self.x_y_z)


class PointCloud:
    """Point cloud processing class

    Parameters
    ----------
    coordinates_df : pandas.DataFrame
        A table with columns `'x', 'y', 'z'` for the coordinates of the point
        cloud
    """

    def __init__(self, coordinates_df) -> None:
        self.cloud = PyntCloud(coordinates_df)

    def get_bounding_box(self) -> tuple:
        """Get bounding box of point cloud

        Returns
        -------
        tuple
            Minimum and maximum coordinates of the point cloud
        """
        xyzmin = self.cloud.xyz.min(0)
        xyzmax = self.cloud.xyz.max(0)
        return xyzmin, xyzmax

    def _sanity_check_voxel_size(self, voxel_size, grid_dims):
        # Parse voxel size / number of voxels
        if (grid_dims is None) and (voxel_size is None):
            raise ValueError("Either voxel_size or n_voxels must be given")
        elif (grid_dims is not None) and (voxel_size is not None):
            raise ValueError("Cannot provide both voxel_size and grid_dims")

    def _make_kwargs(self, voxel_size, grid_dims):
        if voxel_size is not None:
            kwargs = {
                "size_x": voxel_size[0],
                "size_y": voxel_size[1],
                "size_z": voxel_size[2],
            }
        else:
            kwargs = {"n_x": grid_dims[0], "n_y": grid_dims[1], "n_z": grid_dims[2]}
        return kwargs

    def voxelize(
        self,
        voxel_size: tuple = None,
        grid_dims: tuple = None,
        xyzmin: tuple = None,
        xyzmax: tuple = None,
    ) -> None:
        """Voxelize point cloud

        Parameters
        ----------
        voxel_size : tuple, optional
            Voxel size in each dimension, by default None. Must be provided if `grid_dims` is None
        grid_dims : tuple, optional
            Dimensions of the voxel grid, by default None. Must be provided if  `voxel_size` is None
        xyzmin : tuple, optional
            Minimum coordinates of the voxel grid, by default None. Should be a 3D tuple with the minimum coordinates in the x, y, and z dimensions, respectively
        xyzmax : tuple, optional
            Maximum coordinates of the voxel grid, by default None. Should be a 3D tuple with the maximum coordinates in the x, y, and z dimensions, respectively

        Returns
        -------
        `pyntcloud.structures.voxelgrid.VoxelGrid`
            Voxel grid
        `numpy.ndarray`
            Voxel features

        Raises
        ------
        ValueError
            If both `voxel_size` and `grid_dims` are provided or if neither is
            provided
        """
        # Sanity check
        self._sanity_check_voxel_size(voxel_size, grid_dims)

        # Make kwargs from voxel size or grid dims
        kwargs = self._make_kwargs(voxel_size, grid_dims)

        # Create voxel grid
        voxel_grid = VoxelGridMod(
            self.cloud.xyz, xyzmin=xyzmin, xyzmax=xyzmax, **kwargs
        )
        voxel_grid.compute()
        return voxel_grid


class Set:
    """Define a set from a collection of points

    Attributes
    ----------
    classifier : object
        Classifier used to fit the set's indicator function. It should
        implement a `predict` method that takes a `numpy.ndarray` of points
        as input and returns a `numpy.ndarray` of boolean labels indicating
        whether each point belongs to the set or not

    Notes
    -----
    The set's indicator function is fitted using a classifier. The classifier
    is used to predict whether points belong to the set or not. The indices of
    the points belonging to the set are then obtained by selecting the points
    with a positive prediction.
    """

    def __init__(self):
        self.classifier = None

    def fit(self, points, labels, method="knn", verbose=0, **kwargs):
        """Fit the set's indicator function

        Parameters
        ----------
        points : `numpy.ndarray`
            Points to fit, shape `(n_points, n_dimensions)`
        labels : `numpy.ndarray`
            Labels of points, shape `(n_points,)`
        method : str, optional
            Method to use for fitting the set's indicator function. Options are 'knn' (k-nearest neighbors classification), 'uc' (union of convex polytopes classification). By default, 'knn'.
        verbose : int, optional
            Verbosity level, by default 0
        **kwargs
            Optional keyword arguments to pass to the classifier constructor

        Returns
        -------
        float
            F1-score
        numpy.ndarray
            Confusion matrix
        """
        # Create classifier object
        if method == "knn":
            self.classifier = KNeighborsClassifier(**kwargs)
        elif method == "uc":
            self.classifier = UCClassifier(**kwargs)
        else:
            raise ValueError('Invalid method. Valid methods are "knn" and "uc"')

        # Fit the set's indicator function
        if verbose > 0:
            print("Fitting...")
        self.classifier.fit(points, labels)

        # Record geometry (bounding box, dimensions, etc.)
        positive_points = points[labels == 1]
        self.xyzmin = positive_points.min(0)
        self.xyzmax = positive_points.max(0)
        self.dim = positive_points.shape[1]

        # Evaluate predictions
        if verbose > 0:
            print("Evaluating...")

        score, cm = self.evaluate(points, labels, verbose=0)

        if verbose > 0:
            print("f1-score =  {:.3f}".format(score))

        return score, cm

    def evaluate(self, points, labels, verbose=0):
        if verbose > 0:
            print("Evaluating...")

        # Get predictions
        predictions = self.belongs_to_set(points)

        # Get f1-score
        score = f1_score(
            labels,
            predictions,
        )

        if verbose > 0:
            print("f1-score =  {:.3f}".format(score))

        # Compute confusion matrix
        cm = confusion_matrix(labels, predictions)

        return score, cm

    def belongs_to_set(self, points):
        """Check if points belong to the set

        Parameters
        ----------
        points : `numpy.ndarray`
            Points to check, shape `(n_points, n_dimensions)`

        Returns
        -------
        `numpy.ndarray`
            Boolean array indicating whether each point belongs to the set.
            That is, the values of the set's indicator function, evaluated at
            each point
        """
        if self.classifier is None:
            raise ValueError("Set not fitted. Please call .fit() first")

        # Get predictions
        with joblib.parallel_backend("loky", n_jobs=-1):
            pred_labels = self.classifier.predict(points)

        return pred_labels

    def get_indices_belonging_to_set(self, points):
        """Get indices belonging to the set

        Parameters
        ----------
        points : `numpy.ndarray`
            Points to check, shape `(n_points, n_dimensions)`

        Returns
        -------
        `numpy.ndarray`
            Indices of points belonging to the set
        """
        # Get boolean belonging labels
        labels = self.belongs_to_set(points)

        # Get indices of points belonging to the set
        indices = np.where(labels == 1)[0]

        # Return indices
        return indices

    def get_volume(self, method="auto", n_samples=1e5):
        """Get estimate of the volume of the set

        Parameters
        ----------
        method : str, optional
            Method to use for volume estimation. Options are 'auto' and 'mc'. If 'mc', then a Monte Carlo estimation is used. If 'auto', then the union of convex polytopes volumes is used if available, falling back to Monte Carlo estimation otherwise. Default is 'auto'
        n_samples : int, optional
            Number of samples to use for Monte Carlo method, by default 1e5

        Returns
        -------
        float
            Volume estimate
        """

        if self.classifier is None:
            raise ValueError("Set not fitted. Please call .fit() first")

        if method == "auto":
            try:
                return self.classifier.union_of_convex.volume
            except AttributeError:
                # Use Monte Carlo method
                return self.get_volume(method="mc")

        elif method == "mc":
            # Draw points uniformly at random from the set's bounding box
            points = np.random.uniform(
                self.xyzmin, self.xyzmax, size=(int(n_samples), self.dim)
            )

            # Evaluate set's indicator function at the randomly drawn points
            labels = self.belongs_to_set(points)

            # Get volume estimate as fraction of randomly drawn points that
            # fall within the set multiplied by the volume of the bounding box
            fraction = np.sum(labels) / len(labels)
            bbox_volume = np.prod(self.xyzmax - self.xyzmin)
            volume = fraction * bbox_volume

            return volume

        else:
            raise ValueError('Invalid method. Available methods are "auto" and "mc"')


class UCClassifier:
    """Union of convex polytopes classifier

    Parameters
    ----------
    partition_fun : callable, optional
        Function to partition the points into convex parts, by default
        None. If `None`, the points are not partitioned and the whole
        region is represented as a single convex polytope. If not `None`,
        the function should take a `numpy.ndarray` of points as input and
        return a list of arrays of points, each array representing a convex
        part of the region.

    Attributes
    ----------
    union_of_convex : `polytope.Region`
        Union of convex polytopes

    See Also
    --------
    polytope.qhull
    polytope.union
    polytope.reduce
    """

    def __init__(self, partition_fun=None) -> None:
        self.union_of_convex = None

        # Parse partition function
        if partition_fun is None:

            def partition_fun(x):
                return [x]

        self.partition_fun = partition_fun

    def fit(self, points, labels):
        """Fit a union of convex polytopes from a set of points

        The region represented by the point cloud is assumed to the convex at
        each sub-region defined by a partition function. The whole region is
        then represented as the union of such convex polytopes.

        Parameters
        ----------
        points : `numpy.ndarray`
            Array with shape `(n_points, 3)` containing both points in ambient
            space that belong and points that do not belong to the region being
            modeled.
        labels : `numpy.ndarray`
            Array with shape `(n_points,)` containing the labels of the points
            in `points`. The labels should be binary, with `1` indicating that
            the point belongs to the region and `0` indicating that it does
            not.

        Notes
        -----
        This method uses the `polytope.qhull` function to compute the convex
        hull of each set of points. The convex hulls are then combined into a
        single convex polytope using the `polytope.union` function. Finally,
        the `polytope.reduce` function is used to remove redundant inequalities
        from the H-representation of the polytope. The fitted union of convex
        polytopes is stored in the `union_of_convex` attribute.

        Only the points belonging to the region are used to fit the set. The
        rest of them could have dummy values. The `fit(points, labels)`
        signature is kept to match that of scikit-learn classifiers.

        See Also
        --------
        polytope.qhull
        polytope.Region
        polytope.union
        polytope.reduce
        """
        # Get points belonging to the region
        region_points = points[labels == 1]

        # Partition points into convex parts
        region_parts = self.partition_fun(region_points)

        # For each sub-region in the partition, compute the convex hull
        convex_parts = []
        for part_points in region_parts:
            convex_parts.append(polytope.qhull(part_points))

        # Create union of convex polytopes
        self.union_of_convex = polytope.Region(convex_parts)

        # Remove redundant inequalities from the H-representation
        self.union_of_convex = polytope.reduce(self.union_of_convex)

    def predict(self, points):
        """Predict whether points belong to the union of convex polytopes

        Parameters
        ----------
        points : `numpy.ndarray`
            Points to check, shape `(n_points, n_dimensions)`

        Returns
        -------
        `numpy.ndarray`
            Boolean array indicating whether each point belongs to the union of
            convex polytopes. That is, the values of the set's indicator
            function, evaluated at each point
        """
        if self.union_of_convex is None:
            raise ValueError(
                "Union of convex sets not fitted. " + "Please call .fit() first"
            )
        return self.union_of_convex.contains(points.T).astype(int)
