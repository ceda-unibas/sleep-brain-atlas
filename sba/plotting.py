"""Plotting module"""

from logging import warning
from string import Template

import matplotlib.pyplot as plt
import numpy as np
import plotly.graph_objects as go
import pythreejs
from IPython.display import display

from sba import utils


def _default_activation_color():
    return "#ffa500"


def get_centroid_and_camera_position(points, zoom_factor=1.0):
    """Get centroid and camera position

    Parameters
    ----------
    points : `numpy.ndarray`
        Points
    zoom_factor : float, optional
        Zoom factor, by default 1.

    Returns
    -------
    `numpy.ndarray`
        Centroid
    tuple
        Camera position
    """
    centroid = points.mean(0)
    abs_y_max = abs(points.max(0)[1])
    abs_z_max = abs(points.max(0)[2])
    position = tuple(centroid + [0, abs_y_max, abs_z_max / zoom_factor])

    return centroid, position


def get_voxelgrid_mesh(voxel_centers, voxel_colors, opacity=0.2):
    """Get voxel grid mesh

    Parameters
    ----------
    voxel_centers : `numpy.ndarray`
        Voxel centers
    voxel_colors : `numpy.ndarray`
        Voxel colors
    opacity : float, optional
        Opacity of the voxels, by default 0.2

    Returns
    -------
    `pythreejs.Mesh`
        Voxel grid mesh
    """

    # Define base cube
    vertices, faces = _base_cube()

    # Get cube colors and offsets from voxel centers and colors
    colors = pythreejs.InstancedBufferAttribute(
        array=voxel_colors,
        meshPerAttribute=3,
    )
    offsets = pythreejs.InstancedBufferAttribute(
        array=voxel_centers, meshPerAttribute=3
    )

    # Create instanced geometry and material
    instanced_geometry = pythreejs.InstancedBufferGeometry(
        attributes={
            "position": pythreejs.BufferAttribute(array=vertices),
            "index": pythreejs.BufferAttribute(array=faces.ravel()),
            "offset": offsets,
            "color": colors,
        }
    )

    # Remark: I cannot seem to be able to set opacity in the InstancedBufferAttribute, only directly in the material string (see below)
    t = Template("""
        precision highp float;
        attribute vec3 offset;
        attribute vec3 color;
        attribute float opacity;
        varying vec3 vPosition;
        varying vec4 vColor;
        void main(){
            vColor = vec4(color, $opacity);
            vPosition = position + offset;
            gl_Position = projectionMatrix*modelViewMatrix*vec4(vPosition, 1.0);
        }
        """)
    material = pythreejs.ShaderMaterial(
        vertexShader=t.substitute(opacity=opacity),
        fragmentShader="""
    precision highp float;
    varying vec4 vColor;
    void main() {
        gl_FragColor = vColor;
    }
    """,
        # vertexColors='VertexColors',
        transparent=True,
        depthTest=False,
        blending="NormalBlending",
    )

    return pythreejs.Mesh(instanced_geometry, material, frustumCulled=False)


def _base_cube():
    vertices = np.array(
        np.meshgrid([-0.5, 0.5], [-0.5, 0.5], [-0.5, 0.5]), dtype=np.float32
    ).T.reshape(-1, 3)
    faces = np.array(
        [
            [0, 3, 2],
            [0, 1, 3],
            [1, 7, 3],
            [1, 5, 7],
            [5, 6, 7],
            [5, 4, 6],
            [4, 2, 6],
            [4, 0, 2],
            [2, 7, 6],
            [2, 3, 7],
            [4, 1, 0],
            [4, 5, 1],
        ],
        dtype=np.uint32,
    )
    return vertices, faces


def parse_common_plot_kwargs(**kwargs):
    """Parse common plot keyword arguments

    Parameters
    ----------
    **kwargs : dict, optional
        Keyword arguments

    Returns
    -------
    tuple
        Tuple with the following elements:
            - cmap : str
                Color map to use, by default 'Oranges'
            - dims : tuple
                Dimensions of the plot, by default (600, 600)
            - opacity : float
                Opacity of the voxels, by default 0.2
            - kwargs : dict
                The remainder of the input kwargs

    """
    cmap = kwargs.pop("cmap", "Oranges")
    dims = kwargs.pop("dims", (600, 600))
    opacity = kwargs.pop("opacity", 0.2)
    return cmap, dims, opacity, kwargs


def plot_voxels_with_pythreejs(voxel_grid, features, zoom_factor=2, **kwargs):
    """Plot voxel grid with pythreejs

    Parameters
    ----------
    voxel_grid : `pyntcloud.structures.VoxelGrid`
        Voxel grid
    features : `numpy.ndarray`
        Voxel grid features
    zoom_factor : int, optional
        Zoom factor, by default 2. The higher the zoom factor, the closer the
        camera (up to a limit)
    **kwargs : dict, optional
        Extra keyword arguments to pass to `parse_common_plot_kwargs`
    """
    # Parse kwargs
    cmap, dims, opacity, kwargs = parse_common_plot_kwargs(**kwargs)

    # Voxel grid colors
    voxel_colors = _get_voxel_grid_colors(features, cmap=cmap)

    # Find voxel centers from non-zero features
    scaled_shape = np.asarray(voxel_grid.shape) / min(voxel_grid.shape)
    voxel_centers = (np.argwhere(features) * scaled_shape).astype(np.float32)

    # Get centroid and camera position
    centroid, camera_position = get_centroid_and_camera_position(
        voxel_centers, zoom_factor=zoom_factor
    )

    # Create scene
    width, height = dims
    camera = pythreejs.PerspectiveCamera(
        fov=90, aspect=width / height, position=camera_position, up=[0, 0, 1]
    )
    mesh = get_voxelgrid_mesh(voxel_centers, voxel_colors, opacity=opacity)
    scene = pythreejs.Scene(children=[camera, mesh], background="#000000")
    controls = pythreejs.OrbitControls(controlling=camera, target=tuple(centroid))
    camera.lookAt(tuple(centroid))
    renderer = pythreejs.Renderer(
        scene=scene,
        camera=camera,
        controls=[controls],
        width=width,
        height=height,
    )

    # Display scene
    display(renderer)


def plot_voxels_with_plotly(
    voxel_grid,
    features,
    method="volume",
    isomin=None,
    isomax=None,
    null_value=0,
    fig=None,
    **kwargs,
):
    """Plot voxel grid with plotly

    Parameters
    ----------
    voxel_grid : `pyntcloud.structures.VoxelGrid`
        Voxel grid
    features : `numpy.ndarray`
        Voxel grid features
    method : str, optional
        Plotting method, by default 'volume'. Options are 'volume' or 'scatter'
    isomin : float, optional
        Value of the minimum isosurface to plot, by default None. Only used if
        method is 'volume'
    isomax : float, optional
        Value of the maximum isosurface to plot, by default None. Only used if
        method is 'volume'
    null_value : int, optional
        Feature value in voxels that should be discarded when plotting using the
        method 'scatter', by default 0
    fig : plotly.graph_objects.Figure, optional
        Plotly figure onto which to add the trace, by default None
    **kwargs : dict, optional
        Extra keyword arguments to pass to `parse_common_plot_kwargs`

    Returns
    -------
    `plotly.graph_objects.Figure`
        Plotly figure
    """
    # Parse kwargs
    cmap, dims, opacity, kwargs = parse_common_plot_kwargs(**kwargs)

    # Parse figure
    if fig is None:
        fig = go.Figure()

    if method == "volume":
        fig = _plot_voxel_volume(
            voxel_grid,
            features,
            fig,
            cmap=cmap,
            opacity=opacity,
            isomin=isomin,
            isomax=isomax,
        )

    elif method == "scatter":
        fig = _plot_voxel_scatter(
            voxel_grid, features, fig, cmap=cmap, opacity=opacity, null_value=null_value
        )

    # Set layout to match pythreejs
    fig = _match_pythreejs_layout(fig, dims)

    return fig


def _match_pythreejs_layout(fig, dims):
    width, height = dims
    fig.update_layout(
        height=height,
        width=width,
        paper_bgcolor="rgba(0,0,0,1)",
        plot_bgcolor="rgba(0,0,0,1)",
        showlegend=False,
        scene=dict(
            xaxis=dict(visible=False),
            yaxis=dict(visible=False),
            zaxis=dict(visible=False),
        ),
    )
    return fig


def _plot_voxel_volume(voxel_grid, features, fig, **kwargs):
    # Important: plotly Volume does not handle well voxel grids
    # with many voxels, because it has to hold the whole grid
    # (even empty voxels) in memory when plotting
    features = features.ravel()
    voxel_centers = voxel_grid.voxel_centers

    # Parse kwargs
    cmap, _, opacity, kwargs = parse_common_plot_kwargs(**kwargs)
    isomin = kwargs.pop("isomin", None)
    isomax = kwargs.pop("isomax", None)

    fig.add_trace(
        go.Volume(
            x=voxel_centers[:, 0],
            y=voxel_centers[:, 1],
            z=voxel_centers[:, 2],
            value=features.ravel(),
            opacity=opacity,
            colorscale=cmap,
            surface_count=25,
            showscale=False,
            isomin=isomin,
            isomax=isomax,
        )
    )

    return fig


def _plot_voxel_scatter(voxel_grid, features, fig, **kwargs):
    # Parse kwargs
    cmap, _, opacity, kwargs = parse_common_plot_kwargs(**kwargs)
    null_value = kwargs.pop("null_value", None)

    # Unravel features
    features = features.ravel()

    # Get indices of valid voxel grid features
    valid_voxels = np.squeeze(np.argwhere(features != null_value))

    # Restrict voxel grid and features to valid voxels
    features = features[valid_voxels]
    voxel_centers = voxel_grid.voxel_centers[valid_voxels, :]

    # Plot voxel grid with plotly
    fig.add_trace(
        go.Scatter3d(
            x=voxel_centers[:, 0],
            y=voxel_centers[:, 1],
            z=voxel_centers[:, 2],
            mode="markers",
            marker=dict(
                color=features,
                colorscale=cmap,
                opacity=opacity,
                symbol="square",
                size=3,
            ),
        )
    )

    return fig


def plot_voxels(
    voxel_grid,
    features,
    backend="pythreejs",
    **kwargs,
):
    """Plot voxel grid

    Parameters
    ----------
    voxel_grid : `pyntcloud.structures.VoxelGrid`
        Voxel grid
    features : `numpy.ndarray`
        Voxel grid features
    backend : str, optional
        Plotting backend, by default 'pythreejs'. Options are
        'pythreejs', 'plotly_scatter', or 'plotly_volume'
    **kwargs : dict, optional
        Extra keyword arguments to pass to either `plot_voxels_with_pythreejs` or `plot_voxels_with_plotly`, according to the chosen backend


    Returns
    -------
    `plotly.graph_objects.Figure`
        Plotly figure, if backend is 'plotly_scatter' or 'plotly_volume',
        otherwise None

    Raises
    ------
    ValueError
        If the backend is not 'pythreejs', 'plotly_scatter', or 'plotly_volume'

    See Also
    --------
    plot_voxels_with_pythreejs : Plot voxel grid with pythreejs
    plot_voxels_with_plotly : Plot voxel grid with plotly
    """
    # Plot voxel grid with pythreejs
    fig = None
    if backend == "pythreejs":
        plot_voxels_with_pythreejs(voxel_grid, features, **kwargs)
    elif backend == "plotly_scatter":
        # Force method to be 'scatter', despite what the user specifies in
        # kwargs
        _ = kwargs.pop("method", None)
        fig = plot_voxels_with_plotly(
            voxel_grid,
            features,
            method="scatter",
            **kwargs,
        )
    elif backend == "plotly_volume":
        # Force method to be 'volume', despite what the user specifies in
        # kwargs
        _ = kwargs.pop("method", None)
        fig = plot_voxels_with_plotly(
            voxel_grid,
            features,
            method="volume",
            **kwargs,
        )
    else:
        raise ValueError(
            "Invalid plotting backend. "
            + "Options are 'pythreejs', 'plotly_scatter', or 'plotly_volume'"
        )
    return fig


def _get_voxel_grid_colors(features, cmap="Oranges"):
    s_m = plt.cm.ScalarMappable(cmap=cmap)
    flattened = features.ravel()
    flattened = flattened[np.nonzero(flattened)]
    rgba = s_m.to_rgba(flattened)
    voxel_colors = rgba[:, :3].astype(np.float32)
    return voxel_colors


def plot_brain_outline(
    brain_outline_table,
    fig=None,
    flip_z=True,
    **kwargs,
) -> go.Figure:
    """Plot brain outline

    Parameters
    ----------
    brain_outline_table : pandas.DataFrame
        Brain outline table
    fig : plotly.graph_objects.Figure, optional
        Plotly figure onto which to add the trace, by default None
    flip_z : bool, optional
        Whether to flip the z-axis so that the top of the brain is at the top
        of the plot, by default True
    **kwargs : dict, optional
        Extra keyword arguments to pass to the marker dictionary in
        plotly.graph_objects.Scatter3d. By default, the keys 'size', 'opacity' are set
        to 3 and 0.05, respectively. You can also set here any of the common keyword arguments processed by parse_common_plot_kwargs.

    Returns
    -------
    plotly.graph_objects.Figure
        Plotly figure
    """
    # Parse figure
    if fig is None:
        fig = go.Figure()

    # Parse kwargs
    opacity = kwargs.pop("opacity", 0.05)
    _, dims, _, kwargs = parse_common_plot_kwargs(**kwargs)

    # Draw brain outline as a scatter plot
    fig.add_trace(
        go.Scatter3d(
            x=brain_outline_table["x"],
            y=brain_outline_table["y"],
            z=brain_outline_table["z"],
            mode="markers",
            marker=dict(
                size=3,
                color="white",
                opacity=opacity,
                **kwargs,
            ),
            name="Brain outline",
        )
    )

    # Set layout to match pythreejs
    fig = _match_pythreejs_layout(fig, dims)

    # Flip z-axis if requested
    if flip_z:
        fig.update_layout(scene=dict(zaxis=dict(autorange="reversed")))

    # Remove legend
    fig.update_layout(showlegend=False)

    return fig


def add_legend(fig):
    fig.update_layout(
        showlegend=True,
        legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99,
            itemsizing="constant",
            bgcolor="rgba(0,0,0,0)",
            font=dict(size=12, color="white"),
        ),
    )
    return fig


def _parse_anat_struct_names_colors(
    anatomical_atlas,
    names=None,
    colors=None,
):
    # Parse names/ids
    names, ids = _resolve_names_ids(anatomical_atlas, names=names)

    # Get default colors
    if colors is None:
        colors = utils.get_maximally_distinct_colors()

    # Limit the number of structures to plot to the number of colors in the
    # palette
    ids, names = _limit_structures_to_plot(colors, ids, names)

    return names, ids, colors


def _resolve_names_ids(anatomical_atlas, names=None, ids=None):
    if names is not None:
        ids = [anatomical_atlas.translate_to_id("name", name) for name in names]
    elif ids is not None:
        names = [anatomical_atlas.translate_from_id("name", id) for id in ids]
    if names is None and ids is None:
        raise ValueError("Either names or ids must be specified")
    return names, ids


def _limit_structures_to_plot(colors, ids, names):
    n_colors = len(colors)
    if len(ids) > n_colors:
        warning(
            "More than {} structures were specified. ".format(n_colors)
            + "Only the first {} will be plotted.".format(n_colors)
        )
        ids = ids[:n_colors]
        names = names[:n_colors]
    return ids, names


def plot_anatomical_annotations(
    anatomical_atlas, names=None, ids=None, fig=None, colors=None, **kwargs
):
    """Plot anatomical annotations

    Parameters
    ----------
    anatomical_atlas : `sba.AnatomicalAtlas`
        Anatomical atlas
    names : list, optional
        List of names of structures to plot, by default None
    ids : list, optional
        List of ids of structures to plot, by default None
    fig : plotly.graph_objects.Figure, optional
        Plotly figure onto which to add the trace, by default None
    colors : list, optional
        List of colors to use for plotting, by default None. If None, a
        discrete palette of maximally distant colors, with no orange hue
        is used.
    **kwargs : dict, optional
        Extra keyword arguments to pass to the marker dictionary in
        plotly.graph_objects.Scatter3d. By default, the keys 'size', 'opacity' are set
        to 3 and 0.05, respectively.

    Returns
    -------
    plotly.graph_objects.Figure
        Plotly figure

    Raises
    ------
    ValueError
        If neither names nor ids are specified

    See Also
    --------
    sba.dataset.AnatomicalAtlas : Anatomical atlas
    """
    # Parse figure
    if fig is None:
        fig = go.Figure()

    # Parse optional kwargs
    size = kwargs.pop("size", 3)
    opacity = kwargs.pop("opacity", 0.05)

    names, ids, colors = _parse_anat_struct_names_colors(
        anatomical_atlas, names=names, colors=colors
    )

    for i, id in enumerate(ids):
        structure = anatomical_atlas.get_structure_from_id(id)
        fig.add_trace(
            go.Scatter3d(
                x=structure["x"],
                y=structure["y"],
                z=structure["z"],
                mode="markers",
                marker=dict(
                    size=size,
                    color=colors[i],
                    opacity=opacity,
                    **kwargs,
                ),
                name=names[i],
            )
        )

    # Add legend with the names of the structures.
    fig = add_legend(fig)

    return fig
