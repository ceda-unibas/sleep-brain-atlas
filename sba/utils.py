"""Utilities module"""

import logging
import os
from contextlib import contextmanager

import nltk
import nrrd
import numpy as np
import pandas as pd
from skimage.morphology import dilation
from tifffile import TiffFile
from tqdm import tqdm


@contextmanager
def all_logging_disabled(highest_level=logging.CRITICAL):
    """A context manager that will prevent any logging messages
    triggered during the body from being processed.

    Parameters
    ----------
    highest_level : int, optional
        The highest logging level in use. This would only need to be changed if
        a custom level greater than CRITICAL is defined, by default
        `logging.CRITICAL`

    Notes
    -----
    This is a modified version of the code found at
    https://gist.github.com/simon-weber/7853144
    """
    # Two kind-of hacks here:
    # - Can't get the highest logging level in effect => delegate to the user
    # - Can't get the current module-level override => use an undocumented
    #   (but non-private!) interface
    previous_level = logging.root.manager.disable
    logging.disable(highest_level)
    try:
        yield
    finally:
        logging.disable(previous_level)


def fix_typo(source, target, typo_dist=2):
    """Fix typo on string

    Parameters
    ----------
    source : str
        Source string, taken as the ground truth spelling
    target : str
        Target string to potentially fix
    typo_dist : int, optional
        Maximum edit distance between source and target string to define the
        presence of a typo, by default 2

    Returns
    -------
    str
        The corrected string. If a typo has been deemed to take place, then the
        source string is returned. Otherwise, the target string is returned
    """
    if nltk.edit_distance(source, target) <= typo_dist:
        # Consider there is a typo and return the source
        return source
    else:
        # Consider there is no typo and return the target
        return target


def load_3d_tiff(tif_path, dtype=int):
    """Load TIFF file as a 3d numpy array

    Parameters
    ----------
    tif_path : str
        Path to the TIFF file

    Returns
    -------
    `numpy.ndarray`
        The TIFF file as a numpy array
    """
    with TiffFile(tif_path) as tif:
        arrays = []
        for page in tqdm(tif.pages):
            arrays.append(page.asarray()[..., None])

    # Concatenate the arrays into a single 3D array
    tif_arr = np.concatenate(arrays, axis=-1).astype(dtype)

    return tif_arr


def load_nrrd_array(nrrd_path):
    """Load NRRD file as a numpy array

    Parameters
    ----------
    nrrd_path : str
        Path to the NRRD file

    Returns
    -------
    `numpy.ndarray`
        The NRRD file as a numpy array
    """
    nrrd_arr, _ = nrrd.read(nrrd_path)

    return nrrd_arr


def tri_dim_array_to_table(array, column_names=None, val_dtype=int, dim_dtype=int):
    """Convert a 3D array to a table

    Parameters
    ----------
    array : numpy.ndarray
        A 3D array
    column_names : list, optional
        The column names for the table, by default None. If None, the columns
        are named 'val', 'i', 'j' and 'k'. User should provide a list of four
        strings, in the order: name of the value column, name of the column for
        the first dimension, name of the column for the second dimension, name
        of the column for the third dimension
    val_dtype : type, optional
        The data type for the value column, by default int
    dim_dtype : type, optional
        The data type for the dimension columns, by default int

    Returns
    -------
    pandas.DataFrame
        The table
    """
    if column_names is None:
        column_names = ["val", "i", "j", "k"]

    n_x, n_y, n_z = array.shape

    df = pd.DataFrame(
        array.reshape(n_x * n_y * n_z, -1), columns=[column_names[0]], dtype=val_dtype
    )
    df[column_names[1:]] = np.array(
        np.unravel_index(range(n_x * n_y * n_z), (n_x, n_y, n_z)), dtype=dim_dtype
    ).T

    return df


def dilate_3d_skeleton(array, footprint=None):
    """Dilate a 3D skeleton

    Parameters
    ----------
    array : numpy.ndarray
        A 3D array encoding a 3D skeleton
    footprint : numpy.ndarray, optional
        The footprint to use for the dilation, by default None. If None, a
        a cross-shaped footprint (connectivity=1) is used

    Returns
    -------
    numpy.ndarray
        The dilated skeleton

    See Also
    --------
    :func:`skimage.morphology.dilation`

    """
    dilated_array = np.zeros_like(array, dtype=int)

    # Loop over the z-axis and dilate each slice
    for z in tqdm(range(array.shape[-1])):
        dilated_array[..., z] = dilation(array[..., z], footprint=footprint)

    return dilated_array


def moore_neighborhood(current, backtrack):
    """Clockwise list of pixels from the Moore neighborhood of current pixel

    The first element is the coordinates of the backtrack pixel. The following
    elements are the coordinates of the neighboring pixels in clockwise order.

    Parameters
    ----------
    current: list
        Coordinates of the current pixel, in the form [y, x]
    backtrack: list
        Coordinates of the backtrack pixel, in the form [y, x]

    Returns
    -------
    list or int
        Coordinates of the Moore neighborhood pixels, or 0 if the backtrack
        pixel is not a current pixel neighbor

    Notes
    -----
    The code for this function is adapted from this answer on StackOverflow:
    https://stackoverflow.com/questions/25379752/how-can-i-extract-the-boundary-curve-of-an-image-region-in-scikit-image/76507047#76507047
    """

    operations = np.array(
        [[-1, 0], [-1, 1], [0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1]]
    )
    neighbors = (current + operations).astype(int)

    for i, point in enumerate(neighbors):
        if np.all(point == backtrack):
            # Return the sorted neighborhood
            return np.concatenate((neighbors[i:], neighbors[:i]))
    return 0


def boundary_tracing(region):
    """Coordinates of the region's boundary.

    Parameters
    ----------
    region : obj
        Obtained with skimage.measure.regionprops()

    Returns
    -------
    boundary : 2D array
        List of coordinates of pixels in the boundary
        The first element is the most upper left pixel of the region.
        The following coordinates are in clockwise order.

    Notes
    -----
    The code for this function is adapted from this answer on StackOverflow:
    https://stackoverflow.com/questions/25379752/how-can-i-extract-the-boundary-curve-of-an-image-region-in-scikit-image/76507047#76507047
    """

    # Create the binary image from the region's coordinates
    coords = region.coords
    x = coords[:, 1]
    y = coords[:, 0]
    binary = _binary_image_from_coords(coords)

    # Initialization: starting point is the most upper left point
    start = _find_start(binary, x, y)

    # Determine backtrack pixel for the first element
    backtrack_start = _find_backtrack_start(binary, start)

    # Boundary tracing
    current = start
    backtrack = backtrack_start
    boundary = []
    counter = 0
    while True:
        neighbors_current = moore_neighborhood(current, backtrack)
        y = neighbors_current[:, 0]
        x = neighbors_current[:, 1]
        idx = np.argmax(binary[tuple([y, x])])
        boundary.append(current)
        backtrack = neighbors_current[idx - 1]
        current = neighbors_current[idx]
        counter += 1

        if np.all(current == start) and np.all(backtrack == backtrack_start):
            break

    return np.array(boundary)


def _binary_image_from_coords(coords):
    maxs = np.amax(coords, axis=0)
    binary = np.zeros((maxs[0] + 2, maxs[1] + 2))
    binary[tuple([coords[:, 0], coords[:, 1]])] = 1
    return binary


def _find_start(binary, x, y):
    idx_start = 0
    while True:  # asserting that the starting point is not isolated
        start = [y[idx_start], x[idx_start]]
        focus_start = binary[start[0] - 1 : start[0] + 2, start[1] - 1 : start[1] + 2]
        if np.sum(focus_start) > 1:
            break
        idx_start += 1
    return start


def _find_backtrack_start(binary, start):
    if binary[start[0] + 1, start[1]] == 0 and binary[start[0] + 1, start[1] - 1] == 0:
        backtrack_start = [start[0] + 1, start[1]]
    else:
        backtrack_start = [start[0], start[1] - 1]
    return backtrack_start


def get_maximally_distinct_colors():
    """Get colors from discrete palette, as a list of hex strings

    The palette was generated using the web tool https://medialab.github.io/iwanthue/
    so that the colors are maximally distinct and no oranges show up, as shades of
    orange is the default color palette when plotting voxels
    """
    colors = [
        "#01d2bf",
        "#ca5ece",
        "#00ac4f",
        "#807bf6",
        "#dad95f",
        "#ff8dd8",
        "#a29200",
        "#00d9de",
        "#dfbe6a",
        "#74e3c2",
    ]

    return colors


def hex2rgba(hex, alpha=1.0):
    """Convert a hex color to an rgba color"""
    hex = hex.lstrip("#")
    rgba = tuple(int(hex[i : i + 2], 16) for i in (0, 2, 4)) + (alpha,)
    return rgba


def padded_range(min_val, max_val, padding_factor=0.1):
    """Return a padded range"""
    abs_diff = np.abs(max_val - min_val)
    return [min_val - padding_factor * abs_diff, max_val + padding_factor * abs_diff]


def makedir(path):
    """Create a directory if it does not exist."""
    if not os.path.exists(path):
        os.makedirs(path)
